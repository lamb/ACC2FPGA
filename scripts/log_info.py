#!/usr/bin/python
import os
#import sh
import subprocess
#from subprocess import call, check_output, Popen
from time import gmtime, strftime
import sys
import argparse

def try_print (s) :
  try:
    print s
  except:
    pass

# Parse Input
parser = argparse.ArgumentParser(description='Script to compile, run, and collect information from OpenACC FPGA programs, using OpenARC and the Intel OpenCL FPGA SDK (17.1)', epilog="Log files are written to /home/lamb/ACCtoFPGA/logs/")

parser.add_argument("benchmark_file", help="file containing directories of benchmarks (pathnames) to be processed")
parser.add_argument("-b", "--build", help="compile each benchmark", action="store_true")
parser.add_argument("-r", "--run", help="execute binaries and record output for each benchmark", action="store_true")
parser.add_argument("-l", "--log", help="log area and compilation information for each benchmark", action="store_true")
parser.add_argument("-t", "--text", help="sends a text message upon completion", action="store_true")
parser.add_argument("--clean", help="remove log files from log directory", action="store_true")
args = parser.parse_args()

log_dir = "/home/lamb/ACCtoFPGA/logs/"
if args.clean:
  os.chdir(log_dir)
  subprocess.call('rm -f *.log', shell=True)
  sys.exit()

# Verify user wants to overwrite old builds (i.e. .aocx files)
if args.build:
  build_check = raw_input('Are you sure you want to overwrite previous builds? ').lower()
  if not build_check.startswith('y'):
    try_print("Exiting!")
    sys.exit()

try_print("Processing benchmark_file: " + args.benchmark_file)
if args.build:
  try_print("Building....yes")
else:
  try_print("Building....no")

if args.run:
  try_print("Running.....yes")
else:
  try_print("Running.....no")

if args.log:
  try_print("Logging.....yes")
else:
  try_print("Logging.....no")


timestamp = strftime("%Y-%m-%d_%H:%M:%S")

benchmark_file = args.benchmark_file 
log_file = log_dir + timestamp + "-" + os.path.basename(benchmark_file) + ".log"

# Open a file
in_file = open(benchmark_file, "rw+")
paths = in_file.read().splitlines()

# Start Log
out_file = open(log_file, "a")
out_file.write("Compilations started:" + timestamp + "\n") 
out_file.write("Benchmark File: " + benchmark_file + "\n")
out_file.write("Log File: " + log_file + "\n")

aocl_v = subprocess.check_output(['aocl', 'version'])
out_file.write("AOCL: " + aocl_v)
out_file.write("\n")

out_file.write("Command Line: " + str(sys.argv) + "\n\n")

if args.text:
  subprocess.check_output('echo "' + benchmark_file + ' starting" | mail -s "" 4235023477@tmomail.net', shell=True)
  out_file.write("Start Text sent to 4235023477@tmomail.net");
  try_print("\tStart Text success")

cwd = os.getcwd()
for path in paths:

  out_file.write("----\n")
  out_file.write(path)
  out_file.write("\n\n")
  try_print("\nProccessing " + path + "...")

  # move to directory
  os.chdir(cwd)
  try:
    os.chdir(path)
  except Exception as e:
    out_file.write(repr(e) + "\n\n")
    try_print("\tFailed All (benchmark not found)")
    continue

  # Building 
  if args.build:
    out_file.write("Build: make purge; O2Build.script; make\n")
    try:
      out_file.write("Build started: " + strftime("%Y-%m-%d_%H:%M:%S") + "\n")
      subprocess.call(["make", "purge"])
      subprocess.call(["O2GBuild.script"])
      subprocess.call(["make"]) 
      out_file.write("Build completed: " + strftime("%Y-%m-%d_%H:%M:%S") + "\n")
      try_print("\tBuild Success")
    except Exception as e:
      out_file.write(repr(e) + "\n\n")
      try_print("\tBuild Failed")
     
  os.chdir(cwd)
  os.chdir(path)

  # Logging
  if args.log:
    try:
      os.chdir("./cetus_output/openarc_kernel")
    except Exception as e:
      out_file.write(repr(e) + "\n\n")
      try_print("\tLog Failed")
    else:
      # Compilation info
      out_file.write("Compile Log: (quartus_sh_compile.log)\n")
      try:
        compile_info = subprocess.check_output(['tail', '-n', '4', 'quartus_sh_compile.log'])
        try_print("\tLog (Compilation) Success")
      except Exception as e:
        compile_info = "Failed collect compilation info: " + repr(e)  + "\n"
        try_print("\tLog (Compilation) Failed")
  
      out_file.write(compile_info)
      out_file.write("\n")

      # Area info
      out_file.write("Area Estimates Log: (top.fit.summary)\n")
      try:
        area_info = subprocess.check_output('tail -n 10 top.fit.summary | sed \'s/^/     /\'', shell=True)
        try_print("\tLog (Area Est) Success")
      except Exception as e:
        area_info = "Failed collect area info: " + repr(e) + "\n"
        try_print("\tLog (Area Est) Failed")

      out_file.write("Area Actual Log: (acl_quartus_report.txt)\n")
      try:
        area_info = subprocess.check_output('cat acl_quartus_report.txt | sed \'s/^/     /\'', shell=True)
        try_print("\tLog (Area Act) Success")
      except Exception as e:
        area_info = "Failed collect area info: " + repr(e) + "\n"
        try_print("\tLog (Area Act) Failed")

      out_file.write(area_info)
      out_file.write("\n")

  os.chdir(cwd)
  os.chdir(path)

  # Running
  if args.run:
    out_file.write("Running: cd bin; ./*_ACC\n")
    try:
      os.chdir("./bin")
    except Exception as e:
      out_file.write(repr(e) + "\n\n")
      try_print("\tRun failed")
    else:

      try:
        run_info = subprocess.check_output('*_ACC | sed \'s/^/     /\'', shell=True)
      except Exception as e:
        run_info = "Failed to run benchmark: " + repr(e)  + "\n"
        try_print("\tRun failed")

      out_file.write(run_info)
      out_file.write("\n")
      try_print("\tRun success")

if args.text:
  subprocess.check_output('echo "' + benchmark_file + ' completed" | mail -s "" 4235023477@tmomail.net', shell=True)
  out_file.write("Completion Text sent to 4235023477@tmomail.net");
  try_print("\tCompletion Text success")

out_file.write("\n")
out_file.write("fpga_log completed")

out_file.close()
in_file.close()
