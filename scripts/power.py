#!/usr/bin/python
import os
from subprocess import call
from time import gmtime, strftime

# Open a file
in_file = open("oswald_runs", "rw+")
paths = in_file.read().splitlines()

cwd = os.getcwd()

for path in paths:

  # move to directory
  os.chdir(path)

  os.chdir("./cetus_output/openarc_kernel")
  call(["quartus_pow","top.qpf"]) 
  os.chdir(cwd)

  timestamp = strftime("%Y-%m-%d %H:%M:%S")

  out_file = open("completed.txt", "a")
  out_file.write(timestamp + ": "+ path + "\n")
  out_file.close()

in_file.close()
