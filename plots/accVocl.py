import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib import rc

# Latex fonts
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

benchmarks = ('Sobel', 'FD3D', 'HotSpot', 'SRAD', 'NW')

# sobel runtimes (ms)
sobel_rt_swu = np.mean([0.984646, 1.012297, 1.010992, 1.008844, 1.025740]) 
sobel_rt_man = 1000*np.mean([0.006208, 0.006208, 0.006209, 0.006206, 0.006203])
sobel_rt = [sobel_rt_swu, sobel_rt_man]
sobel_speedup = [ (sobel_rt_swu / i) for i in sobel_rt]

# sobel resource usage 
sobel_ru_swu = 29
sobel_ru_man = 23
sobel_ru = [sobel_ru_swu, sobel_ru_man]

# fd3d runtimes (ms)
fd3d_rt_swu = np.mean([  21.0709, 20.9575, 21.4632, 20.5881, 20.4481])
fd3d_rt_man = np.mean([  19.5885, 19.7165, 18.7186, 19.1861, 19.3388]) 
fd3d_rt = [fd3d_rt_swu, fd3d_rt_man]
fd3d_speedup = [ (fd3d_rt_swu / i) for i in fd3d_rt]

# fd3d resource usage 
fd3d_ru_swu = 61
fd3d_ru_man = 82
fd3d_ru = [fd3d_ru_swu, fd3d_ru_man]

# hotspot runtimes (s)
hotspot_rt_swu = np.mean([7.966, 7.951484, 7.961617, 7.950861, 7.951341]) 
hotspot_rt_man = np.mean([5024.003, 5026.654, 5034.126, 5036.350, 5034.825]) / 1000 
hotspot_rt = [hotspot_rt_swu, hotspot_rt_man]
hotspot_speedup = [ (hotspot_rt_swu / i) for i in hotspot_rt]

# hotspot resource usage 
hotspot_ru_swu = 56
hotspot_ru_man = 71 
hotspot_ru = [hotspot_ru_swu, hotspot_ru_man]

# srad runtimes (s)
srad_rt_swu = np.mean([8.393911, 8.395052, 8.392908, 8.394773, 8.396337])        
srad_rt_man = np.mean([5.091631045, 5.088309033, 5.091588060, 5.088438490, 5.091691952])
srad_rt = [srad_rt_swu, srad_rt_man]
srad_speedup = [ (srad_rt_swu / i) for i in srad_rt]

# srad resource usage 
srad_ru_swu = 81
srad_ru_man = 87   # VERIFY 
srad_ru = [srad_ru_swu, srad_ru_man]

# nw runtimes (s)
nw_rt_sw  = np.mean([3.071,3.072480, 3.068598, 3.068731, 3.068734, 3.072601])
nw_rt_man = np.mean([309.908, 309.618, 309.421, 309.071, 309.458]) / 1000
nw_rt = [nw_rt_sw, nw_rt_man]
nw_speedup = [ (nw_rt_sw / i) for i in nw_rt]

# nw resource usage 
nw_ru_sw  = 24
nw_ru_man = 56
nw_ru = [nw_ru_sw, nw_ru_man]

speedups = [sobel_speedup, fd3d_speedup, hotspot_speedup, srad_speedup, nw_speedup]
resource_usages = [sobel_ru, fd3d_ru, hotspot_ru, srad_ru, nw_ru]

N = len(speedups)
ind = np.arange(N)  # the x locations for the groups
width = 0.25        # the width of the bars

fig, ax = plt.subplots()

# Speedup
nbars = 2
r_array = [None] * nbars
colors = ['white','black']
legend_names = ['OpenACC', 'OpenCL']

# nd speedup
for index in range(0, nbars):
	r_array[index] = ax.bar(0.3 + ind + index*width, [s[index] for s in speedups], width, 
    bottom=0.1,
		color=colors[index])

ax.set_ylabel('Speedup')
ax.set_xticks(ind+width)
ax.legend( (r_array[0][0], r_array[1][0]), 
  legend_names, loc='upper left', prop={'size': 10})

ax.set_yscale('log')
ax.set_ylim([0,17])
plt.xticks(ind + 0.55, benchmarks, fontsize=12)

for index in range(0, nbars):
  for rect in r_array[index]:
    if (rect.get_height() >= 1):
      height = 1.15 * rect.get_height()
    else:
      height = rect.get_height() + 0.2
    plt.text(rect.get_x() + rect.get_width()/2.0, height, '%.1f' % rect.get_height(), ha='center', va='bottom', size='smaller')

plt.show()
