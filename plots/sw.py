import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib import rc
from matplotlib.ticker import ScalarFormatter

# Latex fonts
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

benchmarks = ('Sobel', 'FD3D', 'HotSpot', 'SRAD', 'NW')

# sobel runtimes (ms)
sobel_rt_swi = np.mean([ 21.177612, 21.171751, 21.155164, 21.170842, 21.171415 ])
sobel_rt_sw  = np.mean([6.836, 6.825, 6.839, 6.838, 6.854]) 
sobel_rt = [sobel_rt_swi, sobel_rt_sw]
sobel_speedup = [ (sobel_rt_swi / i) for i in sobel_rt]

# fd3d runtimes (ms)
fd3d_rt_swi = np.mean([  2523.5107, 2523.4314, 2522.5281, 2522.4583, 2523.4456])
fd3d_rt_sw  = np.mean([  111.5395, 111.9913, 111.4584, 111.3562, 111.6962])
fd3d_rt = [fd3d_rt_swi, fd3d_rt_sw]
fd3d_speedup = [ (fd3d_rt_swi / i) for i in fd3d_rt]

# hotspot runtimes (s)
hotspot_rt_swi = np.mean([47.560, 47.383262, 47.358588, 47.383114, 47.391847])
hotspot_rt_sw  = np.mean([34.928, 34.928293, 34.930796, 34.926772, 34.917256])
hotspot_rt = [hotspot_rt_swi, hotspot_rt_sw]
hotspot_speedup = [ (hotspot_rt_swi / i) for i in hotspot_rt]

# srad runtimes (s)
srad_rt_swi = np.mean([27.792110, 27.400091, 27.860566, 27.894033, 27.897259]) 
# sw normal + reduction
srad_rt_sw  = np.mean([23.242782, 23.231826, 23.239022, 23.243115, 23.238653]) 
srad_rt = [srad_rt_swi, srad_rt_sw]
srad_speedup = [ (srad_rt_swi / i) for i in srad_rt]

# nw runtimes (s)
nw_rt_swi = np.mean([115.556, 115.533348, 115.591749, 115.530831, 115.590033])
nw_rt_sw  = np.mean([3.071,3.072480, 3.068598, 3.068731, 3.068734, 3.072601])
nw_rt = [nw_rt_swi, nw_rt_sw]
nw_speedup = [ (nw_rt_swi / i) for i in nw_rt]

speedups = [sobel_speedup, fd3d_speedup, hotspot_speedup, srad_speedup, nw_speedup]

N = len(speedups)
ind = np.arange(N)  # the x locations for the groups
width = 0.2         # the width of the bars

fig, ax = plt.subplots()

# Speedup
nbars = 2
r_array = [None] * nbars 
colors = ['white','black']
legend_names = ['Single work-item', 'Sliding window']

# nd speedup
for index in range(0, nbars):
	r_array[index] = ax.bar(0.7 + ind + index*width, [s[index] for s in speedups], width, 
  bottom=0.1,
  color=colors[index])

ax.set_ylabel('Speedup')
ax.set_xticks(ind+width)
ax.legend( (r_array[0][0], r_array[1][0] ), 
  legend_names, loc='upper left', prop={'size': 10})

ax.set_yscale('log')

plt.xticks(ind + 0.95, benchmarks, fontsize=12)

for rect in r_array[1]:
  height = 1.15*rect.get_height()
  plt.text(rect.get_x() + rect.get_width()/2.0, height, '%.1f' % rect.get_height(), ha='center', va='bottom')

plt.show()
#plt.savefig("overall.pdf", bbox_inches='tight')
