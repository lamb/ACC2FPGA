import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib import rc

# Latex fonts
rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

benchmarks = ('Sobel', 'FD3D', 'HotSpot', 'SRAD', 'NW')
devices = ('E5-2683', 'K40c', 'Stratix V')

# sobel runtimes (ms)
sobel_rt_cpu = 6.2374
sobel_rt_gpu = 1.462510
sobel_rt_fpga =  0.989657
sobel_rt = [sobel_rt_cpu, sobel_rt_gpu, sobel_rt_fpga]
sobel_speedup = [ (sobel_rt_cpu / i) for i in sobel_rt]

# sobel energy 
sobel_en_cpu = 0.5708
sobel_en_gpu = 0.1242
sobel_en_fpga = 0.0135
sobel_en = [sobel_en_cpu, sobel_en_gpu, sobel_en_fpga]
sobel_pef = [ (sobel_en_cpu / i) for i in sobel_en]

# fd3d runtimes (ms)
fd3d_rt_cpu = 30.3983
fd3d_rt_gpu = 40.7843 
fd3d_rt_fpga = 21.0022
fd3d_rt = [fd3d_rt_cpu, fd3d_rt_gpu, fd3d_rt_fpga]
fd3d_speedup = [ (fd3d_rt_cpu / i) for i in fd3d_rt]

# fd3d energy 
fd3d_en_cpu = 3.2788
fd3d_en_gpu = 2.6994 
fd3d_en_fpga = 0.3713
fd3d_en = [fd3d_en_cpu, fd3d_en_gpu, fd3d_en_fpga]
fd3d_pef = [ (fd3d_en_cpu / i) for i in fd3d_en]

# hotspot runtimes (ms)
hotspot_rt_cpu = 1.816
hotspot_rt_gpu = 3.446612
hotspot_rt_fpga = 7.949280 
hotspot_rt = [hotspot_rt_cpu, hotspot_rt_gpu, hotspot_rt_fpga]
hotspot_speedup = [ (hotspot_rt_cpu / i) for i in hotspot_rt]

# hotspot energy 
hotspot_en_cpu = 296.3264
hotspot_en_gpu = 100.863
hotspot_en_fpga = 136.9414
hotspot_en = [hotspot_en_cpu, hotspot_en_gpu, hotspot_en_fpga]
hotspot_pef = [ (hotspot_en_cpu / i) for i in hotspot_en]

# srad runtimes (ms)
srad_rt_cpu = 4.6274
srad_rt_gpu = 6.699691 
srad_rt_fpga = 8.396799
srad_rt = [srad_rt_cpu, srad_rt_gpu, srad_rt_fpga]
srad_speedup = [ (srad_rt_cpu / i) for i in srad_rt]

# srad energy 
srad_en_cpu = 585.0369
srad_en_gpu = 541.1045
srad_en_fpga = 199.7629
srad_en = [srad_en_cpu, srad_en_gpu, srad_en_fpga]
srad_pef = [ (srad_en_cpu / i) for i in srad_en]

# nw runtimes (ms)
nw_rt_cpu = 0.014
nw_rt_gpu = .01654
nw_rt_fpga = 0.1755
nw_rt = [nw_rt_cpu, nw_rt_gpu, nw_rt_fpga]
nw_speedup = [ (nw_rt_cpu / i) for i in nw_rt]

# nw energy 
nw_en_cpu = 1.4330
nw_en_gpu = 1.0437
nw_en_fpga = 2.2244
nw_en = [nw_en_cpu, nw_en_gpu, nw_en_fpga]
nw_pef = [ (nw_en_cpu / i) for i in nw_en]

speedups = [sobel_speedup, fd3d_speedup, hotspot_speedup, srad_speedup, nw_speedup]
energy = [sobel_pef, fd3d_pef, hotspot_pef, srad_pef, nw_pef]

N = len(speedups)
ind = np.arange(N)  # the x locations for the groups
width = 0.3       # the width of the bars
nbars = 3

fig = plt.figure()

# Speedup
ax = fig.add_subplot(211)

r_array = [None] * nbars
colors = ['white','gray','black']
#hatches = [' ', ' ', '-', ' ', '/']

# nd speedup
for index in range(0, nbars):
  r_array[index] = ax.bar(0.3 + ind + index*width, [s[index] for s in speedups], width,
      bottom=0.1,
      color=colors[index])

ax.set_ylabel('Speedup')
ax.set_yscale('log')
ax.set_ylim([0,17])
plt.xticks([0.75, 1.75, 2.75, 3.75, 4.75], benchmarks, fontsize=12)
plt.xlim([0, 5.5])

for index in range(0, nbars):
  for rect in r_array[index]:
    if (rect.get_height() >= 1): 
      height = 1.2 * rect.get_height()
    elif (rect.get_height() < 0.2):
      height = rect.get_height() + 0.13
    else: 
      height = rect.get_height() + 0.2

    plt.text(rect.get_x() + rect.get_width()/2.0, height, '%.1f' % rect.get_height(), ha='center', va='bottom', size='smaller')


# Energy 
ax = fig.add_subplot(212)

for index in range(0, nbars):
  r_array[index] = ax.bar(0.3 + ind + index*width, [s[index] for s in energy], width,
  bottom=0.1,
  color=colors[index])

ax.set_ylabel('Power Efficiency')
ax.set_yscale('log')

plt.xticks([0.75, 1.75, 2.75, 3.75, 4.75], benchmarks, fontsize=12)
plt.xlim([0, 5.5])

for index in range(0, nbars):
  for rect in r_array[index]:
    if (rect.get_height() >= 1):
      height = 1.2 * rect.get_height()
    else: 
      height = rect.get_height() + 0.2
    plt.text(rect.get_x() + rect.get_width()/2.0, height, '%.1f' % rect.get_height(), ha='center', va='bottom', size='smaller')

plt.figlegend( (r_array[0][0], r_array[1][0], r_array[2][0]), devices, loc = 'upper center', ncol=5, labelspacing=0. )
plt.show()
