#!/usr/bin/python
import os
from subprocess import call

# Open a file
in_file = open("runs.txt", "rw+")
args = map(str.split, in_file)

out_file = open("runs_completed.txt", "w")

for arg in args:

  # make
  call(["build.sh", "--board", "p385_hpc_d5", "-v 11", "--bsize", arg[0], "--ssize", arg[1]])

  out_file.write("completed: BSIZE: " + arg[0] + ", SSIZE: " + arg[1] +  "\n")

# Close opend file
in_file.close()
out_file.close()
