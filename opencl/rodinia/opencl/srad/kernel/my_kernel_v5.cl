#define REDUCTION_UNROLL 16
#define FADLATENCY 8
#define REDUCE_LATENCY (REDUCTION_UNROLL/2)*FADLATENCY

#define BSIZE 128     // default 128
#define SSIZE 1       // default 4

#define C_LOC_BASE_SIZE  BSIZE
#define C_LOC_SR_SIZE    C_LOC_BASE_SIZE + SSIZE
#define I_BASE_SIZE      2 * BSIZE
#define I_SR_SIZE        I_BASE_SIZE + SSIZE

#define sw_offset        BSIZE
#define sw_offset_n      sw_offset + 1
#define sw_offset_s      sw_offset - 1
#define sw_offset_w      sw_offset + BSIZE
#define sw_offset_e      sw_offset - BSIZE

#include "../main.h"
#include "../../common/opencl_kernel_common.h"

  __attribute__((max_global_work_dim(0)))
__kernel void extract_kernel(long         Ne,
    __global float* RESTRICT I)   
{
  for (long ei = 0; ei < Ne; ++ei)
  {
    I[ei] = exp(I[ei]/255); 
  }
}

//      Compute KERNEL
  __attribute__((max_global_work_dim(0)))
__kernel void compute_kernel(float            lambda,
    int           Nr, 
    int           Nc,
    __global volatile float*  RESTRICT I, // volatile disables Altera's cache 
    __global          float*  RESTRICT c_boundary,
    __global          float*  RESTRICT I_out)
{
  // reduction kernel variables
  float sums = 0, sums2 = 0;  

  // shift register for c_loc to resolve right and bottom dependency locally
  float c_loc_SR[C_LOC_SR_SIZE];
  // shift register for storing values of I to reduce global memory access
  float I_SR[I_SR_SIZE]; 

  long Ne = Nr * Nc;

  #pragma unroll REDUCTION_UNROLL
  for (long i = 0; i < Ne; ++i) {
    sums += I[i];
    sums2 += I[i] * I[i];
  }


  float mean   = sums / Ne;
  float var    = (sums2 / Ne) - mean * mean;
  float q0sqr  = var / (mean * mean);

  // SRAD Compute

  // initializing shift registers
#pragma unroll
  for (int i = 0; i < I_SR_SIZE; i++)
  {
    I_SR[i] = 0;
  }

#pragma unroll
  for (int i = 0; i < C_LOC_SR_SIZE; i++)
  {
    c_loc_SR[i] = 0;
  }

  // number of blocks is calculated by dividing column size by BSIZE and 
  // adding one if not divisible
  int block_num = (Nr % BSIZE == 0) ? Nr / BSIZE : (Nr / BSIZE) + 1;

  // the below loop on blocks can be merged into the while loop, but it shouldn't
  // the only way to guarantee that c_boundary will be written in one block,
  // before it is read in the next, is for blocks to be processed sequentially
  // this only happens if the loop on blocks is outside of the while loop
  for (int i = block_num - 1; i >= 0; --i)
  {
    int col = Nc; 
    // start from bottom row in the block
    int row_offset = BSIZE - 1; 

    #pragma ivdep array(c_boundary) // to fix false dependency on the c_boundary array
    do
    {
      float boundary;

      // shift both shift registers by SSIZE
      #pragma unroll
      for (int j = 0; j < I_BASE_SIZE; j++)
      {
        I_SR[j] = I_SR[j + SSIZE];
      }

      #pragma unroll
      for (int j = 0; j < C_LOC_BASE_SIZE; j++)
      {
        c_loc_SR[j] = c_loc_SR[j + SSIZE];
      }

      #pragma unroll
      for (int j = 0; j < SSIZE; j++)
      {
        // row number in the current block
        uint block_row = row_offset - j;

        // real global row number
        uint row = i * BSIZE + block_row;

        if (row < Nr && col != 0)   
        {
          ulong readoffset = (col - 1) * Nr + row;

          // read new values from memory, 
          // reads column i-1 (west dependency) when computing column i
          I_SR[I_BASE_SIZE + j] = I[readoffset]; 
        }

        if (row < Nr && col != Nc)  // avoid going out-of-bounds in either axis
        {
          float dN, dS, dW, dE;  // original SRAD1 kernel temp variables
          float cC, cN, cS, cW, cE;  // original SRAD2 kernel temp variables
          ulong ei = col * Nr + row;

          // directional derivatives, ICOV, diffusion coefficient
          cC = I_SR[sw_offset + j];   // current index

          // directional derivatives
          // north direction derivative
          if (row == 0)   
          {
            dN = 0; // I[ei] - I[ei]
          }
          // top block boundary, 
          //avoid using block_row == 0 to prevent extra off-chip memory read ports
          else if (row_offset == SSIZE - 1 && j == SSIZE - 1) 
          {
            dN = I[ei - 1] - cC; // read from off-chip memory
          }
          else
          {
            dN = I_SR[sw_offset_n + j] - cC; // read from shift register
          }

          // south direction derivative
          if (row == Nr - 1)  // bottom image border
          {
            dS = 0; // I[ei] - I[ei]
          }
          // bottom block boundary, 
          // avoid using block_row == BSIZE - 1 
          // to prevent extra off-chip memory read ports
          else if (row_offset == BSIZE - 1 && j == 0) 
          {
            dS = I[ei + 1] - cC; // read from off-chip memory
          }
          else
          {
            dS = I_SR[sw_offset_s + j] - cC; // read from shift register
          }

          // west direction derivative
          if (col == 0)   // left image border
          {
            dW = 0; // I[ei] - I[ei]
          }
          else
          {
            dW = I_SR[sw_offset_w + j] - cC;// read from shift register
          }

          // east direction derivative
          if (col == Nc - 1)  // right image border
          {
            dE = 0; // I[ei] - I[ei]
          }
          else
          {
            dE = I_SR[sw_offset_e + j] - cC;// read from shift register
          }

          // normalized discrete gradient mag squared (equ 52,53)
          float G2 = (dN * dN + dS * dS + dW * dW + dE * dE) / (cC * cC);

          // normalized discrete laplacian (equ 54)
          float L = (dN + dS + dW + dE) / cC;

          // ICOV (equ 31/35)
          float num  = (0.5 * G2) - ((1.0 / 16.0) * (L * L));
          float denl = 1 + (0.25 * L);   
          float qsqr = num / (denl * denl);  

          // diffusion coefficient (equ 33)
          float denq = (qsqr - q0sqr) / (q0sqr * (1 + q0sqr));
          float c_loc = 1.0 / (1.0 + denq);  

          // clamp diffusion coefficient if out of 0-1 range
          if (c_loc < 0)
          {
            c_loc = 0;
          }
          else if (c_loc > 1)
          {
            c_loc = 1;
          }

          // write new value to shift register to be reused later
          c_loc_SR[C_LOC_BASE_SIZE + j] = c_loc;

          // diffusion coefficient
          // south diffusion coefficient
          if (row == Nr - 1)  // bottom image boundary
          {
            cS = c_loc;
          }
          // bottom block boundary, 
          // avoid using block_row == BSIZE - 1 to prevent 
          // extra off-chip memory read ports
          else if (row_offset == BSIZE - 1 && j == 0) 
          {
            cS = c_boundary[col];   // read from the extra boundary buffer
          }
          else
          {
            cS = c_loc_SR[sw_offset_s + j]; // read from shift register
          }

          // east diffusion coefficient
          if (col == Nc - 1)  // right image boundary
          {
            cE = c_loc;
          }
          else
          {
            cE = c_loc_SR[sw_offset_e + j]; // read from shift register
          }

          // divergence (equ 58)
          float D = c_loc * dN + cS * dS + c_loc * dW + cE * dE;

          // updates image based on input time step and divergence (equ 61)
          I_out[ei] = cC + 0.25 * lambda * D;
          // top block boundary, 
          // avoid using block_row == 0 to prevent extra read ports
          if (row_offset == SSIZE - 1 && j == SSIZE - 1) 
          {
            // make a backup from this value to be later written to off-chip memory
            boundary = c_loc;   
          }
        }
      }

      row_offset = row_offset - SSIZE;// go to next chunk
      if (row_offset == -1)   // top of the block
      {
        row_offset = BSIZE - 1; // reset row_offset
  
        // write backup value to off-chip memory at 
        // the top of the block for resolving south dependency in the upper block
        c_boundary[col] = boundary; 
        col--;  // move to next column
      }
    } while (col >= 0);
  }
}

//  Compress KERNEL
  __attribute__((max_global_work_dim(0)))
__kernel void compress_kernel(long Ne,
    __global float* RESTRICT I)
{
  for (long i = 0; i < Ne; ++i)
  {
    I[i] = log(I[i])*255;   // exponentiate input IMAGE and copy to output image
  }
}
