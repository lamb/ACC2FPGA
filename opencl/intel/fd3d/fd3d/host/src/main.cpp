// Copyright (C) 2013-2016 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

///////////////////////////////////////////////////////////////////////////////////
// This OpenCL application executs a 3D Finite Difference Stencil Computation 
// on an Altera FPGA. The kernel is defined in a device/fd3d.cl file.  The Altera 
// Offline Compiler tool ('aoc') compiles the kernel source into a 'fd3d.aocx' 
// file containing a hardware programming image for the FPGA.  The host program 
// provides the contents of the .aocx file to the clCreateProgramWithBinary OpenCL
// API for runtime programming of the FPGA.
//
// When compiling this application, ensure that the Altera SDK for OpenCL
// is properly installed.
///////////////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <cstring>
#include "CL/opencl.h"
#include "AOCLUtils/aocl_utils.h"

// Source the geometry restrictions (sizex and RADIUS)
#include "fd3d_config.h"

//#define DEFAULT_SIZEX 504
//#define DEFAULT_SIZEY 504
//#define DEFAULT_SIZEZ 504
#define DEFAULT_SIZEX 64 
#define DEFAULT_SIZEY 64
#define DEFAULT_SIZEZ 64

using namespace aocl_utils;

#define STRING_BUFFER_LEN 1024

// ACL runtime configuration
static cl_platform_id platform = NULL;
static cl_device_id device = NULL;
static cl_context context = NULL;
static cl_command_queue queue = NULL;
static cl_kernel kernel = NULL;
static cl_program program = NULL;
static cl_int status = 0;

// Function prototypes
bool init();
void cleanup();
int coord(int x, int y, int z);
bool fd3d_gold(int time_steps);


// Host memory buffers
float *h_dataA, *h_dataB;
float *h_verifyA, *h_verifyB;
float *h_coeff;

// Device memory buffers
cl_mem d_dataA, d_dataB, d_coeff;

int sizex = DEFAULT_SIZEX, sizey = DEFAULT_SIZEY, sizez = DEFAULT_SIZEZ;

// Entry point.
int main(int argc, char **argv) {
  Options options(argc, argv);

  if(!init()) {
    return false;
  }

  // Options.
  int time_steps = 100;
  if(options.has("t")) {
    time_steps = options.get<int>("t");
  }

  if(options.has("x")) {
    sizex = options.get<int>("x");
  }
  if(options.has("y")) {
    sizey = options.get<int>("y");
  }
  if(options.has("z")) {
    sizez = options.get<int>("z");
  }

  // Options validation.
  if (time_steps <= 0 || sizex <= 2 * RADIUS || sizey <= 2 * RADIUS || sizez <= 2 * RADIUS) {
    printf("ERROR: Invalid arguments\n\nUsage: %s [n [x y z]]\n\tn: number of time_steps to run (default 5)\n"
                "\tx, y, z: volume dimensions on each axis (default %d x %d x %d, min %d x %d x %d)\n", 
                argv[0], DEFAULT_SIZEX, DEFAULT_SIZEY, DEFAULT_SIZEZ, 2 * RADIUS + 1, 2 * RADIUS + 1, 2 * RADIUS + 1);
    return false;
  }
  printf("Volume size: %d x %d x %d\n", sizex, sizey, sizez);

  // Allocate host memory
  h_dataA = (float *)alignedMalloc(sizeof(float) * sizex * sizey * sizez);
  h_dataB = (float *)alignedMalloc(sizeof(float) * sizex * sizey * sizez);
  h_verifyA = (float *)alignedMalloc(sizeof(float) * sizex * sizey * sizez);
  h_verifyB = (float *)alignedMalloc(sizeof(float) * sizex * sizey * sizez);
  h_coeff = (float *)alignedMalloc(sizeof(float) * (RADIUS + 1));
  if (!(h_dataA && h_dataB && h_verifyA && h_verifyB && h_coeff)) {
    printf("ERROR: Couldn't create host buffers\n");
    return false;
  }

  printf("Launching an order-%d stencil computation for %d time steps\n", RADIUS * 2, time_steps);

  // Initialize input
  for (int i = 0; i < RADIUS + 1; i++) {
    h_coeff[i] = (float)((double)rand() / (double)RAND_MAX);
  }
  for (int i = 0; i < sizex * sizey * sizez; i++) {
    h_verifyA[i] = h_dataA[i] = (float)((double)rand() / (double)RAND_MAX);
  }

  // Create device buffers - assign the buffers in different banks for more efficient
  // memory access 
 
  d_dataA = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(float) * sizex * sizey * sizez, NULL, &status);
  checkError(status, "Failed to allocate input device buffer\n");
  d_dataB = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_BANK_2_ALTERA, sizeof(float) * sizex * sizey * sizez, NULL, &status);
  checkError(status, "Failed to allocate output device buffer\n");
  d_coeff = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float) * (RADIUS + 1), NULL, &status);
  checkError(status, "Failed to allocate constant device buffer\n");

  // Copy data from host to device

  status = clEnqueueWriteBuffer(queue, d_dataA, CL_TRUE, 0, sizeof(float) * sizex * sizey * sizez, h_dataA, 0, NULL, NULL);
  checkError(status, "Failed to copy data to device");

  status = clEnqueueWriteBuffer(queue, d_coeff, CL_TRUE, 0, sizeof(float) * (RADIUS + 1), h_coeff, 0, NULL, NULL);
  checkError(status, "Failed to copy data to device");

  // Set the kernel arguments
  status = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&d_coeff);
  checkError(status, "Failed to set kernel arg 2");
  status = clSetKernelArg(kernel, 3, sizeof(int), (void *)&sizex);
  checkError(status, "Failed to set kernel arg 3");
  status = clSetKernelArg(kernel, 4, sizeof(int), (void *)&sizey);
  checkError(status, "Failed to set kernel arg 4");
  status = clSetKernelArg(kernel, 5, sizeof(int), (void *)&sizez);
  checkError(status, "Failed to set kernel arg 5");

  // Get the time_stepstamp to evaluate performance
  double time = getCurrentTimestamp();

  for (int i = 0; i < time_steps; i++) {
    status = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)(i % 2 ? &d_dataA : &d_dataB));
    status = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)(i % 2 ? &d_dataB : &d_dataA));
    // Launch the kernel - we launch a single work item hence enqueue a task
    status = clEnqueueTask(queue, kernel, 0, NULL, NULL);
    checkError(status, "Failed to launch kernel");
    clFinish(queue);
  }

  status = clFinish(queue);
  checkError(status, "Failed to finish");

  // Record execution time
  time = getCurrentTimestamp() - time;

  // Copy results from device to host
  status = clEnqueueReadBuffer(queue, time_steps % 2 ? d_dataB : d_dataA, CL_TRUE, 0, sizeof(float) * sizex * sizey * sizez, h_dataB, 0, NULL, NULL);
  checkError(status, "Failed to copy data from device");
  printf("\tProcessing time = %.4fms\n", (float)(time * 1E3));
  double gpoints_per_sec = ((double) sizex * sizey * sizez / (time / time_steps)) * 1E-9;
  printf("\tThroughput = %.4f Gpoints / sec\n", gpoints_per_sec);

  printf("\tVerifying data --> %s\n\n", fd3d_gold(time_steps) ? "PASSED" : "FAILED");

  // Free the resources allocated
  cleanup();

  return 0;
}


/////// HELPER FUNCTIONS ///////

int coord(int x, int y, int z) {
   return z * sizex * sizey + y * sizex + x;
}

bool fd3d_gold(int time_steps) {
  float *from, *to;
  for (int i = 0; i < time_steps; i++) {
    from = i % 2 ? h_verifyB : h_verifyA;
    to = i % 2 ? h_verifyA : h_verifyB;
    for (int z = 0; z < sizez; z++) {
      for (int y = 0; y < sizey; y++) {
        for (int x = 0; x < sizex; x++) {
           if (x >= RADIUS && x < sizex - RADIUS &&
               y >= RADIUS && y < sizey - RADIUS &&
               z >= RADIUS && z < sizez - RADIUS) {
             float result = h_coeff[0] * from[coord(x, y, z)];
             for (int r = 1 ; r <= RADIUS; r++) {
               result += h_coeff[r] * from[coord(x - r, y, z)];
               result += h_coeff[r] * from[coord(x + r, y, z)];
               result += h_coeff[r] * from[coord(x, y - r, z)];
               result += h_coeff[r] * from[coord(x, y + r, z)];
               result += h_coeff[r] * from[coord(x, y, z - r)];
               result += h_coeff[r] * from[coord(x, y, z + r)];
             }
             to[coord(x, y, z)] = result;
           } else {
             to[coord(x, y, z)] = from[coord(x, y, z)];
           }
        }
      }
    }
  }
  for (int z = 0; z < sizez; z++) {
    for (int y = 0; y < sizey; y++) {
      for (int x = 0; x < sizex; x++) {
        float delta = h_dataB[coord(x, y, z)] - to[coord(x, y, z)];
        float error = to[coord(x, y, z)] != 0 ? fabs(delta) / to[coord(x, y, z)] : fabs(delta);
        if (error >  1e-4) {
           printf("%d %d %d %f %f\n", x, y, z, h_dataB[coord(x, y, z)], to[coord(x, y, z)]);
           return false;
        }
      }
    }
  }
  return true;
}

bool init() {
  cl_int status;

  if(!setCwdToExeDir()) {
    return false;
  }

  // Get the OpenCL platform.
  platform = findPlatform("Intel");
  if(platform == NULL) {
    printf("ERROR: Unable to find Intel OpenCL platform\n");
    return false;
  }

  // Query the available OpenCL devices.
  scoped_array<cl_device_id> devices;
  cl_uint num_devices;

  devices.reset(getDevices(platform, CL_DEVICE_TYPE_ALL, &num_devices));

  // We'll just use the first device.
  device = devices[0];

  // Create the context.
  context = clCreateContext(NULL, 1, &device, &oclContextCallback, NULL, &status);
  checkError(status, "Failed to create context");

  // Create the command queue.
  queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  checkError(status, "Failed to create command queue");

  // Create the program.
  std::string binary_file = getBoardBinaryFile("fd3d", device);
  printf("Using AOCX: %s\n\n", binary_file.c_str());
  program = createProgramFromBinary(context, binary_file.c_str(), &device, 1);

  // Build the program that was just created.
  status = clBuildProgram(program, 0, NULL, "", NULL, NULL);
  checkError(status, "Failed to build program");

  // Create the kernel - name passed in here must match kernel name in the
  // original CL file, that was compiled into an AOCX file using the AOC tool
  const char *kernel_name = "FiniteDifferences";  // Kernel name, as defined in the CL file
  kernel = clCreateKernel(program, kernel_name, &status);
  checkError(status, "Failed to create kernel");

  return true;
}

// Free the resources allocated during initialization
void cleanup() {
  if(kernel) 
    clReleaseKernel(kernel);  
  if(program) 
    clReleaseProgram(program);
  if(queue) 
    clReleaseCommandQueue(queue);
  if(context) 
    clReleaseContext(context);
  if(h_dataA)
	alignedFree(h_dataA);
  if (h_dataB)
	alignedFree(h_dataB);
  if (h_verifyA)
	alignedFree(h_verifyA);
  if (h_verifyB)
	alignedFree(h_verifyB);
  if (h_coeff)
	alignedFree(h_coeff);
  if (d_dataA)
	clReleaseMemObject(d_dataA);
  if (d_dataB)
	clReleaseMemObject(d_dataB);
  if (d_coeff)
	clReleaseMemObject(d_coeff);
}



