// Copyright (C) 2013-2016 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

/* This is the device source file for the fd3d example. The code is written as
 * an OpenCL single work-item kernel. This coding style allows the compiler to
 * extract loop-level parallelism from the source code and instantiate a
 * hardware pipeline capable of executing concurrently a large number of loop
 * iterations. The compiler analyses the loop-carried dependencies, and these
 * translate into data transfers across concurrently executed loop iterations. 
 * 
 * Careful coding ensures that all loop-carried dependencies are trivial, 
 * merely data transfers, which span a single clock cycle. This ensures an 
 * overall processing throughput of one loop iteration per clock cycle.
 *
 * The algorithm linearizes the input volume and slides it through a window
 * backed by an on-chip array. For a volume of size 3, the sequence of inputs
 * would be: x0y0z0, x1y0z0, x2y0z0, x0,y1,z0 ... x2,yz,z0, x0,y0,z1 ...
 * The algorithm leverages the property that accesses on the same column are 
 * TILEX elements apart, while elements on different planes are TILEX * TILEY 
 * elements apart. Given a reference offset 'r' in the window array,
 * corresponding to coordinates (x, y, z), the following offsets would provide
 * points adjacent on all directions:
 *
 *       r - n                (x - n, y, z)
 *       r + n                (x + n, y, z)
 *       r - n * dimx         (x, y - n, z)
 *       r + n * dimx         (x, y + n, z)
 *       r - n * dimx * dimy  (x, y, z - n)
 *       r + n * dimx * dimy  (x, y, z + n)
 *
 * Using a sliding volume reduces the on-chip memory (state) requirement from 
 * dimx * dimy * dimz to (order * dimx * dimy). The implementation utilizes 
 * tiling on 'y' axis to further reduce the on-chip storage requirement. 
 */

// Source the radius from a header shared with the host code
#include "../host/inc/fd3d_config.h" 

// Define a window through which we slide the volume
// Volumes with x and y dimensions of size k * DIMX - 2 * (k - 1) * RADIUS;
// utilize the window efficiently
//#define DIMX 256 
//#define DIMY 256
#define DIMX 64
#define DIMY 64

// specifies how many points to compute in parallel
// DIMX should be a multiple of PAR_POINTS
//#define PAR_POINTS 16
//#define PAR_POINTS 16 

/* Attaching the attribute 'task' to the top level kernel to indicate
 * that the host enqueues a task (a single work-item kernel)
 *
 *'input' and 'output' point to the corresponding volumes in global memory;
 * using restrict pointers as there are no dependencies between buffers
 *
 * 'coeff' are the filter coefficients, in constant memory
 * 'dimy' and 'dimz' specify the size of the non-restricted dimensions of 
 * the volume to process ('dimx' is the compile-time constant DIMX)
 */

__attribute__((task))
kernel void FiniteDifferences(global float * restrict output, 
                              global const float * restrict input,
                              constant float *coeff, 
                              const int dimx, const int dimy, const int dimz) {

  /* Use a window of size 2 * RADIUS * DIMX * DIMY + PAR_POINTS to slide a volume
   * of size DIMX * DIMY * dimz; the linearized volume will slide through this 
   * window, ensuring access to the 3k + 1 neighboring points required to compute 
   * each output point
   *
   * The sliding window 'taps' is centered on the current PAR_POINTS processed 
   * in parallel, and must be long enough to span the following points (i.e. 
   * for RADIUS = 3, x = DIMX, y = DIMY):
   *     -3xy -2xy -xy -3x -2x -x -3 -2 -1 0 1 2 3 x 2x 3x xy 2xy 3xy
   */

   float taps[2 * RADIUS * DIMX * DIMY + PAR_POINTS];

   #pragma unroll
   for (int i = 0; i < 2 * RADIUS * DIMX * DIMY + PAR_POINTS; i++) {
     taps[i] = 0;
   }

   /* Iterate over the entire volume in a single loop, sliding each subvolume 
    * (and its halos) through the sliding window. 'xtile', 'ytile' and 'ztile' 
    * represent coordinates in the current tile. As the tile spans the entire 
    * array on 'z' axis, only 'x' and 'y' are necessary to represent the 
    * coordinate of the tile in the entire volume
    */

   int x = 0, y = 0, xtile = 0, ytile = 0, ztile = 0;
   do {
     /* First, shift taps in the shift register to make room for the new points.
      * Fully unroll the loop, the compiler is capable of shifting the contents 
      * of the entire 'taps' register in parallel
      */

     #pragma unroll
     for (int i = 0; i < 2 * RADIUS * DIMX * DIMY; i++) {
       taps[i] = taps[i + PAR_POINTS];
     }

     // Determine the coordinate of the point to read from the input volume
     int inOffset = ztile * dimx * dimy + (y + ytile) * dimx + (x + xtile);

     // This loop is unrolled to read PAR_POINTS in parallel 
     #pragma unroll
     for (int i = 0; i < PAR_POINTS; i++) {
       // read new input - if valid
       if (inOffset + i >= 0 && inOffset + i < dimx * dimy * dimz) {
         taps[2 * RADIUS * DIMX * DIMY + i] = input[inOffset + i];
       }
     }

     /* Compute a new output value, using the stencil equation, based on the 
      * points currently available in the sliding window - fully unroll this 
      * computation to process PAR_POINTS in parallel
      */
     float value[PAR_POINTS];
     #pragma unroll
     for (int j = 0; j < PAR_POINTS; j++) {
       value[j] = coeff[0] * taps[RADIUS * DIMX * DIMY + j];

       /* Add RADIUS points from each direction. Adjacent 'y' points are TILEX 
        * points appart. Adjacent 'z' points are TILEX * TILEY points appart
        */ 
       #pragma unroll
       for (int i = 1; i <= RADIUS; i++) {
         value[j] += coeff[i] * (taps[(RADIUS - i) * DIMX * DIMY + j] + 
                                 taps[(RADIUS + i) * DIMX * DIMY + j] + 
                                 taps[DIMX * (RADIUS * DIMY - i) + j] + 
                                 taps[DIMX * (RADIUS * DIMY + i) + j] + 
                                 taps[RADIUS * DIMX * DIMY - i + j] + 
                                 taps[RADIUS * DIMX * DIMY + i + j]);
       }
     }

     /* Compute the coordinates of the output point. As a volume is streamed 
      * through the 'taps' array, the input is RADIUS planes ahead of the 
      * output - adjust the z coordinate accordingly
      */
     int xoutput = x + xtile, youtput = y + ytile, zoutput = ztile - RADIUS;
     int outOffset = zoutput * dimx * dimy + youtput * dimx + xoutput;

     #pragma unroll 
     for (int i = 0; i < PAR_POINTS; i++) {

       // Halo range on each axis - don't write to tile halo but write to volume halo (copy the input)
       bool haloX = ((xoutput + i < RADIUS) || (xoutput + i >= dimx - RADIUS));
       bool haloY = ((youtput < RADIUS) || (youtput >= dimy - RADIUS));
       bool haloZ = ((zoutput >= 0 && zoutput < RADIUS) || (zoutput >= dimz - RADIUS));

       /* Compute the output address - check if the output is halo and, if not,
        * replace the output with the input value
        */
       if ((xtile + i)>= RADIUS && (xtile + i) < (DIMX - RADIUS) && xoutput + i < dimx - RADIUS &&
                ytile >= RADIUS && ytile < (DIMY - RADIUS) && youtput < dimy - RADIUS &&
                ztile >= 2 * RADIUS && ztile < dimz) {
         output[outOffset + i] = value[i];
       } else if ((haloX || haloY || haloZ) && ztile >= RADIUS && xoutput + i < dimx && youtput < dimy ) {
         output[outOffset + i] = taps[RADIUS * DIMX * DIMY + i];
       }
     }

     // Determine the next point to compute - increment the coordinates
     xtile = xtile < DIMX - PAR_POINTS ? xtile + PAR_POINTS : 0;
     ytile = xtile == 0 ? ytile < DIMY - 1 ? ytile + 1 : 0 : ytile;
     ztile = xtile == 0 && ytile == 0 ? ztile < dimz + RADIUS - 1 ? ztile + 1 : 0 : ztile;

     // If outside tile boundaries, increment coordinates in the global volume
     bool intile = (xtile != 0 || ytile != 0 || ztile != 0);
     x =  intile ? x : x < dimx - DIMX ? x + DIMX - 2 * RADIUS : 0;
     y = intile ? y : x == 0 ? y + DIMY - 2 * RADIUS : y;
  } while (y < dimy - 2 * RADIUS);
}
