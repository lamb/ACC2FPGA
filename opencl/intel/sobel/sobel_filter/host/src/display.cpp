// Copyright (C) 2013-2017 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.


#include <CL/opencl.h>
#include <algorithm>
#include <iostream>
#include "parse_ppm.h"
#include "defines.h"
#include "AOCLUtils/aocl_utils.h"

using namespace aocl_utils;

bool useFilter = false;
unsigned int thresh = 128;
int fps_raw = 0;

cl_uint *input = NULL;
cl_uint *output = NULL;

cl_uint num_platforms;
cl_platform_id platform;
cl_uint num_devices;
cl_device_id device;
cl_context context;
cl_command_queue queue;
cl_program program;
cl_kernel kernel;
cl_mem in_buffer, out_buffer;

std::string imageFilename;
std::string aocxFilename;
std::string deviceInfo;

static bool profile = true;
bool useDisplay = true;
bool testMode = false;
unsigned testThresholds[] = {32, 96, 128, 192, 225};
unsigned testFrameIndex = 0;

void teardown(int exit_status = 1);
void initCL();
void dumpFrame(unsigned frameIndex, unsigned *frameData);

void filter()
{

  //double time_0, time_s;

  size_t sobelSize = 1;
  cl_int status;

  status = clEnqueueWriteBuffer(queue, in_buffer, CL_FALSE, 0, sizeof(unsigned int) * ROWS * COLS, input, 0, NULL, NULL);
  checkError(status, "Error: could not copy data into device");

  status = clFinish(queue);
  checkError(status, "Error: could not finish successfully");

  //time_0 = getCurrentTimestamp();

  if(!testMode) {
    status = clSetKernelArg(kernel, 3, sizeof(unsigned int), &thresh);
  }
  else {
    // In test mode, iterate through different thresholds automatically.
    status = clSetKernelArg(kernel, 3, sizeof(unsigned int), &testThresholds[testFrameIndex]);
  }
  checkError(status, "Error: could not set sobel threshold");

  cl_event event;
  status = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &sobelSize, &sobelSize, 0, NULL, &event);
  checkError(status, "Error: could not enqueue sobel filter");

  status  = clFinish(queue);
  checkError(status, "Error: could not finish successfully");

  //time_s = (getCurrentTimestamp() - time_0);
  //printf("sobel %d time:\t%.3fs\n", thresh, time_s);

  cl_ulong start, end;
  status  = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, NULL);
  status |= clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, NULL);
  checkError(status, "Error: could not get profile information");
  clReleaseEvent(event);

  //fps_raw = (int)(1.0f / ((end - start) * 1e-9f));
  //printf("Throughput: %d FPS\n", fps_raw);
  float runtime = end - start;
  printf("kernel runtime: %fs\n", runtime / 10e8f);

  status = clEnqueueReadBuffer(queue, out_buffer, CL_FALSE, 0, sizeof(unsigned int) * ROWS * COLS, output, 0, NULL, NULL);
  checkError(status, "Error: could not copy data from device");

  status = clFinish(queue);
  checkError(status, "Error: could not successfully finish copy");

  if(testMode) {
    // Dump out frame data in PPM (ASCII).
    dumpFrame(testFrameIndex, output);

    // Increment test frame index.
    testFrameIndex++;
  }
  else {
    dumpFrame(testFrameIndex, output);
  }

}

// Dump frame data in PPM format.
void dumpFrame(unsigned frameIndex, unsigned *frameData) {
  char fname[256];
  sprintf(fname, "frame%d.ppm", frameIndex);
  printf("Dumping %s\n", fname);

  FILE *f = fopen(fname, "wb");
  fprintf(f, "P6 %d %d %d\n", COLS, ROWS, 255);
  for(unsigned y = 0; y < ROWS; ++y) {
    for(unsigned x = 0; x < COLS; ++x) {
      // This assumes byte-order is little-endian.
      unsigned pixel = frameData[y * COLS + x];
      fwrite(&pixel, 1, 3, f);
    }
    //fprintf(f, "\n");
  }
  fclose(f);
}

int main(int argc, char **argv)
{
  Options options(argc, argv);
  profile = options.has("profile");

  imageFilename = "../butterflies.ppm";
  if(options.has("img")) {
    imageFilename = options.get<std::string>("img");
  }

  // If in test mode, start with useFilter = true.
  if(options.has("test")) {
    useFilter = true;
    testMode = true;
  }

  input = (cl_uint*)alignedMalloc(sizeof(unsigned int) * ROWS * COLS);
  output = (cl_uint*)alignedMalloc(sizeof(unsigned int) * ROWS * COLS);

  initCL();

  // Read the image
  if (!parse_ppm(imageFilename.c_str(), COLS, ROWS, (unsigned char *)input)) {
    std::cerr << "Error: could not load " << argv[1] << std::endl;
    teardown();
  }

  filter();
 
  teardown(0);
}

void initCL()
{
  cl_int status;

  if (!setCwdToExeDir()) {
    teardown();
  }

  platform = findPlatform("Intel");
  if (platform == NULL) {
    printf("COULD NOT FIND PLATFORM\n");
    teardown();
  }

  status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 1, &device, NULL);
  checkError (status, "Error: could not query devices");
  num_devices = 1; // always only using one device

  char info[256];
  clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(info), info, NULL);
  deviceInfo = info;

  context = clCreateContext(0, num_devices, &device, &oclContextCallback, NULL, &status);
  checkError(status, "Error: could not create OpenCL context");

  queue = clCreateCommandQueue(context, device, 0, &status);
  checkError(status, "Error: could not create command queue");

  std::string binary_file = getBoardBinaryFile("sobel", device);
  std::cout << "Using AOCX: " << binary_file << "\n";
  program = createProgramFromBinary(context, binary_file.c_str(), &device, 1);

  status = clBuildProgram(program, num_devices, &device, "", NULL, NULL);
  checkError(status, "Error: could not build program");

  kernel = clCreateKernel(program, "sobel", &status);
  checkError(status, "Error: could not create sobel kernel");

  in_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(unsigned int) * ROWS * COLS, NULL, &status);
  checkError(status, "Error: could not create device buffer");

  out_buffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(unsigned int) * ROWS * COLS, NULL, &status);
  checkError(status, "Error: could not create output buffer");

  int pixels = COLS * ROWS;

  status = clSetKernelArg(kernel, 0, sizeof(cl_mem), &in_buffer);
  checkError(status, "Error: could not set sobel arg 0");
  status = clSetKernelArg(kernel, 1, sizeof(cl_mem), &out_buffer);
  checkError(status, "Error: could not set sobel arg 1");

  status = clSetKernelArg(kernel, 2, sizeof(int), &pixels);
  checkError(status, "Error: could not set sobel arg 2");
}

void cleanup()
{
  // Called from aocl_utils::check_error, so there's an error.
  teardown(-1);
}

void teardown(int exit_status)
{
  if (input) alignedFree(input);
  if (output) alignedFree(output);
  if (in_buffer) clReleaseMemObject(in_buffer);
  if (out_buffer) clReleaseMemObject(out_buffer);

  if (kernel) clReleaseKernel(kernel);
  if (program) clReleaseProgram(program);
  if (queue) clReleaseCommandQueue(queue);
  if (context) clReleaseContext(context);

  exit(exit_status);
}

