#include "srad.h"

extern int grid_rows, grid_cols, size_I, size_R, niter;
extern int r1, r2, c1, c2;
extern float lambda;
extern int nthreads;

#if MAN_SW_MULTI 

#define SSIZE   (8)
#define SSIZE_2 (8)

// iN, iS, jE, jW unused in this version
void mainComp(float J[_SIZE_I_],
    int iN[_ROWS_], int iS[_ROWS_],
    int jE[_COLS_], int jW[_COLS_], float dN[_SIZE_I_], float dS[_SIZE_I_],
    float dW[_SIZE_I_], float dE[_SIZE_I_], float c[_SIZE_I_])
{
  float sum=0, sum2=0;     

  #pragma acc parallel num_gangs(1) num_workers(1) \
    copy(sum, sum2) present(J) copyin(r1, r2, c1, c2, grid_cols)
  {
    for (int i = r1; i <= r2; i++) {
      for (int j = c1; j <= c2; j++) {
        float tmp   = J[i * grid_cols + j];
        sum  += tmp ;
        sum2 += tmp*tmp;
      }
    }
  }

  float meanROI = sum / size_R;
  float varROI  = (sum2 / size_R) - meanROI*meanROI;
  float q0sqr   = varROI / (meanROI*meanROI);

  #pragma acc parallel num_gangs(1) num_workers(1) \
      copyin(q0sqr, grid_rows, grid_cols)  present(J, dN, dS, dE, dW, c)
  {
    #define TARGET (_COLS_)
    #define SW_BASE_SIZE (2*_COLS_)
    #define SW_SIZE (SW_BASE_SIZE + SSIZE)

    float sw_J[SW_SIZE];
    #pragma unroll
    for (int ii = 0; ii < SW_SIZE; ++ii) sw_J[ii] = 0; 

    int i = -1, j = 0;
    #pragma ivdep
    for (int iter = TARGET - SW_BASE_SIZE; iter < grid_rows*grid_cols; iter += SSIZE) {

      #pragma unroll
      for (int ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw_J[ii] = sw_J[ii + SSIZE];
      }

      int read_offset = iter + SW_BASE_SIZE - TARGET;
      #pragma unroll
      for (int ii = 0; ii < SSIZE; ++ii) {
        if (read_offset + ii >= 0 && read_offset + ii < grid_rows*grid_cols) 
          sw_J[SW_BASE_SIZE + ii] = J[read_offset + ii];
        else
          sw_J[SW_BASE_SIZE + ii] = 0;
      }

      float value[SSIZE];

      // Main computation
      if (iter >= 0) {
  
        #pragma unroll
        for (int ss = 0; ss < SSIZE; ++ss) {
          int k = i * grid_cols + j+ss;

          float Jc = sw_J[TARGET+ss + 0];

          // directional derivates
          if( i == 0 ) {
            dN[k] = sw_J[TARGET+ss + 0] - Jc;
            dS[k] = sw_J[TARGET+ss + _COLS_ + 0] - Jc;
          } else if( i == (grid_rows-1) ) {
            dN[k] = sw_J[TARGET+ss + (0-1) * _COLS_ + 0] - Jc;
            dS[k] = sw_J[TARGET+ss + 0] - Jc;
          } else {
            dN[k] = sw_J[TARGET+ss + (0-1) * _COLS_ + 0] - Jc;
            dS[k] = sw_J[TARGET+ss + (0+1) * _COLS_ + 0] - Jc;
          }
          if( j+ss == 0 ) {	
            dW[k] = sw_J[TARGET+ss + 0 * _COLS_] - Jc;
            dE[k] = sw_J[TARGET+ss + 0 * _COLS_ + 1] - Jc;
          } else if( j+ss == (grid_cols-1) ) {
            dW[k] = sw_J[TARGET+ss + 0 * _COLS_ + (0-1)] - Jc;
            dE[k] = sw_J[TARGET+ss + 0] - Jc;
          } else {
            dW[k] = sw_J[TARGET+ss + 0 * _COLS_ + (0-1)] - Jc;
            dE[k] = sw_J[TARGET+ss + 0 * _COLS_ + (0+1)] - Jc;
          }

          float G2 = (dN[k]*dN[k] + dS[k]*dS[k] 
              + dW[k]*dW[k] + dE[k]*dE[k]) / (Jc*Jc);

          float L = (dN[k] + dS[k] + dW[k] + dE[k]) / Jc;

          float num  = (0.5F*G2) - ((1.0F/16.0F)*(L*L)) ;
          float den  = 1 + (.25F*L);
          float qsqr = num/(den*den);

          // diffusion coefficent (equ 33)
          den = (qsqr-q0sqr) / (q0sqr * (1+q0sqr)) ;
          value[ss] = 1.0F / (1.0F+den) ;

          // saturate diffusion coefficent
          if (value[ss] < 0) {value[ss] = 0;}
          else if (value[ss] > 1) {value[ss] = 1;}
        } // SSIZE

        int k = i * grid_cols + j;

        #pragma unroll
        for (int ii = 0; ii < SSIZE; ++ii) {
          c[k + ii] = value[ii];
        }
  
      }

      j += SSIZE;
      if (j == grid_cols) { i++; j = 0; }
    } // loop
  } // acc

  #pragma acc parallel num_gangs(1) num_workers(1) \
      present(J, dN, dS, dE, dW, c) copyin(grid_cols, grid_rows, lambda)
  {
    #define TARGET_2 (0)
    #define SW_BASE_SIZE_2 (_COLS_)
    #define SW_SIZE_2 (SW_BASE_SIZE_2 + SSIZE_2)

    float sw_c[SW_SIZE_2];
    #pragma unroll
    for (int ii = 0; ii < SW_SIZE_2; ++ii) sw_c[ii] = 0; 

    int i = -1, j = 0;
    #pragma ivdep
    for (int iter = TARGET_2 - SW_BASE_SIZE_2; iter < grid_rows*grid_cols; iter+=SSIZE_2) {

      #pragma unroll
      for (int ii = 0; ii < SW_BASE_SIZE_2; ++ii) {
        sw_c[ii] = sw_c[ii + SSIZE_2];
      }

      int read_offset = iter + SW_BASE_SIZE_2 - TARGET_2;
      #pragma unroll
      for (int ii = 0; ii < SSIZE_2; ++ii) {
        if (read_offset + ii >= 0 && read_offset + ii < grid_rows*grid_cols)
          sw_c[SW_BASE_SIZE_2 + ii] = c[read_offset + ii];
        else
          sw_c[SW_BASE_SIZE_2 + ii] = 0;
      }

      float value[SSIZE_2];

      if (iter >= 0) {

        #pragma unroll
        for (int ss = 0; ss < SSIZE_2; ++ss) {

          int k = i * grid_cols + j+ss;

          float cN, cS, cW, cE;

          // diffusion coefficent
          cN = sw_c[TARGET_2+ss + 0]; 
          if( i == (grid_rows-1) ) {
            cS = sw_c[TARGET_2+ss + 0];
          } else {
            cS = sw_c[TARGET_2+ss + (0+1) * _COLS_ + 0];
          }
          cW = sw_c[TARGET_2+ss + 0];
          if( j+ss == (grid_cols-1) ) {
            cE = sw_c[TARGET_2+ss + 0];
          } else {
            cE = sw_c[TARGET_2+ss + 0 * _COLS_ + 0+1];
          }

          // divergence (equ 58)
          float D = cN * dN[k] + cS * dS[k] + cW * dW[k] + cE * dE[k];

          // image update (equ 61)
          value[ss] = J[k] + 0.25F*lambda*D;
        } // SSIZE

        int k = i * grid_cols + j;

        #pragma unroll
        for (int ii = 0; ii < SSIZE_2; ++ii) {
          J[k + ii] = value[ii];
        }
      }

      j += SSIZE_2;
      if (j == grid_cols) { i++; j = 0; }
    } 
  } // acc

}
#endif
