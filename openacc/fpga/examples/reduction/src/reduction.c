#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <assert.h>

#include <float.h>
#include <limits.h>
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#define MAX(a,b) (((a) > (b)) ? (a) : (b))

#define ACC_DIR   1   // reduction directive
#define ACC_SWI   0   // single-work item directive
#define ACC_MAN   0   // manual xformation

#define SUM       0
#define PRODUCT   1
#define MAXIMUM   0

#define TYPE double // double, float, int
#define MIN_VALUE (-FLT_MAX)  // DBL_MAX, FLT_MAX, INT_MAX

#define N (4096*4096) 
#pragma openarc #define N (4096*4096) 

double getCurrentTimestamp() {
  struct timespec a;
  clock_gettime(CLOCK_MONOTONIC, &a);
  return ( (double) a.tv_nsec * 1.0e-9) + (double) a.tv_sec;
}

void print_window(TYPE * sw, char const *s, int sw_size) {
  int ii;
  for (ii = 0; ii < sw_size ; ii++) printf("%s[%d] = %f\n", s, ii, sw[ii]);
}

TYPE *input;

void cpu();
void acc();

int main(int argc, char **argv)
{
  int i, j;

  #if SUM
  printf("Running SUM\n");
  #elif PRODUCT
  printf("Running PRODUCT\n");
  #elif MAXIMUM
  printf("Running MAXIMUM\n");
  #endif

  input  = (TYPE *) malloc(sizeof(TYPE)*N);

  for (i = 0; i < N; i++) {
    #if SUM
    input[i] = (TYPE)rand() / (TYPE)RAND_MAX ; 
    #elif PRODUCT
    input[i] = ((TYPE)rand() / (TYPE)RAND_MAX) + 1.0 ; 
    #elif MAXIMUM
    input[i] = ((TYPE)rand() / (TYPE)RAND_MAX) + 5.0 ; 
    #endif
  }

  double acc_start, acc_end;
  #pragma acc data copyin(input[0:N]) 
  {
    printf("\n  Running Manual Accelerator version\n");
    acc_start = getCurrentTimestamp();
    acc();
    acc_end = getCurrentTimestamp();
  }

  double cpu_start, cpu_end;

  printf("\n  Running CPU version\n");

  cpu_start = getCurrentTimestamp();
  cpu();
  cpu_end = getCurrentTimestamp();

  printf("\n  Accelerator Elapsed Time = \t%lf sec\n", acc_end - acc_start);
  printf("  CPU Elapsed Time = \t\t\t%lf sec\n", cpu_end - cpu_start);
}

#if SUM 
void cpu() {
  
  TYPE sum = 0;

  for (int i = 0; i < N; i++) {
    sum += input[i];
  }
  printf("    sum: %f\n", sum);

  return;
}

#if ACC_MAN
void acc() {

  //Sum every element of shift register
  TYPE sum = 0;

  #pragma acc parallel num_gangs(1) num_workers(1) \
     present(input[0:N]) copy(sum)
  {
    #define REG_DEPTH 20

    //Create shift register with II_CYCLE+1 elements
    TYPE shift_reg[REG_DEPTH+1];
  
    //Initialize all elements of the register to 0
    #pragma unroll
    for (int i = 0; i < REG_DEPTH + 1; i++)
    {
      shift_reg[i] = 0;
    }
  
    //Iterate through every element of input array
    #pragma unroll 8
    for(int i = 0; i < N; ++i)
    {
      //Load ith element into end of shift register
      shift_reg[REG_DEPTH] = shift_reg[0] + input[i];
  
      //Shift every element of shift register
      #pragma unroll
      for(int j = 0; j < REG_DEPTH; ++j)
      {
        shift_reg[j] = shift_reg[j + 1];
      }
    }
  
    #pragma unroll 
    for(int i = 0; i < REG_DEPTH; ++i)
    {
      sum += shift_reg[i];
    }

  } // acc

  printf("    sum: %lf\n", sum);

}
#endif

#if ACC_SWI
void acc() {

  //Sum every element of shift register
  TYPE sum = 0;

  #pragma acc parallel num_gangs(1) num_workers(1) present(input) copy(sum)
  {
    for (int i = 0; i < N; i++) {
      sum += input[i];
    }
  }

  printf("    sum: %lf\n", sum);
}
#endif

#if ACC_DIR
void acc() {

  //Sum every element of shift register
  TYPE sum = 0;

#pragma acc parallel loop reduction(+:sum) 
  for (int i = 0; i < N; i++) {
    sum += input[i];
  }

  printf("    sum: %f\n", sum);
}
#endif
#endif

#if PRODUCT 
void cpu() {
  
  TYPE product = 1;

  for (int i = 0; i < N; i++) {
    product *= input[i];
  }
  printf("    product: %lf\n", product);

  return;
}

#if ACC_MAN
void acc() {

  //Sum every element of shift register
  TYPE product = 1;

  #pragma acc parallel num_gangs(1) num_workers(1) \
     present(input[0:N]) copy(product)
  {
    #define REG_DEPTH 0

    //Create shift register with II_CYCLE+1 elements
    TYPE shift_reg[REG_DEPTH+1];
  
    //Initialize all elements of the register to 0
    #pragma unroll
    for (int i = 0; i < REG_DEPTH + 1; i++)
    {
      shift_reg[i] = 1;
    }
  
    //Iterate through every element of input array
    #pragma unroll 1
    for(int i = 0; i < N; ++i)
    {
      //Load ith element into end of shift register
      //if N > II_CYCLE, add to shift_reg[0] to preserve values
      shift_reg[REG_DEPTH] = shift_reg[0] * input[i];
  
      //Shift every element of shift register
      #pragma unroll
      for(int j = 0; j < REG_DEPTH; ++j)
      {
        shift_reg[j] = shift_reg[j + 1];
      }
    }

    #pragma unroll 
    for(int i = 0; i < REG_DEPTH; ++i)
    {
      product *= shift_reg[i];
    }

  } // acc

  printf("    product: %lf\n", product);

}
#endif

#if ACC_DIR
void acc() {

  TYPE product = 1;

  #pragma acc parallel num_gangs(1) num_workers(1) present(input) copy(product)
  {
    #pragma acc loop reduction(*:product) 
    #pragma unroll 8
    for (int i = 0; i < N; i++) {
      product *= input[i];
    }
  }

  printf("    product: %lf\n", product);
}
#endif
#endif


#if MAXIMUM 
void cpu() {
  
  TYPE maxi = MIN_VALUE;

  for (int i = 0; i < N; i++) {
    maxi = MAX(maxi, input[i]);
  }
  printf("    maximum: %lf\n", maxi);

  return;
}

#if ACC_MAN
void acc() {

  //Sum every element of shift register
  TYPE maxi = MIN_VALUE;

  #pragma acc parallel num_gangs(1) num_workers(1) \
     present(input[0:N]) copy(maxi)
  {
    #define REG_DEPTH 0

    //Create shift register with II_CYCLE+1 elements
    TYPE shift_reg[REG_DEPTH+1];
  
    //Initialize all elements of the register to 0
    #pragma unroll
    for (int i = 0; i < REG_DEPTH + 1; i++)
    {
      shift_reg[i] = 1;
    }
  
    //Iterate through every element of input array
    #pragma unroll 1
    for(int i = 0; i < N; ++i)
    {
      //Load ith element into end of shift register
      //if N > II_CYCLE, add to shift_reg[0] to preserve values
      shift_reg[REG_DEPTH] = MAX(shift_reg[0], input[i]);
  
      //Shift every element of shift register
      #pragma unroll
      for(int j = 0; j < REG_DEPTH; ++j)
      {
        shift_reg[j] = shift_reg[j + 1];
      }
    }

    #pragma unroll 
    for(int i = 0; i < REG_DEPTH; ++i)
    {
      maxi = MAX(maxi, shift_reg[i]);
    }

  } // acc

  printf("    maximum: %lf\n", maxi);
}
#endif

#if ACC_DIR
void acc() {

  //Sum every element of shift register
  TYPE maxi = MIN_VALUE;

  //#pragma acc parallel present(input) 
  #pragma acc parallel num_gangs(1) num_workers(1) present(input) copy(maxi)
  {
    //#pragma acc loop reduction(max:maxi) 
    #pragma unroll 32
    for (int i = 0; i < N; i++) {
      maxi = MAX(maxi, input[i]);
    }
  }

  printf("    maximum: %lf\n", maxi);
}
#endif
#endif
