Example 1
----------
 #pragma acc loop reduction(+:sum)
 for(int i = 0; i < N; i++) {
   sum += input[i];
 }

Example 2
----------
 #pragma acc loop reduction(+:sum)
 for(int i = 0; i < N; i++) {
   sum += a[i] * b[i];
 }

Example 3
----------
 #pragma acc loop reduction(+:sum)
 for(int j= row_start; j < row_end; j++) {
   unsigned int Acol=cols[j];
   double Acoef=Acoefs[j];
   double xcoef=xcoefs[Acol];
   sum += Acoef*xcoef;
 }

Example 4
----------
  #pragma acc parallel loop reduction(max:error)
  for( int j = 1; j < n-1; j++)
  {
    #pragma acc loop reduction(max:error)
    for( int i = 1; i < m-1; i++ )
    {
      A[j][i] = 0.25 * ( Anew[j][i+1] + Anew[j][i-1]
                + Anew[j-1][i] + Anew[j+1][i]);
      error = fmax( error, fabs(A[j][i] - Anew[j][i]));
    }
  }

