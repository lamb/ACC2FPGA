#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <assert.h>

#define N 3     // input size
#define M 3     
#ifdef _OPENARC_
#pragma openarc #define N 3 
#pragma openarc #define M 3
#endif

// EX: Example number
//   1 - Left with an offset 
//   2 - Right with an offset
//   3 - Left inverse loop
//   4 - Collapse clause
#define EX 4

#define MIN(a,b) (((a) < (b)) ? (a) : (b))
void print_window(float * input_window, char const *s) {
  int ii;
  for (ii = 0; ii < 7 ; ii++) printf("%s[%d] = %f\n", s, ii, input_window[ii]);
}

float *input;
float *output;
float *verify;

void basic_1(); 
void basic_2(); 
void basic_3(); 
void basic_4(); 

void fpga_1();
void fpga_2();
void fpga_3();
void fpga_4();

int main(int argc, char **argv)
{
  
  int i, j;
  #if (EX == 4)
  int SZ = N*M;
  #else
  int SZ = N;
  #endif

  input  = (float *) malloc(sizeof(float)*SZ);
  output = (float *) malloc(sizeof(float)*SZ);
  verify = (float *) malloc(sizeof(float)*SZ);

  for (i = 0; i < SZ; i++) {
    //input[i] = (double)rand() / (double)RAND_MAX ; 
    input[i] = i;
    output[i] = 0;
    verify[i] = 0;
  }

  #pragma acc data copyout(output[0:SZ], verify[0:SZ])
  {
    if (EX == 1) {
      basic_1();
      fpga_1();
    }
    if (EX == 2) {
      basic_2();
      fpga_2();
    }
    if (EX == 3) {
      basic_3();
      fpga_3();
    }
    if (EX == 4) {
      basic_4();
      fpga_4();
    }
  }

  int flag = 1;
  
  for (i = 0; i < SZ; i++) {
    printf("output[%d] = %f, verify[%d] = %f\n", i, output[i], i, verify[i]);
    if (output[i] != verify[i]) {
      //printf("Verification FAIL\n");
      //exit(0);
      flag = 0;
    }
  }
  (flag) ? printf("Verification PASS\n") : printf("Verification FAIL\n");
}


void basic_1() {

  int i, j;

  // We need six extra boundary cells for a 7-point stencil
  float input_b[N + 6];

  // initalize input_b
  for (i = 0; i < N + 6; i++) {
    if (i < 3) input_b[i] = -1;
    else if (i >=  N + 3) input_b[i] = -1;
    else input_b[i] = input[i - 3];
  }

  #if 1 
  // Implementation  1
  // Solution:
  //   We can add an extra step between 1.5 and 2. After we shift up to the target
  //   element, we can shift X more times, where X is the initial value of the loop 
  //   (here X == 3). We could also just ammend rule 1.5 to shift an additional X times.
  #pragma openarc transform window(input_b[0:N+6], 7, 3, 0)
  #pragma acc parallel loop copyin(input_b) present(verify)
  for (i = 3; i < N + 3; i++) {

    float result = 0.0f;
    for (j = -3; j < 4; j++) {
      result += input_b[i + j];
    }
    verify[i - 3] = result; 
  }
  #endif

  #if 0
  // Implementation 2
  // This one is trickier. It violates the condition that the iteration index must 
  // directly correspond with the input array. More specifically, at each iteration,
  // the target_element must be in location input[i], where i is the value of the
  // loop iteration variable.
  //
  // Here the target_element is in input[i+3]. 

  for (i = 0; i < N; i++) {

    float result = 0.0f;
    for (j = -3; j < 4; j++) {
      result += input_b[(i + 3) + j];
    }
    verify[i] = result;
  }
  #endif
}

void fpga_1() {
  int i, j;

  // We need six extra boundary cells for a 7-point stencil
  float input_b[N + 6];

  // initalize input_b
  for (i = 0; i < N + 6; i++) {
    if (i < 3) input_b[i] = -1;
    else if (i >=  N + 3) input_b[i] = -1;
    else input_b[i] = input[i - 3];
  }

  #pragma acc parallel num_workers(1) num_gangs(1) copyin(input_b) present(output) private(i, j)
  {
    // Step 1
    float input_window[7];
    int ii;
    #pragma unroll
    for (ii = 0; ii < 7; ++ii) input_window[ii] = 0;
  
    // Step 1.5
    int jj;
    for (ii = 0; ii < (7 - 1) - 3 + 3; ++ii) {
      #pragma unroll
      for (jj = 0; jj < 7 - 1; ++jj) {
        input_window[jj] = input_window[jj + 1];
      }
      input_window[7 - 1] = input_b[ii];
    }
  
    // Main Loop
    for (i = 3; i < N + 3; i++) {
      // Step 2
      #pragma unroll
      for (ii = 0; ii < 7 - 1;  ++ii) {
        input_window[ii] = input_window[ii + 1];
      }
  
      // Step 3
      int index = i + (7 - 1) - 3;
      if (index < N + 6 && index >= 0)
        input_window[7 - 1] = input_b[index];
      else
        input_window[7 - 1] = 0;
      
      float result = 0.0f;
      for (j = -3; j < 4; j++) {
        result += input_window[3 + j];
      }
      output[i - 3] = result; 
    }
  }
}

void basic_2() {

  int i, j;

  // We need six extra boundary cells for a 7-point stencil
  float input_b[N + 6];

  // initalize input_b
  for (i = 0; i < N + 6; i++) {
    if (i < 3) input_b[i] = -1;
    else if (i >=  N + 3) input_b[i] = -1;
    else input_b[i] = input[i - 3];
  }

  // Main Loop
  #pragma acc parallel loop copyin(input_b) present(verify)
  for (i = (N + 6) - 3; i >= 3; i--) {

    float result = 0.0f;
    for (j = -3; j < 4; j++) {
      result += input_b[i + j];
    }
    verify[i - 3] = result; 
  }
}

void fpga_2() {

  int i, j;

  // We need six extra boundary cells for a 7-point stencil
  float input_b[N + 6];

  // initalize input_b
  for (i = 0; i < N + 6; i++) {
    if (i < 3) input_b[i] = -1;
    else if (i >=  N + 3) input_b[i] = -1;
    else input_b[i] = input[i - 3];
  }

  #pragma acc parallel num_workers(1) num_gangs(1) copyin(input_b) present(output) private(i, j)
  {
    //  Step 1
    float input_window[7];
    int ii;
    #pragma unroll
    for (ii = 0; ii < 7; ++ii) input_window[ii] = 0;
  
    //  Step 1.5
    int  jj;
    for (ii = 0; ii  < 3 + 1 - 1 + 3; ++ii) {
      #pragma unroll
      for (jj = (7 - 1); jj > 0; --jj) {
        input_window[jj] = input_window[jj - 1];
      }
      input_window[0] = input_b[(N + 6 - 1) - ii];
    }
  
    // #pragma openarc window(input_b[0:N+6], 7, 3, 0)
    for (i = ((N + 6) - 1) - 3; i >= 3; i--) {
      // Step 2
      #pragma unroll
      for (ii = 7 - 1; ii > 0; --ii) input_window[ii]  = input_window[ii - 1];
  
      // Step 3
      int index = i - 3;
      if (index < (N + 6) && index >= 0)
        input_window[0] = input_b[index];
      else
        input_window[0] = 0;
  
      float result = 0.0f;
      for (j = -3; j < 4; j++) {
        result += input_window[3 + j];
      }
      output[i - 3] = result; 
    }
  }
}

void basic_3() {

  int i, j;

  #pragma openarc transform window(input[0:N], 7, 3, 0)
  #pragma acc parallel loop copyin(input[0:N]) present(verify)
  for (i = 0; i < N; i++) {

    float result = 0.0f;
    for (j = -3; j < 4; j++) {
      if ( N - 1 - i  + j >= 0 && N - 1 - i + j < N) 
        result += input[((N - 1) - i) + j];
    }
    verify[i] = result; 
  }
}

void fpga_3() {

  int i, j;

  #pragma acc parallel num_workers(1) num_gangs(1) copyin(input[0:N]) present(output) private(i, j)
  {
    // Step 1
    float input_window[7];
    int ii;
    for (ii = 0; ii < 7; ++ii) {
      input_window[ii] = 0;
    }
    
    // Step 1.5
    int jj;
    for (ii = 0; ii < (7 - 1) - 3; ++ii)  {
      for (jj = 0; jj  < 7 - 1; ++jj) {
        input_window[jj] = input_window[jj + 1];
      }
      input_window[7 - 1] = input[ii];
    }
      
  
    // Main Loop
    for (i = 0; i < N; i++) {
      // Step 2
      for (ii = 0; ii < 7 - 1; ++ii) {
        input_window[ii] = input_window[ii + 1];
      }
  
      // Step 3
      int index = i + (7  - 1) - 3;
      if (index < N && index >= 0)
        input_window[6] = input[index];
      else
        input_window[6] = 0;
  
      float result = 0.0f;
      for (j = -3; j < 4; j++) {
        if ( N - 1 - i  + j >= 0 && N - 1 - i + j < N) 
          result += input_window[((N - 1) - 3) + j];
      }
      output[i] = result; 
    }
  }
}


// Basic stencil where we add N, S, E, W and Current cells
void basic_4() {

  int i, j;

  for (i = 0; i < N; i++) { // N rows
    for (j = 0; j < M; j++) { // M cols
      printf("%f ", input[i*M + j]);
    }
    printf("\n");
  }

  #pragma openarc transform window(input[0:N*M], 2*M + 1, M, 0)
  #pragma acc parallel loop collapse(2) copyin(input[0:N*M]) present(verify)
  for (i = 0; i < N; i++) { // N rows
    for (j = 0; j < M; j++) { // M cols
      
      float result = 0.0;

      result += input[i*M + j];
  
      // N
      if (i > 0) result += input[(i-1)*M + j]; 
      // S
      if (i < (N - 1)) result += input[(i+1)*M + j];
      // W
      if (j > 0) result += input[i*M + (j - 1)]; 
      // E
      if (j < (M - 1)) result += input[i*M + (j + 1)];

      verify[i*M + j] = result;
    }
  }

}

void fpga_4() {
  int i, j;

  #pragma acc parallel num_workers(1) num_gangs(1) copyin(input[0:N*M]) present(output) private(i, j)
  {
    // Step 1
    int ii;
    float input_window[2*M + 1]; 
    for (ii = 0; ii < 2*M + 1; ++ii) {
      input_window[ii] = 0;
    }

    // Step 1.5
    int jj;
    for (ii = 0; ii < 2*M - M; ++ii) {
      for (jj = 0; jj < 2*M; ++jj) {
        input_window[jj] = input_window[jj + 1];
      }
      input_window[2*M] = input[ii];
    }

    // Main Loop
    //#pragma openarc transform collapse(2) window(input[0:N*M, 2*M + 1, M, 0)
    for (i = 0; i < N; i++) { // N rows
      for (j = 0; j < M; j++) { // M cols
        int collapse_index = i*M + j;

        // Step 2
        for (ii = 0; ii < 2*M; ++ii) {
          input_window[ii] = input_window[ii + 1];
        }

        // Step 3
        int index = collapse_index + 2*M - M;
        input_window[2*M] = input[index];

        float result = 0.0;

        result += input_window[i*M + j - collapse_index + M];

        // N
        if (i > 0) result += input_window[(i-1)*M + j - collapse_index + M]; 
        // S
        if (i < (N - 1)) result += input_window[(i+1)*M + j - collapse_index + M];
        // W
        if (j > 0) result += input_window[i*M + (j - 1) - collapse_index + M]; 
        // E
        if (j < (M - 1)) result += input_window[i*M + (j + 1) - collapse_index + M];

        output[i*M + j] = result;
      }
    }  
  }

}
