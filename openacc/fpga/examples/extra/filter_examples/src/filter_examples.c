/* A minimal working example of the sliding window transformation
 The filter is a 1D filter of size K (ex. K == 3, and input size == N)

 -----
 FIRST (f)                MIDDLE (m)              LAST (l)
 in[n+0] + --> out[n+0]   in[n+0] +               in[n+0] + 
 in[n+1] +                in[n+1] + --> out[1]    in[n+1] +  
 in[n+2]                  in[n+2]                 in[n+2]   --> out[n+2]  
   
 -----
 STRIDE: If the stride is greater than one, computation is only performed on elements
 with indicies divisible by STRIDE

 -----
 LEFT/RIGHT: This determines whether the computation is performed starting at the 0th
 element (left) or Nth element (right)
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <assert.h>

#define FIRST   1
#define MIDDLE  0
#define LAST    0

#define LEFT    1
#define RIGHT   0

#define N 30     
#define K 7      
#define STRIDE 2

#ifdef _OPENARC_
#pragma openarc #define N 30  
#pragma openarc #define K 7
#pragma openarc #define STRIDE 10
#endif

#define WS ((K/2)*2 + 1)
#pragma openarc #define WS ((K/2)*2 + 1)

#define MIN(a,b) (((a) < (b)) ? (a) : (b))
void print_window(float * input_window, char const *s) {
  int ii;
  for (ii = 0; ii < 7 ; ii++) printf("%s[%d] = %f\n", s, ii, input_window[ii]);
}

float *input;
float *output;
float *verify;

void f_left_basic();
void f_left_fpga();

void l_left_basic();
void l_left_fpga();

void m_left_basic();
void m_left_fpga();

void f_right_basic();
void f_right_fpga();

void l_right_basic();
void l_right_fpga();

void m_right_basic();
void m_right_fpga();

int main(int argc, char **argv)
{
  
  int i, j;

  input  = (float *) malloc(sizeof(float)*N);
  output = (float *) malloc(sizeof(float)*N);
  verify = (float *) malloc(sizeof(float)*N);

  for (i = 0; i < N; i++) {
    //input[i] = (double)rand() / (double)RAND_MAX ; 
    input[i] = i;
    output[i] = 0;
    verify[i] = 0;
  }

  #pragma acc data copyin(input[0:N]) copyout(output[0:N], verify[0:N])
  {
    if (FIRST && LEFT ) { f_left_basic();  f_left_fpga(); } 
    if (LAST && LEFT ) { l_left_basic();  l_left_fpga(); }
    if (MIDDLE && LEFT ) { m_left_basic();  m_left_fpga(); }

    if (FIRST && RIGHT) { f_right_basic();  f_right_fpga() ; }
    if (LAST && RIGHT) { l_right_basic(); l_right_fpga(); } 
    if (MIDDLE && RIGHT) { m_right_basic(); m_right_fpga(); }
  }

  int flag = 1;
  for (i = 0; i < N; i++) {
    printf("man[%d] = %5.1f, auto[%d] = %5.1f\n", i, output[i], i, verify[i]);
    if (output[i] != verify[i]) {
      //printf("Verification FAIL\n");
      //exit(0);
      flag = 0;
    }
  }
  (flag) ? printf("Verification PASS\n") : printf("Verification FAIL\n");
}


void f_left_basic() {
  printf("FIRST LEFT BASIC\nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;

  #pragma openarc transform window(input[0:N], K, 0, 0)
  #pragma acc parallel loop present(input, verify)
  for (i = 0; i < N; i += 2) {

    float result = 0.0;
    for (j = 0; j < K; j++) {
      if (i + j < N) result += input[i + j];
    }
    verify[i] = result;
  }
}

void f_left_fpga() {
  printf("FIRST LEFT FPGA\nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;

  #pragma acc parallel num_gangs(1) num_workers(1) present(input, output)
  {
    // Step 1
    float input_window[K];
    int ii;
#pragma unroll
    for (ii = 0; ii < K; ++ii) input_window[ii] = 0;

    // Step 1.5
    int jj;
    for (ii = 0; ii < (K - STRIDE) - 0; ++ii) {
#pragma unroll
      for (jj = 0; jj < (K - 1); ++jj) {
        input_window[jj] = input_window[jj + 1];
      }
      input_window[K - 1] = input[ii];
    } 

    // Main Loop
    for (i = 0; i < N; i += STRIDE) {
      // Step 2
#pragma unroll
      for (ii = 0; ii < K - STRIDE; ++ii) {
        input_window[ii] = input_window[ii + STRIDE];
      }

      // Step 3
      for (ii = 0; ii < STRIDE; ++ii) {
        int index = i + (K - 1) - 0 - ii;
        if (index < N && index >= 0) {
          input_window[(K - 1) - ii] = input[index];
        } 
        else {
          input_window[(K - 1) - ii] = 0;
        }
      }

      float result = 0.0;
      for (j = 0; j < K; j++) {
        if (i + j < N) {
          result += input_window[0 + j];
        }
      }
      output[i] = result;
    }
  }
}


void l_left_basic() {
  printf("LAST LEFT BASIC\nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;

  #pragma openarc transform window(input[0:N], K, K-1, 0)
  #pragma acc parallel loop present(input, verify)
  for (i = 0; i < N; i += STRIDE) {

    float result = 0.0;
    for (j = 0; j < K; j++) {
      if (i - j > 0) result += input[i - j];
    }
    verify[i] = result;
  }
}

void l_left_fpga() {
  printf("LAST LEFT FGPA\nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;

  #pragma acc parallel num_gangs(1) num_workers(1) present(input, output) private(i, j)
  {
    // Step 1
    float input_window[K];
    int ii;
    #pragma unroll
    for (ii = 0; ii < K; ++ii) input_window[ii] = 0;
  
    // Step 1.5
    int jj;
    for (ii = 0; ii < (K - STRIDE) - (K - 1); ++ii) {
      #pragma unroll
      for (jj = 0; jj < K; ++jj) { 
        input_window[jj] = input_window[jj + 1];
      }
      input_window[K - 1] = input[ii];
    }
  
    // Main Loop
    for (i = 0; i < N; i += STRIDE) {
      // Step 2
      #pragma unroll
      for (ii = 0; ii < K - STRIDE; ++ii) {
        input_window[ii] = input_window[ii + STRIDE];
      }
  
      // Step 3
      int bound = MIN(STRIDE, K);
      for (ii = 0; ii < bound; ++ii) {
        int index = i + (K - 1) - (K - 1) - ii;
        if (index < N && index  >= 0) {
          input_window[(K - 1) - ii] = input[index];
        }
        else {
          input_window[(K - 1) - ii] = 0;
        }
      }
  
      float result = 0.0;
      for (j = 0; j < K; j++) {
        if (i - j > 0) { 
          result += input_window[(K - 1) - j];
        }
      }
      output[i] = result;
    }
  }
}


#define WS ((K/2)*2 + 1)
#pragma openarc #define WS ((K/2)*2 + 1)
void m_left_basic() {
  printf("MIDDLE LEFT BASIC \nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;
  
  #pragma openarc transform window(input[0:N], WS, K/2, 0)
  #pragma acc parallel loop present(input, verify)
  for (i = 0; i < N; i += STRIDE) {

    float result = 0.0;
    for (j = -(K/2); j < K/2 + 1; j++) {
      if (i + j < N && i + j >= 0) 
        result += input[i + j];
    }
    verify[i] = result;
  }
}

void m_left_fpga() {
  printf("MIDDLE LEFT FGPA\nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;
  
  #pragma acc parallel num_gangs(1) num_workers(1) present(input, output) private(i,j)
  {
    // Step 1
    float input_window[WS];
    int ii;
    #pragma unroll
    for (ii = 0; ii < WS; ++ii) input_window[ii] = 0;
  
    // Step 1.5
    int jj;
    for (ii = 0; ii < (WS - STRIDE) - (K/2); ++ii) {
      #pragma unroll
      for (jj = 0; jj < WS; ++jj)  {
        input_window[jj] = input_window[jj + 1];
      }
      input_window[WS - 1] = input[ii];
    }
  
    // Main Loop
    for (i = 0; i < N; i += STRIDE) {
      // Step 2
      #pragma unroll
      for (ii = 0; ii < ((K/2)*2 + 1) - STRIDE; ++ii) {
        input_window[ii] = input_window[ii + STRIDE];
      }
  
      // Step 3
      for (ii = 0; ii < STRIDE; ++ii) {
        int index = i + (WS - 1) - (K/2) - ii;
        if (index < N &&  index >= 0) {
          input_window[(WS - 1) - ii] = input[index];
        }
        else {
          input_window[(WS - 1) - ii] = 0; 
        }
      }
      
      float result = 0.0;
      for (j = -(K/2); j < K/2 + 1; j++) {
        if (i + j < N && i + j >= 0) 
          result += input_window[(K/2) + j];
      }
      output[i] = result;
    }
  }

}

void f_right_basic() {
  printf("FIRST RIGHT BASIC\nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;

  #pragma openarc transform window(input[0:N], K, 0, 0)
  #pragma acc parallel loop present(input, verify)
  for (i = N - 1; i >=0; i -= STRIDE) {

    float result = 0.0;
    for (j = 0; j < K; j++) {
      if (i + j < N) result += input[i + j];
    }
    verify[i] = result;
  }

}

void f_right_fpga() {
  printf("FIRST RIGHT FGPA\nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;

  #pragma acc parallel num_gangs(1) num_workers(1) present(input[0:N], output[0:N])
  {
    // Step 1
    float input_window[K];
    int ii;
    #pragma unroll
    for (ii = 0; ii < K; ++ii) input_window[ii] = 0;

    // Step 1.5
    int jj;
    for (ii = 0; ii < 0 + 1 - STRIDE; ++ii){
      #pragma unroll
      for (jj = (K - 1); jj > 0; --jj) {
        input_window[jj] = input_window[jj - 1];
      }
      input_window[0] = input[(N - 1) - ii];
    }

    // Main Loop
    for (i = N - 1; i >=0; i -= STRIDE) {
      // Step 2
      #pragma unroll
      for (ii = K - 1; ii > 0; --ii) {
        input_window[ii] = input_window[ii - STRIDE];
      }

      // Step 3
      int bound = MIN(STRIDE, K);
      for (ii = 0; ii < bound; ++ii) {  
        int index = i - 0 + ii;
        if (index < N &&  index >= 0)
          input_window[ii] = input[index];
        else
          input_window[ii] = 0;
      }

      float result = 0.0;
      for (j = 0; j < K; j++) {
        if (i + j < N) {
          result += input_window[0 + j];
        }
      }
      output[i] = result;
    }
  }
}

void l_right_basic() {
  printf("LAST RIGHT BASIC\nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;
  
  #pragma openarc transform window(input[0:N], K, K-1, 0)
  #pragma acc parallel loop present(input, verify)
  for (i = N - 1; i >= 0; i -= STRIDE) {
    float result = 0.0;
    for (j = 0; j < K; j++) {
      if (i - j > 0) result += input[i - j];
    }
    verify[i] = result;
  }
}

void l_right_fpga() {
  printf("LAST RIGHT FPGA\nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;
  
  #pragma acc parallel num_gangs(1) num_workers(1) present(input, output) private(i, j)
  {
    // Step 1
    float input_window[K];
    int ii;
    #pragma unroll
    for (ii = 0; ii < K; ++ii) input_window[ii] = 0;
    
    // Step 1.5
    int jj;
    for (ii = 0; ii < K - 1 + 1 - STRIDE; ++ii) {
      #pragma unroll
      for (jj = (K - 1); jj > 0; --jj) {
        input_window[jj] = input_window[jj - 1];
      }
      input_window[0] = input[(N - 1) - ii];
    }  
   
    // Main Loop
    for (i = N - 1; i >= 0; i -= STRIDE) {
      // Step 2
      #pragma unroll
      for (ii = K - 1; ii > 0; --ii)
        input_window[ii] = input_window[ii - STRIDE];
      
      // Step 3
      int bound = MIN(STRIDE, K);
      for (ii = 0; ii < bound; ++ii) {
        int index = i - (K - 1) + ii;
        if (index < N && index >= 0) 
          input_window[ii] = input[index];
        else
          input_window[ii] = 0;
      }
  
      float result = 0.0;
      for (j = 0; j < K; j++) {
        if (i - j > 0) {
          result += input_window[(K - 1) - j];
        }
      }
      output[i] = result;
    }
  }

}

void m_right_basic() {
  printf("MIDDLE RIGHT BASIC\nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;
  
  #pragma openarc transform window(input[0:N], (K/2)*2 + 1, K/2, 0)
  #pragma acc parallel loop present(input, verify)
  for (i = N - 1; i >= 0; i -= STRIDE) {

    float result = 0.0;
    for (j = -(K/2); j < K/2 + 1; j++) {
      if (i + j < N && i + j >= 0) 
        result += input[i + j];
    }
    verify[i] = result;
  }
}

void m_right_fpga() { 
  printf("MIDDLE RIGHT FGPA\nN: %d, K: %d, STRIDE:%d\n\n", N, K, STRIDE);

  int i, j;

  #pragma acc parallel num_gangs(1) num_workers(1) present(input, output) private(i, j)
  {
    // Step 1
    float input_window[WS];
    int ii;
    #pragma unroll
    for (ii = 0; ii < WS; ++ii) input_window[ii] = 0;
    
    // Step 1.5
    int jj;
    for (ii = 0; ii < (K/2) + 1 - STRIDE; ++ii) {
      #pragma unroll
      for (jj = (WS - 1); jj > 0; --jj)  {
        input_window[jj] = input_window[jj  - 1];
      }
      input_window[0] = input[(N - 1) - ii];
    }
   
    // Middle Loop
    for (i = N - 1; i >= 0; i -= STRIDE) {
      // Step 2
      #pragma unroll
      for (ii = WS - 1; ii > 0; --ii)
        input_window[ii] = input_window[ii - STRIDE];
  
      // Step 3
      int bound = MIN(STRIDE, WS);
      for (ii = 0; ii < bound; ++ii) {
        int index = i - (K/2) + ii;
        if (index < N && index >= 0) 
          input_window[ii] = input[index];
        else
          input_window[ii]  = 0;
      }
    
      float result = 0.0;
      for (j = -(K/2); j < K/2 + 1; j++) {
        if (i + j < N && i + j >= 0) 
          result += input_window[(K/2) + j];
      }
      output[i] = result;
    }
  }
}
