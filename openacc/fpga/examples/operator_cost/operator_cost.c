// To calculate operator costs, compile with the
//   desired TYPE and OPER/FUNC, using the 
//   -c flag.
//
// The operator costs can then be viewied in 
//  ../cetus_output/openarc_kernel/openarc_kernel.log
//
//  Increasing the unroll factor can help if the 
//    compiler is launching successive iterations every cycle by
//    decreasing the circuit frequency

#include <stdio.h>
#include <stdlib.h>
#define N 1000
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))

// int, float, double
#define TYPE double 

// +=, *= 
#define OPER *=
// MIN, MAX
#define FUNC MAX 

#define MIN_MAX 0

#pragma openarc #define N 100

// Computation
int main(int argc, char** argv) {

  TYPE *input = (TYPE *) malloc(N * sizeof(TYPE));

  TYPE red = 0;

  for (int i = 0; i < N; ++i) input[i] = i;

  #pragma acc parallel num_gangs(1) num_workers(1)  copyin(input[0:N])
  {
    #pragma unroll 4
    #pragma acc loop reduction(*: red)
    for (int i = 0; i < N; ++i) {
      #if MIN_MAX
      red = FUNC(red, input[i]);
      #else
      red OPER input[i];
      #endif
    }
  }

  TYPE red_ver = 0;
  for (int i = 0; i < N; ++i) {
    #if MIN_MAX
    red_ver = FUNC(red_ver, input[i]);
    #else
    red_ver OPER input[i];
    #endif
  }

  if (red == red_ver)
    printf("Verification Successful!\n");
  else
    printf("Verfication Failed:\n  red = %d\n  red_ver = %d\n", red, red_ver);

  return 0;
}
