#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#define MIN(a,b) (((a) < (b)) ? (a) : (b))

//#define ITER 66

#define NSEW 1
// Used in Hotspot and SRAD
//    N
// W  T  E
//    S

#define SOBEL 0
// P P P
// P P P
// P P T

#define CANNY 0
// NW NN NE
// WW TT EE
// SW SS SE

#define FD3D 0
// 3k+1 input points (k adjacent points in each direction
//-3xy -2xy -xy -3x -2x -x -3 -2 -1 0 1 2 3 x 2x 3x xy 2xy 3xy

#define R_SIZE (32) 
#define C_SIZE (32) 
#define Z_SIZE (32) // number of planes (FD3D only)

#pragma openarc #define R_SIZE (32)
#pragma openarc #define C_SIZE (32)
#pragma openarc #define Z_SIZE (32)

#if FD3D
  #define DATA_SIZE C_SIZE*R_SIZE*Z_SIZE
  #pragma openarc #define DATA_SIZE (C_SIZE*R_SIZE*Z_SIZE)
#else
  #define DATA_SIZE C_SIZE*R_SIZE
  #pragma openarc #define DATA_SIZE (C_SIZE*R_SIZE)
#endif

void print_window(float * sw, char const *s, int sw_size) {
  int ii;
  for (ii = 0; ii < sw_size ; ii++) printf("%s[%d] = %f\n", s, ii, sw[ii]);
}

float *input;
float *output;
float *verify;

void basic();
void fpga();

int main(int argc, char **argv)
{

  int i, j;

  #if NSEW
  printf("NSEW Stencil\n");
  #elif SOBEL
  printf("SOBEL Stencil\n");
  #elif CANNY
  printf("CANNY Stencil\n");
  #elif FD3D 
  printf("FD3D Stencil\n");
  #endif

  input  = (float *) malloc(sizeof(float)*DATA_SIZE);
  output = (float *) malloc(sizeof(float)*DATA_SIZE);
  verify = (float *) malloc(sizeof(float)*DATA_SIZE);

  for (i = 0; i < DATA_SIZE; i++) {
    //input[i] = (double)rand() / (double)RAND_MAX ; 
    input[i] = i;
    output[i] = 0;
    verify[i] = 0;
  }
  
  input[0] = 0.1;

#pragma acc data copyin(input[0:DATA_SIZE]) \
  copyout(output[0:DATA_SIZE])
  {
    printf("\n  Running Accelerator version\n");
    fpga();
  }

  printf("\n  Running CPU version\n");
  basic();

  int flag = 1;
  for (i = 0; i < DATA_SIZE; i++) {
    if (output[i] != verify[i]) {
      #if SOBEL
      if (i % R_SIZE > 1) {
        printf("cpu[%d] = %5.1f, acc[%d] = %5.1f\n", i, verify[i], i, output[i]);
        flag = 0;
        exit(0);
      }
      #else
      printf("cpu[%d] = %5.1f, acc[%d] = %5.1f\n", i, verify[i], i, output[i]);
      flag = 0;
      exit(0);
      #endif 

    }
  }
  (flag) ? printf("Verification PASS\n") : printf("Verification FAIL\n");
}


#if NSEW
void basic() {

  for (int i = 0; i < R_SIZE * C_SIZE ; i++) {

    int r = i / R_SIZE;
    int c = i % R_SIZE;

    float result = 0.0;
    float N=0.0, S=0.0, E=0.0, W=0.0;

    // Corner 1
    if (r == 0 && c == 0) {
      S = input[i + C_SIZE];
      E = input[i + 1];
    }
    // Corner 2
    else if (r == 0 && c == C_SIZE - 1) {
      S = input[i + C_SIZE];
      W = input[i - 1]; 
    }
    // Corner 3
    else if (r == R_SIZE - 1 && c == C_SIZE - 1) {
      N = input[i - C_SIZE];
      W = input[i - 1]; 
    }
    // Corner 4
    else if (r == R_SIZE - 1 && c == 0) {
      N = input[i - C_SIZE];
      E = input[i + 1];
    }
    // Edge 1
    else if (r == 0) {
      S = input[i + C_SIZE];
      E = input[i + 1];
      W = input[i - 1]; 
    } 
    // Edge 2
    else if (c == C_SIZE - 1) {
      N = input[i - C_SIZE];
      S = input[i + C_SIZE];
      W = input[i - 1]; 
    } 
    // Edge 3
    else if (r == R_SIZE - 1) {
      N = input[i - C_SIZE];
      E = input[i + 1];
      W = input[i - 1]; 
    } 
    // Edge 4
    else if (c == 0) {
      N = input[i - C_SIZE];
      S = input[i + C_SIZE];
      E = input[i + 1];
    } 
    // Inside 
    else {
      N = input[i - C_SIZE];
      S = input[i + C_SIZE];
      E = input[i + 1];
      W = input[i - 1]; 
    } 

    verify[i] = input[i] + N + E + S + W;
  }
}

void fpga() {

  #pragma acc parallel num_gangs(1) num_workers(1)
  {
    #define TARGET (C_SIZE)
    #define SW_BASE_SIZE (2*C_SIZE)
    #define SW_SIZE (SW_BASE_SIZE + 1)

    float sw[SW_SIZE];

    #pragma unroll
    for (int ii = 0; ii < SW_SIZE; ++ii) sw[ii] = 0; 

    //int i = TARGET - SW_BASE_SIZE;
    //do {
    for (int i = TARGET - SW_BASE_SIZE; i < R_SIZE * C_SIZE ; i++) {

      #pragma unroll
      for (int ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + 1];
      }

      int read_offset = i + SW_BASE_SIZE - TARGET;
      sw[SW_BASE_SIZE] = input[read_offset];

      int sw_offset = TARGET - i;

      float result = 0.0;
      float N=0.0, S=0.0, E=0.0, W=0.0;

      int r = i / R_SIZE;
      int c = i % R_SIZE;

      // Corner 1
      if (r == 0 && c == 0) {
        S = sw[TARGET + C_SIZE];
        E = sw[TARGET + 1];
      }
      // Corner 2
      else if (r == 0 && c == C_SIZE - 1) {
        S = sw[TARGET + C_SIZE];
        W = sw[TARGET - 1]; 
      }
      // Corner 3
      else if (r == R_SIZE - 1 && c == C_SIZE - 1) {
        N = sw[TARGET - C_SIZE];
        W = sw[TARGET - 1]; 
      }
      // Corner 4
      else if (r == R_SIZE - 1 && c == 0) {
        N = sw[TARGET - C_SIZE];
        E = sw[TARGET + 1];
      }
      // Edge 1
      else if (r == 0) {
        S = sw[TARGET + C_SIZE];
        E = sw[TARGET + 1];
        W = sw[TARGET - 1]; 
      } 
      // Edge 2
      else if (c == C_SIZE - 1) {
        N = sw[TARGET - C_SIZE];
        S = sw[TARGET + C_SIZE];
        W = sw[TARGET - 1]; 
      } 
      // Edge 3
      else if (r == R_SIZE - 1) {
        N = sw[TARGET - C_SIZE];
        E = sw[TARGET + 1];
        W = sw[TARGET - 1]; 
      } 
      // Edge 4
      else if (c == 0) {
        N = sw[TARGET - C_SIZE];
        S = sw[TARGET + C_SIZE];
        E = sw[TARGET + 1];
      } 
      // Inside 
      else {
        N = sw[TARGET - C_SIZE];
        S = sw[TARGET + C_SIZE];
        E = sw[TARGET + 1];
        W = sw[TARGET - 1]; 
      } 

      if (i >= 0)
        output[i] = sw[TARGET] + N + S + E + W;


      i += 1;
    //} while (i < R_SIZE*C_SIZE);
    }

  } // exit acc parallel

}
#endif

#if SOBEL
void basic() {
  int i, j, k;

  for (i = 0; i < R_SIZE * C_SIZE ; i++) {

    float result = 0;

    for (j = 0; j < 3; j++) {
      for (k = 0; k < 3; k++) {

        if (i - (j*C_SIZE) - k >= 0) {
          result += input[i - (j*C_SIZE) - k];
        }
      }
    }

    verify[i] = result;
  }
}

#define TARGET (2*C_SIZE + 2)
#define SW_BASE_SIZE (2*C_SIZE + 2)
#define SW_SIZE (SW_BASE_SIZE + 1)

void fpga() {

  #pragma acc parallel num_gangs(1) num_workers(1)
  {
    float sw[SW_SIZE];

    #pragma unroll
    for (int ii = 0; ii < SW_SIZE; ++ii) sw[ii] = 0;

    for (int i = 0; i < R_SIZE * C_SIZE ; i++) {

      #pragma unroll
      for (int ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + 1];
      }

      int read_offset = i + SW_BASE_SIZE - TARGET;
      sw[SW_BASE_SIZE] = input[read_offset];

      int sw_offset = TARGET - i;

      // Main computation
      float result = 0;
            
      //#pragma unroll
      //for (int j = 0; j < 3; j++) {
      //  #pragma unroll
      //  for (int k = 0; k < 3; k++) {
      //    if (i - (j*C_SIZE) - k >= 0) {
      //      //result += sw[i - (j*C_SIZE) - k + sw_offset];
      //      result += sw[TARGET - j*C_SIZE - k];
      //    }
      //  }
      //}

      //result += sw[TARGET];
      //result += sw[TARGET - 1];
      //result += sw[TARGET - 2];
      //result += sw[TARGET - 1*C_SIZE];
      //result += sw[TARGET - 1*C_SIZE - 1];
      //result += sw[TARGET - 1*C_SIZE - 2];
      //result += sw[TARGET - 2*C_SIZE];
      //result += sw[TARGET - 2*C_SIZE - 1];
      //result += sw[TARGET - 2*C_SIZE - 2];

      result += sw[TARGET - 3];
      result += sw[TARGET - 4];
      result += sw[TARGET - 5];


      output[i] = result;
    }
  }
}
#endif
