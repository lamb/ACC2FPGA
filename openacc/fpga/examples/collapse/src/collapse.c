#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#define MIN(a,b) (((a) < (b)) ? (a) : (b))

// 3k+1 input points (k adjacent points in each direction
//-3xy -2xy -xy -3x -2x -x -3 -2 -1 0 1 2 3 x 2x 3x xy 2xy 3xy
#define coord(x, y, z) ((z) * C_SIZE * R_SIZE + (y) * Z_SIZE + (x))
#define RADIUS 3

#define ACC_COLLAPSE  1
#define MAN_COLLAPSE  0

#define R_SIZE (32) // number of rows
#define C_SIZE (32) // number of cols
#define Z_SIZE (32) // number of planes

#pragma openarc #define R_SIZE (32)
#pragma openarc #define C_SIZE (32)
#pragma openarc #define Z_SIZE (32)

#define DATA_SIZE C_SIZE*R_SIZE*Z_SIZE
#pragma openarc #define DATA_SIZE (C_SIZE*R_SIZE*Z_SIZE)

void print_window(float * sw, char const *s, int sw_size) {
  int ii;
  for (ii = 0; ii < sw_size ; ii++) printf("%s[%d] = %f\n", s, ii, sw[ii]);
}

double getCurrentTimestamp() {
  struct timespec a;
  clock_gettime(CLOCK_MONOTONIC, &a);
  return ( (double) a.tv_nsec * 1.0e-9) + (double) a.tv_sec;
}
void basic(float *input, float *output, int sizex, int sizey, int sizez);
void collapse(float *input, float *output, int sizex, int sizey, int sizez);

int main(int argc, char **argv)
{

  int i, j;

  #if ACC_COLLAPSE
  printf("ACC Collapse\n");
  #else
  printf("Man Collapse\n");
  #endif
  
  int sizex = C_SIZE;
  int sizey = R_SIZE;
  int sizez = Z_SIZE;

  int data_size = DATA_SIZE;

  float *input;
  float *output;
  float *verify;

  input  = (float *) malloc(sizeof(float)*data_size);
  output = (float *) malloc(sizeof(float)*data_size);
  verify = (float *) malloc(sizeof(float)*data_size);

  for (i = 0; i < data_size; i++) {
    //input[i] = (double)rand() / (double)RAND_MAX ; 
    input[i] = i;
    output[i] = 0;
    verify[i] = 0;
  }
  
  input[0] = 0.1;

  double acc_start, acc_end;

  #pragma acc data copyin(input[0:DATA_SIZE]) \
    copyout(output[0:DATA_SIZE])
  {
    printf("\n  Running Accelerator version\n");
    acc_start = getCurrentTimestamp();
    collapse(input, output, sizex, sizey, sizez);
    acc_end = getCurrentTimestamp();
  }

  double cpu_start, cpu_end;
  printf("\n  Running CPU version\n");

  cpu_start = getCurrentTimestamp();
  basic(input, verify, sizex, sizey, sizez);
  cpu_end = getCurrentTimestamp();

  printf("\n  Accelerator Elapsed Time = \t%lf sec\n", acc_end - acc_start);
  printf("  CPU Elapsed Time = \t\t\t%lf sec\n", cpu_end - cpu_start);

  int flag = 1;
  for (i = 0; i < data_size; i++) {
    if (output[i] != verify[i]) {
      printf("basic[%d] = %5.1f, collapse[%d] = %5.1f\n", i, verify[i], i, output[i]);
      flag = 0;
      exit(0);
    }
  }
  (flag) ? printf("Verification PASS\n") : printf("Verification FAIL\n");
}


void basic(float *input, float *output, int sizex, int sizey, int sizez) {

  for (int z = 0; z < sizez; z++) {
    for (int y = 0; y < sizey; y++) {
      for (int x = 0; x < sizex; ++x) {

    if (x >= RADIUS && x < sizex - RADIUS &&
        y >= RADIUS && y < sizey - RADIUS &&
        z >= RADIUS && z < sizez - RADIUS) {

      float result = input[coord(x,y,z)];   // T
      #pragma unroll
      for (int r = 1; r <= RADIUS; r++) {
        result += input[coord(x - r, y, z)];  // -X
        result += input[coord(x + r, y, z)];  // +X
        result += input[coord(x, y - r, z)];  // -Y 
        result += input[coord(x, y + r, z)];  // +Y
        result += input[coord(x, y, z - r)];  // -Z
        result += input[coord(x, y, z + r)];  // +Z
      }
      output[coord(x,y,z)] = result;
    }
    else {
      output[coord(x,y,z)] = input[coord(x,y,z)];
    }

      } // x
    } // y
  } // z
}

#if ACC_COLLAPSE
void collapse(float *input, float *output, int sizex, int sizey, int sizez) {

  //#pragma acc parallel loop collapse(3) num_gangs(1) num_workers(1) \
    present(input[0:DATA_SIZE], output[0:DATA_SIZE])
  #pragma acc parallel loop collapse(3) num_gangs(1) num_workers(1) \
    present(input[0:DATA_SIZE], output[0:DATA_SIZE])
  for (int z = 0; z < sizez; z++) {
    for (int y = 0; y < sizey; y++) {
      for (int x = 0; x < sizex; ++x) {

    if (x >= RADIUS && x < sizex - RADIUS &&
        y >= RADIUS && y < sizey - RADIUS &&
        z >= RADIUS && z < sizez - RADIUS) {

      float result = input[coord(x,y,z)];   // T
      #pragma unroll
      for (int r = 1; r <= RADIUS; r++) {
        result += input[coord(x - r, y, z)];  // -X
        result += input[coord(x + r, y, z)];  // +X
        result += input[coord(x, y - r, z)];  // -Y 
        result += input[coord(x, y + r, z)];  // +Y
        result += input[coord(x, y, z - r)];  // -Z
        result += input[coord(x, y, z + r)];  // +Z
      }
      output[coord(x,y,z)] = result;
    }
    else {
      output[coord(x,y,z)] = input[coord(x,y,z)];
    }

      } // x
    } // y
  } // z
}
#endif

#if MAN_COLLAPSE
void collapse(float *input, float *output, int sizex, int sizey, int sizez) {

  #pragma acc parallel num_gangs(1) num_workers(1) \
    present(input[0:DATA_SIZE], output[0:DATA_SIZE])
  {
  int x = 0, y = 0, z = 0;
  for (int i = 0; i < sizex*sizey*sizez; i++) {

    if (x >= RADIUS && x < sizex - RADIUS &&
        y >= RADIUS && y < sizey - RADIUS &&
        z >= RADIUS && z < sizez - RADIUS) {

      float result = input[coord(x,y,z)];   // T
      #pragma unroll
      for (int r = 1; r <= RADIUS; r++) {
        result += input[coord(x - r, y, z)];  // -X
        result += input[coord(x + r, y, z)];  // +X
        result += input[coord(x, y - r, z)];  // -Y 
        result += input[coord(x, y + r, z)];  // +Y
        result += input[coord(x, y, z - r)];  // -Z
        result += input[coord(x, y, z + r)];  // +Z
      }
      output[coord(x,y,z)] = result;
    }
    else {
      output[coord(x,y,z)] = input[coord(x,y,z)];
    }

    z++;
    if (z == sizez) { y++; z = 0; }
    if (y == sizey) { x++; y = 0; }
  } // i
  } // acc
}
#endif
