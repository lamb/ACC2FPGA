#include <stdio.h>
#include <stdlib.h>
#define R 12
#define C 16
#define N (R*C)

#pragma openarc #define R 12
#pragma openarc #define C 16
#pragma openarc #define N (R*C)

#define TARGET C
#define NBD_SIZE 2*C + 1
#pragma openarc #define TARGET C
#pragma openarc #define NBD_SIZE 2*C + 1

// Computation
int main(int argc, char** argv) {

    int *input, *output;

    input = (int *) malloc(N * sizeof(int));
    output = (int *) malloc(N * sizeof(int));
    int value;

    for (int i = 0; i < N; ++i) input[i] = i;

    /* Parallel Base Translations */
    /* 1. Normal          - Working 
     * 2. Nested          - Working
     * 3. Normal Collapse - Working
     * 4. Nested Collapse - Working */

    /* Parallel Multi Translations */
    /* 5. Normal          - Working 
     * 6. Nested          - Working
     * 7. Normal Collapse - Working
     * 8. Nested Collapse - Working */

    /* Kernels Base Translations */
    /* 9.  Normal          - Working  
     * 10. Nested          - Working
     * 11. Normal Collapse - Working
     * 12. Nested Collapse - Working */

    /* Kernels Multi Translations */
    /* 13  Normal          - Working 
     * 14. Nested          - Working
     * 15. Normal Collapse - Working 
     * 16. Nested Collapse - Working */

    #if 1
    // 1. Working
    #pragma openarc transform window(input[0:N], output[0:N])
    #pragma acc parallel loop num_gangs(1) num_workers(1)  \
      copyin(input[0:N]) copyout(output[0:N])
    #pragma unroll 5
    for (int i = 0; i < N; ++i) {

      int value = 0;

      if ( i >= C && i <= N - C) {
        value += input[i - C]; // N
        value += input[i + C]; // S
        value += input[i]; // C
      }
      else {
        value = input[i];
      }

      output[i] = value;
    } 
    #endif

    #if 0
    // 2. Working
    #pragma acc parallel num_gangs(1) num_workers(1) \
        copyin(input[0:N]) copyout(output[0:N])
    {
      #pragma openarc transform window(input[0:N], output[0:N])
      #pragma acc loop
      for (int i = 0; i < N; ++i) {
        
        int value = 0;

        if ( i >= C && i <= N - C) {
          value += input[i - C]; // N
          value += input[i + C]; // S
          value += input[i]; // C
        }
        else {
          value = input[i];
        }

        output[i] = value;
      } 

    }
    #endif

    #if 0
    // 3. Working
    #pragma openarc transform window(input[0:N], output[0:N])
    #pragma acc parallel loop num_gangs(1) num_workers(1) \
      copyin(input[0:N]) copyout(output[0:N]) collapse(2)
    for (int r = 0; r < R; ++r) {
      for (int c = 0; c < C; ++c) {

        int value = 0;

        if ( r >= 1 && r <= R - 1) {
          value += input[(r*C + c) - C]; // N
          value += input[(r*C + c) + C]; // S
          value += input[r*C + c]; // C
        }
        else {
          value = input[r*C + c];
        }

        output[r*C + c] = value;
      } // c
    } // r
    #endif

    #if 0
    // 4. Working
    #pragma acc parallel num_gangs(1) num_workers(1) \
      copyin(input[0:N]) copyout(output[0:N]) 
    {
      #pragma openarc transform window(input[0:N], output[0:N])
      #pragma acc loop gang worker collapse(2)
      for (int r = 0; r < R; ++r) {
        for (int c = 0; c < C; ++c) {

          int value = 0;

          if ( r >= 1 && r <= R - 1) {
            value += input[(r*C + c) - C]; // N
            value += input[(r*C + c) + C]; // S
            value += input[r*C + c]; // C
          }
          else {
            value = input[r*C + c];
          }

          output[r*C + c] = value;
        } // c
      } // r
    } // acc
    #endif

    #if 0
    // 5. Working
    #pragma openarc transform window(input[0:N], output[0:N])
    #pragma acc parallel num_gangs(1) num_workers(1)  \
      copyin(input[0:N]) copyout(output[0:N])
    #pragma unroll 4
    for (int i = 0; i < N; ++i) {

      int value = 0;

      if ( i >= C && i <= N - C) {
        value += input[i - C]; // N
        value += input[i + C]; // S
        value += input[i]; // C
      }
      else {
        value = input[i];
      }

      output[i] = value;
    } 
    #endif

    #if 0
    // 6. Working
    #pragma acc parallel num_gangs(1) num_workers (1) \
        copyin(input[0:N]) copyout(output[0:N])
    {
      #pragma openarc transform window(input[0:N], output[0:N])
      #pragma acc loop
      #pragma unroll 4
      for (int i = 0; i < N; ++i) {
        
        int value = 0;

        if ( i >= C && i <= N - C) {
          value += input[i - C]; // N
          value += input[i + C]; // S
          value += input[i]; // C
        }
        else {
          value = input[i];
        }

        output[i] = value;
      } 

    }
    #endif

    #if 0
    // 7. Working
    #pragma openarc transform window(input[0:N], output[0:N])
    #pragma acc parallel loop num_gangs(1) num_workers(1) \
      copyin(input[0:N]) copyout(output[0:N]) collapse(2)
    #pragma unroll 4
    for (int r = 0; r < R; ++r) {
      for (int c = 0; c < C; ++c) {

        int value = 0;

        if ( r >= 1 && r <= R - 1) {
          value += input[(r*C + c) - C]; // N
          value += input[(r*C + c) + C]; // S
          value += input[r*C + c]; // C
        }
        else {
          value = input[r*C + c];
        }

        output[r*C + c] = value;
      } // c
    } // r
    #endif

    #if 0
    // 8. Working
    #pragma acc parallel num_gangs(1) num_workers(1) \
      copyin(input[0:N]) copyout(output[0:N]) 
    {
      #pragma openarc transform window(input[0:N], output[0:N])
      #pragma acc loop collapse(2)
      #pragma unroll 4
      for (int r = 0; r < R; ++r) {
        for (int c = 0; c < C; ++c) {

          int value = 0;

          if ( r >= 1 && r <= R - 1) {
            value += input[(r*C + c) - C]; // N
            value += input[(r*C + c) + C]; // S
            value += input[r*C + c]; // C
          }
          else {
            value = input[r*C + c];
          }

          output[r*C + c] = value;
        } // c
      } // r
    } // acc
    #endif

    #if 0
    // 9. Working
    #pragma openarc transform window(input[0:N], output[0:N])
    #pragma acc kernels loop gang(1) worker(1)  \
      copyin(input[0:N]) copyout(output[0:N])
    for (int i = 0; i < N; ++i) {

      int value = 0;

      if ( i >= C && i <= N - C) {
        value += input[i - C]; // N
        value += input[i + C]; // S
        value += input[i]; // C
      }
      else {
        value = input[i];
      }

      output[i] = value;
    } 
    #endif

    #if 0
    // 10. Working
    #pragma acc kernels \
        copyin(input[0:N]) copyout(output[0:N])
    {
      #pragma openarc transform window(input[0:N], output[0:N])
      #pragma acc loop gang(1) worker(1)
      for (int i = 0; i < N; ++i) {
        
        int value = 0;

        if ( i >= C && i <= N - C) {
          value += input[i - C]; // N
          value += input[i + C]; // S
          value += input[i]; // C
        }
        else {
          value = input[i];
        }

        output[i] = value;
      } 

    }
    #endif

    #if 0
    // 11. Working
    #pragma openarc transform window(input[0:N], output[0:N])
    #pragma acc kernels loop gang(1) worker(1) \
      copyin(input[0:N]) copyout(output[0:N]) collapse(2)
    for (int r = 0; r < R; ++r) {
      for (int c = 0; c < C; ++c) {

        int value = 0;

        if ( r >= 1 && r <= R - 1) {
          value += input[(r*C + c) - C]; // N
          value += input[(r*C + c) + C]; // S
          value += input[r*C + c]; // C
        }
        else {
          value = input[r*C + c];
        }

        output[r*C + c] = value;
      } // c
    } // r
    #endif

    #if 0
    // 12. Working
    #pragma acc kernels \
      copyin(input[0:N]) copyout(output[0:N]) 
    {
      #pragma openarc transform window(input[0:N], output[0:N])
      #pragma acc loop gang(1) worker(1) collapse(2)
      for (int r = 0; r < R; ++r) {
        for (int c = 0; c < C; ++c) {

          int value = 0;

          if ( r >= 1 && r <= R - 1) {
            value += input[(r*C + c) - C]; // N
            value += input[(r*C + c) + C]; // S
            value += input[r*C + c]; // C
          }
          else {
            value = input[r*C + c];
          }

          output[r*C + c] = value;
        } // c
      } // r
    } // acc
    #endif

    #if 0
    // 13. Working
    #pragma openarc transform window(input[0:N], output[0:N])
    #pragma acc kernels loop gang(1) worker(1)  \
      copyin(input[0:N]) copyout(output[0:N])
    #pragma unroll 4
    for (int i = 0; i < N; ++i) {

      int value = 0;

      if ( i >= C && i <= N - C) {
        value += input[i - C]; // N
        value += input[i + C]; // S
        value += input[i]; // C
      }
      else {
        value = input[i];
      }

      output[i] = value;
    } 
    #endif

    #if 0
    // 14. Working
    #pragma acc kernels copyin(input[0:N]) copyout(output[0:N])
    {
      #pragma openarc transform window(input[0:N], output[0:N])
      #pragma acc loop gang(1) worker(1)
      #pragma unroll 4
      for (int i = 0; i < N; ++i) {
        
        int value = 0;

        if ( i >= C && i <= N - C) {
          value += input[i - C]; // N
          value += input[i + C]; // S
          value += input[i]; // C
        }
        else {
          value = input[i];
        }

        output[i] = value;
      } 

    }
    #endif

    #if 0
    // 15. Working
    #pragma openarc transform window(input[0:N], output[0:N])
    #pragma acc kernels loop gang(1) worker(1) \
      copyin(input[0:N]) copyout(output[0:N]) collapse(2)
    #pragma unroll 4
    for (int r = 0; r < R; ++r) {
      for (int c = 0; c < C; ++c) {

        int value = 0;

        if ( r >= 1 && r <= R - 1) {
          value += input[(r*C + c) - C]; // N
          value += input[(r*C + c) + C]; // S
          value += input[r*C + c]; // C
        }
        else {
          value = input[r*C + c];
        }

        output[r*C + c] = value;
      } // c
    } // r
    #endif

    #if 0
    // 16. Working
    #pragma acc kernels \
      copyin(input[0:N]) copyout(output[0:N]) 
    {
      #pragma openarc transform window(input[0:N], output[0:N]) 
      #pragma acc loop gang(1) worker(1) collapse(2)
      #pragma unroll 4
      for (int r = 0; r < R; ++r) {
        for (int c = 0; c < C; ++c) {

          int value = 0;

          if ( r >= 1 && r <= R - 1) {
            value += input[(r*C + c) - C]; // N
            value += input[(r*C + c) + C]; // S
            value += input[r*C + c]; // C
          }
          else {
            value = input[r*C + c];
          }

          output[r*C + c] = value;
        } // c
      } // r
    } // acc
    #endif

    // Verification
    for (int i = C; i < N - C - 1; ++i) {
      if (output[i] != input[i]*3) {
        printf("Verfication Failed:\n  output[%d] = %d\n  3*input[%d] = %d\n",
            i, output[i], i, 3*input[i]);
        exit(0);
      }
    }

    printf("Verification Successful!\n");

    return 0;
}
