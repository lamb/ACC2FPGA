#include <stdio.h>
#include <stdlib.h>
#define R 12
#define C 8
#define P 16
//#define D 2
//#define SZ (D*P*R*C)
#define SZ (R*C)

#pragma openarc #define R 12
#pragma openarc #define C 8
#pragma openarc #define P 16
//#pragma openarc #define D 2
//#pragma openarc #define SZ (D*P*C*R)
#pragma openarc #define SZ (R*C*P)

int main(int argc, char** argv) {

  //int array[P][R][C];
  int array[R][C];
  //for (int p = 0; p < P; ++p) 
  for (int r = 0; r < R; ++r) 
    for (int c = 0; c < C; ++c) 
      //array[p][r][c] = -1;
      array[r][c] = -1;

    /* Collapse parallel 
     * 1. Non-nested            Working
     * 2. Nested                Working
     * 3. Nested (no gang/work) Working
     */

    /* Collapse kernels 
     * 1. Non-nested            Working
     * 2. Nested                Working
     */

    #if 1
    // 1. Working
    
    int sum = 0;

    #pragma acc data copy(array[0:P][0:R][0:C])
    {
      #pragma acc parallel loop num_gangs(1) num_workers(1) collapse(2) 
      //for (int p = 1; p < P; ++p) 
      for (int r = 1; r < R; ++r) 
      for (int c = 1; c < C; ++c) 
        //array[p][r][c] = p*R*C + r*C + c;
        sum += array[r][c];
        array[r][c] = r*C + c;
    }
    #endif

    #if 0
    // 1.5 Working
    #pragma acc parallel loop num_gangs(1) num_workers(1) collapse(3) copyout(array[0:SZ])
    for (int p = 0; p < P; ++p) {
      for (int r = 0; r < R; ++r) {
        for (int c = 0; c < C; ++c) {

          array[r*C + c] = r*C + c;
        }
      }
    }
#endif

#if 0
    // 2. Working
#pragma acc parallel num_gangs(1) num_workers(1) copyout(array[0:SZ])
    {
#pragma acc loop collapse(3)
      for (int p = 0; p < P; ++p) {
        for (int r = 0; r < R; ++r) {
          for (int c = 0; c < C; ++c) {

            array[r*C + c] = r*C + c;
          }
        }
      }
    }
#endif

#if 0
    // 3. Working
#pragma acc kernels loop gang(1) worker(1) copyout(array[0:SZ]) collapse(3)
    for (int p = 0; p < P; ++p) {
      for (int r = 0; r < R; ++r) {
        for (int c = 0; c < C; ++c) {

          array[r*C + c] = r*C + c;
        }
      }
    }
#endif

#if 0
    // 4. Working
    //int p, r, c;
#pragma acc kernels copyout(array[0:SZ])
    {
#pragma acc loop gang(1) worker(1) collapse(3)
      for (int p = 0; p < P; ++p) {
        for (int r = 0; r < R; ++r) {
          for (int c = 0; c < C; ++c) {

            array[r*C + c] = r*C + c;
          }
        }
      }
    }
#endif

#if 0
    // 5. Working
#pragma acc kernels copyout(array[0:SZ])
    {
#pragma acc loop collapse(3)
      for (int p = 0; p < P; ++p) {
        for (int r = 0; r < R; ++r) {
          for (int c = 0; c < C; ++c) {

            array[r*C + c] = r*C + c;
          }
        }
      }
    }
#endif

    //for (int p = 0; p < P; ++p) {
    for (int r = 0; r < R; ++r) {
      for (int c = 0; c < C; ++c) {

        int ver;
        //if (p > 0 && r > 0 && c > 0)
        //  ver = p*R*C + r*C + c; 
        if (r > 0 && c > 0)
          ver = r*C + c;
        else
          ver = -1;

        //if (array[p][r][c] != ver) {
        if (array[r][c] != ver) {
          printf("Verification Failed!\n");
          //printf("ver = %d, array[%d][%d][%d] = %d\n", ver, p, r, c, array[p][r][c]);
          printf("ver = %d, array[%d][%d] = %d\n", ver, r, c, array[r][c]);
          exit(0);
        }

      }
    }
    //}

    printf("Verification Successful!\n");

    return 0;
}
