#include <stdio.h>
#include <stdlib.h>
#define R 10
#define C 15
#define N R*C

#pragma openarc #define R 10
#pragma openarc #define C 15
#pragma openarc #define N R*C


// Computation
int main(int argc, char** argv) {

  int *input, *output;

  input = (int *) malloc(N * sizeof(int));
  int sum = 0;
  int sum2 = 0;

  for (int k = 0; k < N; ++k) input[k] = k;

  /* Parallel Clause */
  /* 1. Single Pragma -             Working
   * 2. Nested Pragma -             Working
   * 3. Nested Pragma + collapse -  Working 
   * 4. Single Pragma + collapse -  Working
   */

  /* Kernels Clause */
  /* 5. Single Pragma -             Working
   * 5.25                           Working
   * 5.5                            Not Working
   * 6. Nested Pragma -             Working
   * 6.5                            Working
   * 7. Nested Pragma + collapse -  Working 
   * 7.5                            Working
   * 8. Single Pragma + collapse -  Working
   */

  #if 0
  // 1. Working
  #pragma acc data copyin(input[0:N])
  {
    #pragma acc parallel loop num_gangs(1) num_workers(1) reduction(+:sum,sum2) 
    for (int i = 0; i < N; ++i) {
      int tmp = input[i];
      sum += tmp;
      sum2 += tmp*tmp;
    }
  }
  #endif

  #if 0
  // 2. Working
  #pragma acc parallel num_gangs(1) num_workers(1) copyin(input[0:N])
  {
    #pragma acc loop reduction(+:sum, sum2) 
    for (int i = 0; i < N; ++i) {
      int tmp = input[i];
      sum += tmp;
      sum2 += tmp*tmp;
    }
  }
  #endif

  #if 0
  // 3. Working
  // sum and sum2 not passed to kernel
  #pragma acc parallel num_gangs(1) num_workers(1) copyin(input[0:N])
  {
    #pragma acc loop collapse(2) reduction(+:sum, sum2) 
    for (int r = 0; r < R; ++r) {
      for (int c = 0; c < C; ++c) {
        int tmp = input[r*C + c];
        sum += tmp;
        sum2 += tmp*tmp;
      }
    }
  }
  #endif

  #if 1
  // 4. Working
  #pragma acc data copyin(input[0:N])
  {
    #pragma acc parallel loop num_gangs(1) num_workers(1) \
      collapse(2) reduction(+:sum, sum2) 
    for (int r = 0; r < R; ++r) {
      for (int c = 0; c < C; ++c) {
        int tmp = input[r*C + c];
        sum += tmp;
        sum2 += tmp*tmp;
      }
    }
  }
  #endif

  #if 0
  // 5. Working
  // Extra acc statement
  #pragma acc data copyin(input[0:N])
  {
    #pragma acc kernels loop independent gang(1) worker(1) reduction(+:sum, sum2) 
    for (int i = 0; i < N; ++i) {
      int tmp = input[i];
      sum += tmp;
      sum2 += tmp*tmp;
    }
  }
  #endif

  #if 0
  // 5.25 Working
  // Extra acc statement
  #pragma acc kernels loop gang(1) worker(1) reduction(+:sum, sum2) copyin(input[0:N])
  for (int i = 0; i < N; ++i) {
    int tmp = input[i];
    sum += tmp;
    sum2 += tmp*tmp;
  }
  #endif

  #if 0
  // 5.5 Not Working
  //  Undeclared symbol sum, sum2
  //  Seems like it is applying the SR-reduction even though it isn't SWI
  #pragma acc data copyin(input[0:N])
  {
    #pragma acc kernels loop reduction(+:sum, sum2) 
    for (int i = 0; i < N; ++i) {
      int tmp = input[i];
      sum += tmp;
      sum2 += tmp*tmp;
    }
  }
  #endif

  #if 0
  // 6. Working
  int i;
  #pragma acc kernels copyin(input[0:N])
  {
    #pragma acc loop gang(1) worker(1) reduction(+:sum, sum2) 
    for (i = 0; i < N; ++i) {
      int tmp = input[i];
      sum += tmp;
      sum2 += tmp*tmp;
    }
  }
  #endif

  #if 0
  // 6.5 Working
  #pragma acc kernels copyin(input[0:N])
  {
    #pragma acc loop reduction(+:sum, sum2) 
    for (int i = 0; i < N; ++i) {
      int tmp = input[i];
      sum += tmp;
      sum2 += tmp*tmp;
    }
  }
  #endif

  #if 0
  // 7. Working
  #pragma acc kernels copyin(input[0:N])
  {
    #pragma acc loop gang(1) worker(1) collapse(2) reduction(+:sum, sum2) 
    for (int r = 0; r < R; ++r) {
      for (int c = 0; c < C; ++c) {
        int tmp = input[r*C + c];
        sum += tmp;
        sum2 += tmp*tmp;
      }
    }
  }
  #endif

  #if 0
  // 7.5 Working
  int r, c;
  #pragma acc kernels copyin(input[0:N])
  {
    #pragma acc loop gang(1) worker(1) collapse(2) reduction(+:sum, sum2) 
    for (r = 0; r < R; ++r) {
      for (c = 0; c < C; ++c) {
        int tmp = input[r*C + c];
        sum += tmp;
        sum2 += tmp*tmp;
      }
    }
  }
  #endif

  #if 0
  // 8. Working
  #pragma acc data copyin(input[0:N])
  {
    #pragma acc kernels loop gang(1) worker(1) \
      collapse(2) reduction(+:sum, sum2) 
    #pragma unroll 16
    for (int r = 0; r < R; ++r) {
      for (int c = 0; c < C; ++c) {
        int tmp = input[r*C + c];
        sum += tmp;
        sum2 += tmp*tmp;
      }
    }
  }
  #endif
  
  //----------------
  #if 0
  // 9. Not Working
  // Unexplored
  #pragma acc data copyin(input[0:N])
  {
    #pragma acc parallel loop num_gangs(1) num_workers(1) reduction(+:sum, sum2) 
    for (int r = 0; r < R; ++r) {
      for (int c = 0; c < C; ++c) {
        int tmp = input[r*C + c];
        sum += tmp;
        sum2 += tmp*tmp;
      }
    }
  }
  #endif

  int sum_ver = 0;
  int sum2_ver = 0;
  for (int k = 0; k < N; ++k) {
    sum_ver += input[k];
    sum2_ver += input[k]*input[k];
  }

  if (sum == sum_ver && sum2 == sum2_ver)
    printf("Verification Successful!\n");
  else
    printf("Verfication Failed:\n  sum = %d sum2 = %d\n  sum_ver = %d sum2_ver = %d\n", sum, sum2, sum_ver, sum2_ver);

  return 0;
}
