#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#define MIN(a,b) (((a) < (b)) ? (a) : (b))

//#define ITER 66

#define NSEW 0
// Used in Hotspot and SRAD
//    N
// W  T  E
//    S

#define SOBEL 1
// P P P
// P P P
// P P T

#define CANNY 0
// NW NN NE
// WW TT EE
// SW SS SE

#define FD3D 0
// 3k+1 input points (k adjacent points in each direction
//-3xy -2xy -xy -3x -2x -x -3 -2 -1 0 1 2 3 x 2x 3x xy 2xy 3xy

#define R_SIZE (64) // number of rows
#define C_SIZE (64) // number of cols
#define Z_SIZE (64) // number of planes (FD3D only)

#pragma openarc #define R_SIZE (64)
#pragma openarc #define C_SIZE (64)
#pragma openarc #define Z_SIZE (64)

#if FD3D
  #define DATA_SIZE C_SIZE*R_SIZE*Z_SIZE
  #pragma openarc #define DATA_SIZE (C_SIZE*R_SIZE*Z_SIZE)
#else
  #define DATA_SIZE C_SIZE*R_SIZE
  #pragma openarc #define DATA_SIZE (C_SIZE*R_SIZE)
#endif

void print_window(float * sw, char const *s, int sw_size) {
  int ii;
  for (ii = 0; ii < sw_size ; ii++) printf("%s[%d] = %f\n", s, ii, sw[ii]);
}

float *input;
float *output;
float *verify;

void basic();
void fpga();

int main(int argc, char **argv)
{

  int i, j;

  #if NSEW
  printf("NSEW Stencil\n");
  printf("%d x %d\n", R_SIZE, C_SIZE);
  #elif SOBEL
  printf("SOBEL Stencil\n");
  printf("%d x %d\n", R_SIZE, C_SIZE);
  #elif CANNY
  printf("CANNY Stencil\n");
  printf("%d x %d\n", R_SIZE, C_SIZE);
  #elif FD3D 
  printf("FD3D Stencil\n");
  printf("%d x %d x %d\n", R_SIZE, C_SIZE, Z_SIZE);
  #endif

  input  = (float *) malloc(sizeof(float)*DATA_SIZE);
  output = (float *) malloc(sizeof(float)*DATA_SIZE);
  verify = (float *) malloc(sizeof(float)*DATA_SIZE);

  for (i = 0; i < DATA_SIZE; i++) {
    //input[i] = (double)rand() / (double)RAND_MAX ; 
    input[i] = i;
    output[i] = 0;
    verify[i] = 0;
  }
  
  input[0] = 0.1;

#pragma acc data copyin(input[0:DATA_SIZE]) \
  copyout(output[0:DATA_SIZE])
  {
    printf("\n--Running Accelerator version\n");
    fpga();
  }

  printf("\n--Running CPU version\n");
  basic();

  int flag = 1;
  for (i = 0; i < DATA_SIZE; i++) {
    if (output[i] != verify[i]) {
      #if SOBEL
      if (i % R_SIZE > 1) {
        printf("man[%d] = %5.1f, auto[%d] = %5.1f\n", i, output[i], i, verify[i]);
        flag = 0;
        exit(0);
      }
      #else
      printf("man[%d] = %5.1f, auto[%d] = %5.1f\n", i, output[i], i, verify[i]);
      flag = 0;
      exit(0);
      #endif 

    }
  }
  (flag) ? printf("Verification PASS\n") : printf("Verification FAIL\n");
}


#if NSEW
void basic() {
  int i, j;

  for (i = 0; i < R_SIZE * C_SIZE ; i++) {

    int r = i / R_SIZE;
    int c = i % R_SIZE;

    float result = 0.0;
    float N=0.0, S=0.0, E=0.0, W=0.0;

    // Corner 1
    if (r == 0 && c == 0) {
      S = input[i + C_SIZE];
      E = input[i + 1];
    }
    // Corner 2
    else if (r == 0 && c == C_SIZE - 1) {
      S = input[i + C_SIZE];
      W = input[i - 1]; 
    }
    // Corner 3
    else if (r == R_SIZE - 1 && c == C_SIZE - 1) {
      N = input[i - C_SIZE];
      W = input[i - 1]; 
    }
    // Corner 4
    else if (r == R_SIZE - 1 && c == 0) {
      N = input[i - C_SIZE];
      E = input[i + 1];
    }
    // Edge 1
    else if (r == 0) {
      S = input[i + C_SIZE];
      E = input[i + 1];
      W = input[i - 1]; 
    } 
    // Edge 2
    else if (c == C_SIZE - 1) {
      N = input[i - C_SIZE];
      S = input[i + C_SIZE];
      W = input[i - 1]; 
    } 
    // Edge 3
    else if (r == R_SIZE - 1) {
      N = input[i - C_SIZE];
      E = input[i + 1];
      W = input[i - 1]; 
    } 
    // Edge 4
    else if (c == 0) {
      N = input[i - C_SIZE];
      S = input[i + C_SIZE];
      E = input[i + 1];
    } 
    // Inside 
    else {
      N = input[i - C_SIZE];
      S = input[i + C_SIZE];
      E = input[i + 1];
      W = input[i - 1]; 
    } 

    verify[i] = input[i] + N + E + S + W;
  }
}


// For this pattern, diff can be calculated at compile-time
void fpga() {
  int i, j;

#pragma acc parallel num_workers(1) num_gangs(1) \
  present(input[0:R_SIZE*C_SIZE], output[0:R_SIZE*C_SIZE])
  {
    #define SSIZE (8)
    #define BSIZE (32)
    #define TARGET (BSIZE)
    #define SW_BASE_SIZE (2*BSIZE)
    #define SW_SIZE (SW_BASE_SIZE + SSIZE)

    #define INIT_ITER (1)

    //printf("init iters: %f\n", 
    //    ceil( (float) (SW_BASE_SIZE - TARGET) / (float) BSIZE) );
    //printf("BSIZE: %d, SSIZE: %d\n", BSIZE, SSIZE);

    float sw[SW_SIZE];
    #pragma unroll
    for (int ii = 0; ii < SW_SIZE; ++ii) {
      sw[ii] = 0;
    }

    int x = 0, y = 0, bx = 0;
    do {
      int gx = bx + x;
      int comp_offset_y = y - INIT_ITER;
      int comp_offset = gx + comp_offset_y * R_SIZE;
      int read_offset = bx + (x + SW_BASE_SIZE - TARGET) % BSIZE +
                       (((x + SW_BASE_SIZE - TARGET) / BSIZE) + comp_offset_y)*C_SIZE; 
      int block_offset = x + BSIZE * comp_offset_y;
      int sw_offset = TARGET - block_offset;

      //if (x == 0 && y == 0) printf("Starting Column Block %d\n", bx / 32);
  
      #pragma unroll
      for (int ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + SSIZE];
      }

      #pragma unroll
      for (int ii = 0; ii < SSIZE; ++ii) {
        sw[SW_BASE_SIZE + ii] = read_offset + ii < 0 ? 0 : input[read_offset + ii]; 
      }

      float value[SSIZE];

      if (comp_offset_y >= 0 && comp_offset_y < R_SIZE) {

        #pragma unroll
        for (int ii = 0; ii < SSIZE; ++ii) {
          //int r = (comp_offset + ii) / R_SIZE;
          //int c = (comp_offset + ii) % R_SIZE;
          int r = comp_offset_y;
          int c = gx + ii;

          float result = 0.0;
          float N=0, S=0, E=0, W=0;

          // Corner 1
          if (r == 0 && c == 0) {
            S = sw[block_offset + BSIZE + sw_offset + ii];
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ? 
                input[comp_offset + SSIZE] :
                sw[block_offset+ii + 1 + sw_offset];
            
          }
          // Corner 2
          else if (r == 0 && c == C_SIZE - 1) {
            S = sw[block_offset + BSIZE + sw_offset + ii];
            W = (ii == 0 && x == 0) ?
                input[comp_offset - 1] :
                sw[block_offset+ii - 1 + sw_offset]; 
          }
          // Corner 3
          else if (r == R_SIZE - 1 && c == C_SIZE - 1) {
            N = sw[block_offset - BSIZE + sw_offset + ii];
            W = (ii == 0 && x == 0) ?
                input[comp_offset - 1] :
                sw[block_offset+ii - 1 + sw_offset]; 
          }
          // Corner 4
          else if (r == R_SIZE - 1 && c == 0) {
            N = sw[block_offset - BSIZE + sw_offset + ii];
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ? 
                input[comp_offset + SSIZE] :
                sw[block_offset+ii + 1 + sw_offset];
          }
          // Edge 1
          else if (r == 0) {
            S = sw[block_offset + BSIZE + sw_offset + ii];
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ? 
                input[comp_offset + SSIZE] :
                sw[block_offset+ii + 1 + sw_offset];
            W = (ii == 0 && x == 0) ?
                input[comp_offset - 1] :
                sw[block_offset+ii - 1 + sw_offset]; 
          } 
          // Edge 2
          else if (c == C_SIZE - 1) {
            N = sw[block_offset - BSIZE + sw_offset + ii];
            S = sw[block_offset + BSIZE + sw_offset + ii];
            W = (ii == 0 && x == 0) ?
                input[comp_offset - 1] :
                sw[block_offset+ii - 1 + sw_offset]; 
          } 
          // Edge 3
          else if (r == R_SIZE - 1) {
            N = sw[block_offset - BSIZE + sw_offset + ii];
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ? 
                input[comp_offset + SSIZE] :
                sw[block_offset+ii + 1 + sw_offset];
            W = (ii == 0 && x == 0) ?
                input[comp_offset - 1] :
                sw[block_offset+ii - 1 + sw_offset]; 
          } 
          // Edge 4
          else if (c == 0) {
            N = sw[block_offset - BSIZE + sw_offset + ii];
            S = sw[block_offset + BSIZE + sw_offset + ii];
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ? 
                input[comp_offset + SSIZE] :
                sw[block_offset+ii + 1 + sw_offset];
          } 
          // Inside 
          else {
            N = sw[block_offset - BSIZE + sw_offset + ii];
            S = sw[block_offset + BSIZE + sw_offset + ii];
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ? 
                input[comp_offset + SSIZE] :
                sw[block_offset+ii + 1 + sw_offset];
            W = (ii == 0 && x == 0) ?
                input[comp_offset - 1] :
                sw[block_offset+ii - 1 + sw_offset]; 
          } 

          //output[block_offset] = sw[block_offset + sw_offset] + N + E + S + W;
          value[ii] = sw[block_offset + sw_offset + ii] + N + E + S + W;
        }

        #pragma unroll
        for (int ii = 0; ii < SSIZE; ++ii) {
          output[comp_offset + ii] = value[ii];
        }
      }

      x = x < BSIZE - SSIZE ? x + SSIZE : 0;
      y = x == 0 ? (y == R_SIZE + INIT_ITER - 1 ? 0 : y + 1) : y;
      if (x == 0 && y == 0) bx += BSIZE;

    } while (bx < C_SIZE);
  } // exit kernel
}
#endif

#if SOBEL 
void basic() {
  int i, j, k;

  for (i = 0; i < R_SIZE * C_SIZE ; i++) {
    
    float result = 0;

    for (j = 0; j < 3; j++) {
      for (k = 0; k < 3; k++) {

        if (i - (j*C_SIZE) - k >= 0) { 
          result += input[i - (j*C_SIZE) - k]; 
        }
      }
    }

    verify[i] = result;
  }
}

void fpga() {

#pragma acc parallel num_workers(1) num_gangs(1) \
  present(input[0:R_SIZE*C_SIZE], output[0:R_SIZE*C_SIZE])
  {
    #define SSIZE (4)
    #define BSIZE (32)
    #define TARGET (2*BSIZE + 2)
    #define SW_BASE_SIZE (2*BSIZE + 2)
    #define SW_SIZE (SW_BASE_SIZE + SSIZE)

    #define INIT_ITER (0)

    //printf("init iters: %f\n", 
    //    ceil( (float) (SW_BASE_SIZE - TARGET) / (float) BSIZE) );
    //printf("BSIZE: %d, SSIZE: %d\n", BSIZE, SSIZE);

    float sw[SW_SIZE];
    #pragma unroll
    for (int ii = 0; ii < SW_SIZE; ++ii) {
      sw[ii] = 0;
    }

    int x = 0, y = 0, bx = 0;
    do {
      int gx = bx + x;
      int comp_offset_y = y - INIT_ITER;
      int comp_offset = gx + comp_offset_y * R_SIZE;
      int read_offset = bx + (x + SW_BASE_SIZE - TARGET) % BSIZE +
                       (((x + SW_BASE_SIZE - TARGET) / BSIZE) + comp_offset_y)*C_SIZE;  

      int block_offset = x + BSIZE * comp_offset_y;
      int sw_offset = TARGET - block_offset;

      #pragma unroll
      for (int ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + SSIZE];
      }

      #pragma unroll
      for (int ii = 0; ii < SSIZE; ++ii) {
        sw[SW_BASE_SIZE + ii] = read_offset + ii < 0 ? 0 : input[read_offset + ii];
      }

      float value[SSIZE];

      if (comp_offset_y >= 0 && comp_offset_y < R_SIZE) {

        #pragma unroll
        for (int ii = 0; ii < SSIZE; ii++) {
          float result = 0;

          #pragma unroll
          for (int j = 0; j < 3; j++) {
            #pragma unroll
            for (int k = 0; k < 3; k++) {

              if ( (comp_offset+ii) - (j*C_SIZE) - k >= 0) { 

                //float v = (x+ii) < 2 ?
                float v = (x == 0 && ii < 2) ?
                  input[comp_offset - (j*C_SIZE) - k + ii] :         
                  sw[block_offset - (j*BSIZE) - k + sw_offset + ii];          

                result += v;
              }
            }
          }

          value[ii] = result;
        }
        
        #pragma unroll
        for (int ii = 0; ii < SSIZE; ++ii) {
          output[comp_offset + ii] = value[ii];
        }
      }

      x = x < BSIZE - SSIZE ? x + SSIZE: 0; 
      y = x == 0 ? (y == R_SIZE + INIT_ITER - 1 ? 0 : y + 1) : y;
      if (x == 0 && y == 0) bx += BSIZE;
    } while (bx < C_SIZE);
  }
}
#endif

#if CANNY 
void basic() {
  int i, j;

  for (i = 0; i < R_SIZE * C_SIZE ; i++) {

    int r = i / R_SIZE;
    int c = i % R_SIZE;

    float result = 0.0;
    float NN=0.0, SS=0.0, EE=0.0, WW=0.0;
    float NE=0.0, SE=0.0, NW=0.0, SW=0.0;
    

    // Corner 1
    if (r == 0 && c == 0) {
      SS = input[i + C_SIZE];
      EE = input[i + 1];
      
      SE = input[i + C_SIZE + 1];
    }
    // Corner 2
    else if (r == 0 && c == C_SIZE - 1) {
      SS = input[i + C_SIZE];
      WW = input[i - 1]; 

      SW = input[i + C_SIZE - 1];
    }
    // Corner 3
    else if (r == R_SIZE - 1 && c == C_SIZE - 1) {
      NN = input[i - C_SIZE];
      WW = input[i - 1]; 

      NW = input[i - C_SIZE - 1];
    }
    // Corner 4
    else if (r == R_SIZE - 1 && c == 0) {
      NN = input[i - C_SIZE];
      EE = input[i + 1];

      NE = input[i - C_SIZE + 1];
    }
    // Edge 1
    else if (r == 0) {
      SS = input[i + C_SIZE];
      EE = input[i + 1];
      WW = input[i - 1]; 

      SE = input[i + C_SIZE + 1];
      SW = input[i + C_SIZE - 1];
    } 
    // Edge 2
    else if (c == C_SIZE - 1) {
      NN = input[i - C_SIZE];
      SS = input[i + C_SIZE];
      WW = input[i - 1]; 

      NW = input[i - C_SIZE - 1];
      SW = input[i + C_SIZE - 1];
    } 
    // Edge 3
    else if (r == R_SIZE - 1) {
      NN = input[i - C_SIZE];
      EE = input[i + 1];
      WW = input[i - 1]; 

      NE = input[i - C_SIZE + 1];
      NW = input[i - C_SIZE - 1];
    } 
    // Edge 4
    else if (c == 0) {
      NN = input[i - C_SIZE];
      SS = input[i + C_SIZE];
      EE = input[i + 1];

      NE = input[i - C_SIZE + 1];
      SE = input[i + C_SIZE + 1];
    } 
    // Inside 
    else {
      NN = input[i - C_SIZE];
      SS = input[i + C_SIZE];
      EE = input[i + 1];
      WW = input[i - 1]; 

      NE = input[i - C_SIZE + 1];
      SE = input[i + C_SIZE + 1];
      NW = input[i - C_SIZE - 1];
      SW = input[i + C_SIZE - 1];
    } 

    verify[i] = input[i] + NN + EE + SS + WW + NE + NW + SE + SW;
  }
}

void fpga() {
  int i, j;

#pragma acc parallel num_workers(1) num_gangs(1) \
  present(input[0:R_SIZE*C_SIZE], output[0:R_SIZE*C_SIZE])
  {
    #define SSIZE (4)
    #define BSIZE (32)
    #define TARGET (BSIZE + 1)
    #define SW_BASE_SIZE (2*BSIZE + 2)
    #define SW_SIZE (SW_BASE_SIZE + SSIZE)

    #define INIT_ITER 2

    //printf("init iters: %f\n", 
    //    ceil( (float) (SW_BASE_SIZE - TARGET) / (float) BSIZE) );
    //printf("BSIZE: %d, SSIZE: %d\n", BSIZE, SSIZE);

    float sw[SW_SIZE];
    #pragma unroll
    for (int ii = 0; ii < SW_SIZE; ++ii) {
      sw[ii] = 0;
    }

    int x = 0, y = 0, bx = 0;
    do {
      int gx = bx + x;
      int comp_offset_y = y - INIT_ITER;
      int comp_offset = gx + comp_offset_y * R_SIZE;
      //int read_offset = 
      //    bx + (x + BSIZE + 1) % BSIZE + 
      //    ( ( (x + BSIZE + 1) / BSIZE ) + comp_offset_y) * C_SIZE; 

      int block_offset = x + BSIZE * comp_offset_y;
      int sw_offset = TARGET - block_offset;

      #pragma unroll
      for (int ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + SSIZE];
      }
      // shift in zeros until we get to needed elements
      #pragma unroll
      for (int ii = 0; ii < SSIZE; ++ii) {
        // so this probably isn't great to have here...
        int read_offset = 
          bx + ( (x+ii) + BSIZE + 1) % BSIZE + 
          ( ( ( (x+ii) + BSIZE + 1) / BSIZE ) + comp_offset_y) * C_SIZE; 

        sw[SW_BASE_SIZE + ii] = read_offset < 0 ? 0 : input[read_offset];
      }

      float value[SSIZE];

      if (comp_offset_y >= 0 && comp_offset_y < R_SIZE) {

        #pragma unroll
        for (int ii = 0; ii < SSIZE; ++ii) {
          int r = (comp_offset + ii) / R_SIZE;
          int c = (comp_offset + ii) % R_SIZE;

          float result = 0.0;
          float NN=0.0, SS=0.0, EE=0.0, WW=0.0;
          float NE=0.0, SE=0.0, NW=0.0, SW=0.0;

          // Corner 1
          if (r == 0 && c == 0) {
            SS = sw[(block_offset + ii) + BSIZE + sw_offset];
            EE = sw[(block_offset + ii) + 1 + sw_offset];

            SE = sw[(block_offset + ii) + BSIZE + 1 + sw_offset];
          }
          // Corner 2
          else if (r == 0 && c == C_SIZE - 1) {
            SS = sw[(block_offset + ii) + BSIZE + sw_offset];
            WW = sw[(block_offset + ii) - 1 + sw_offset]; 

            SW = sw[(block_offset + ii) + BSIZE - 1 + sw_offset];
          }
          // Corner 3
          else if (r == R_SIZE - 1 && c == C_SIZE - 1) {
            NN = sw[(block_offset + ii) - BSIZE + sw_offset];
            WW = sw[(block_offset + ii) - 1 + sw_offset]; 

            NW = sw[(block_offset + ii) - BSIZE - 1 + sw_offset];
          }
          // Corner 4
          else if (r == R_SIZE - 1 && c == 0) {
            NN = sw[(block_offset + ii) - BSIZE + sw_offset];
            EE = sw[(block_offset + ii) + 1 + sw_offset];
            NE = sw[(block_offset + ii) - BSIZE + 1 + sw_offset];
          }
          // Edge 1
          else if (r == 0) {
            SS = sw[(block_offset + ii) + BSIZE + sw_offset];
            //EE = (BSIZE - 1) - (x+ii) < 1 ? 
            EE = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ?
              input[(comp_offset + ii) + 1] : 
              sw[(block_offset + ii) + 1 + sw_offset];
            //SE = (BSIZE - 1) - (x+ii) < 1 ? 
            SE = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ?
              input[(comp_offset + ii) + C_SIZE + 1] : 
              sw[(block_offset + ii) + BSIZE + 1 + sw_offset];

            //WW = (x+ii) < 1 ? 
            WW = (ii == 0 && x == 0) ?
              input[(comp_offset + ii) - 1] : 
              sw[(block_offset + ii) - 1 + sw_offset];
            //SW = (x+ii) < 1 ? 
            SW = (ii == 0 && x == 0) ?
              input[(comp_offset + ii) + C_SIZE - 1] : 
              sw[(block_offset + ii) + BSIZE - 1 + sw_offset];

          } 
          // Edge 2
          else if (c == C_SIZE - 1) {
            NN = sw[(block_offset + ii) - BSIZE + sw_offset];
            SS = sw[(block_offset + ii) + BSIZE + sw_offset];
            WW = sw[(block_offset + ii) - 1 + sw_offset]; 

            NW = sw[(block_offset + ii) - BSIZE - 1 + sw_offset];
            SW = sw[(block_offset + ii) + BSIZE - 1 + sw_offset];
          } 
          // Edge 3
          else if (r == R_SIZE - 1) {
            NN = sw[(block_offset + ii) - BSIZE + sw_offset];

            //float ee_1 = (BSIZE - 1) - (x+ii) < 1 ? 
            EE = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ?
              input[(comp_offset + ii) + 1] : 
              sw[(block_offset + ii) + 1 + sw_offset];
            //float ne_1 = (BSIZE - 1) - (x+ii) < 1 ? 
            NE = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ?
              input[(comp_offset + ii) - C_SIZE + 1] : 
              sw[(block_offset + ii) - BSIZE + 1 + sw_offset];

            //float ww_1 = (x+ii) < 1 ? 
            WW = (ii == 0 && x == 0) ?
              input[(comp_offset + ii) - 1] : 
              sw[(block_offset + ii) - 1 + sw_offset];
            //float nw_1 = (x+ii) < 1 ? 
            NW = (ii == 0 && x == 0) ?
              input[(comp_offset + ii) - C_SIZE - 1] : 
              sw[(block_offset + ii) - BSIZE - 1 + sw_offset];

          } 
          // Edge 4
          else if (c == 0) {
            NN = sw[(block_offset + ii) - BSIZE + sw_offset];
            SS = sw[(block_offset + ii) + BSIZE + sw_offset];
            EE = sw[(block_offset + ii) + 1 + sw_offset];

            NE = sw[(block_offset + ii) - BSIZE + 1 + sw_offset];
            SE = sw[(block_offset + ii) + BSIZE + 1 + sw_offset];
          } 
          // Inside 
          else {
            NN = sw[(block_offset + ii) - BSIZE + sw_offset];
            SS = sw[(block_offset + ii) + BSIZE + sw_offset];

            //if ( (BSIZE - 1) - (x+ii) < 1) {
            if (ii == SSIZE - 1 && x + SSIZE == BSIZE) {
              EE = input[(comp_offset + ii) + 1]; 
              NE = input[(comp_offset + ii) - C_SIZE + 1]; 
              SE = input[(comp_offset + ii) + C_SIZE + 1]; 
            }
            else {
              EE = sw[(block_offset + ii) + 1 + sw_offset];
              NE = sw[(block_offset + ii) - BSIZE + 1 + sw_offset];
              SE = sw[(block_offset + ii) + BSIZE + 1 + sw_offset];
            }

            // if ( ??? )
            if (x == 0 && ii == 0) {
              WW = input[(comp_offset + ii) - 1]; 
              NW = input[(comp_offset + ii) - BSIZE - 1]; 
              SW = input[(comp_offset + ii) + BSIZE - 1]; 
            }
            else {
              WW = sw[(block_offset + ii) - 1 + sw_offset];
              NW = sw[(block_offset + ii) - BSIZE - 1 + sw_offset];
              SW = sw[(block_offset + ii) + BSIZE - 1 + sw_offset];
            }

          } 

          value[ii] = 
            sw[(block_offset + ii) + sw_offset] + NN + EE + SS + WW + NE + NW + SE + SW;
        }
        
        #pragma unroll
        for (int ii = 0; ii < SSIZE; ++ii) {
          output[comp_offset + ii] =  value[ii];
        }

      }

      x = x < BSIZE - SSIZE ? x + SSIZE: 0;
      y = x == 0 ? (y == R_SIZE + INIT_ITER - 1? 0 : y + 1) : y;
      if (x == 0 && y == 0) bx += BSIZE;
    } while (bx < C_SIZE);
  }
}
#endif

#if FD3D

#define coord(x, y, z) ((z) * C_SIZE * R_SIZE + (y) * Z_SIZE + (x))
#define RADIUS 3
void basic() {

  int i;

  for (i = 0; i < R_SIZE*C_SIZE*Z_SIZE; i++) {
    int index = i;
    int x = index % C_SIZE;
    index /= C_SIZE;
    int y = index % R_SIZE;
    index /= C_SIZE; 
    int z = index;

    if (x >= RADIUS && x < C_SIZE - RADIUS &&
        y >= RADIUS && y < R_SIZE - RADIUS &&
        z >= RADIUS && z < Z_SIZE - RADIUS) {

      float result = input[i];   // T
      for (int r = 1; r <= RADIUS; r++) {
        result += input[coord(x - r, y, z)];  // -X
        result += input[coord(x + r, y, z)];  // +X
        result += input[coord(x, y - r, z)];  // -Y 
        result += input[coord(x, y + r, z)];  // +Y
        result += input[coord(x, y, z - r)];  // -Z
        result += input[coord(x, y, z + r)];  // +Z
      }
      verify[i] = result;
    }
    else {
      verify[i] = input[i];
    }
  }
}

void fpga() {

#pragma acc parallel num_workers(1) num_gangs(1) \
  present(input[0:R_SIZE*C_SIZE], output[0:R_SIZE*C_SIZE])
  {
    #define BSIZE_X  // DIMX
    #define BSIZE_Y  // DIMY

    #define TARGET (RADIUS * BSIZE_X * BSIZE_Y)
    #define SW_BASE_SIZE (2 * RADIUS * BSIZE_X * BSIZE_Y)
    #define SW_SIZE (SW_BASE_SIZE + 1)

    float sw[SW_SIZE];
    for (int ii = 0; ii < SW_SIZE; ii++) sw[ii] = 0;

    
    int x = 0, y = 0, xtile = 0, ytile = 0, ztile = 0;
    for (int i = 0; i < R_SIZE*C_SIZE*Z_SIZE; i++) {
      int index = i;
      int x_coord = index % C_SIZE;
      index /= C_SIZE;
      int y_coord = index % R_SIZE;
      index /= C_SIZE; 
      int z_coord = index;

      int read_offset = ztile*C_SIZE*R_SIZE + (y + ytile) * C_SIZE + (x + xtile);
      int xoutput = x + xtile, youtput = y + ytile, zoutput = ztile - RADIUS;
      int comp_offset = zoutput * C_SIZE * R_SIZE + youtput * C_SIZE + xoutput;
      // this is as far as I've made it on this one

      #pragma unroll
      for (int ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + 1];
      }
      // shift in zeros until we get to needed elements
      sw[SW_BASE_SIZE] = read_offset < 0 ? 0 : input[read_offset];

      if (x_coord >= RADIUS && x_coord < C_SIZE - RADIUS &&
          y_coord >= RADIUS && y_coord < R_SIZE - RADIUS &&
          z_coord >= RADIUS && z_coord < Z_SIZE - RADIUS) {

        float result = input[i];   // T
        for (int r = 1; r <= RADIUS; r++) {
          result += input[i - r];  // -X
          result += input[i + r];  // +X
          result += input[i - r * C_SIZE];  // -Y 
          result += input[i + r * C_SIZE];  // +Y
          result += input[i - r * C_SIZE*R_SIZE];  // -Z
          result += input[i + r * C_SIZE*R_SIZE];  // +Z
        }
        output[i] = result;
      }
      else {
        output[i] = input[i];
      }
    }
  } // exit acc 
}
#endif
