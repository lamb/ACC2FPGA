#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#define MIN(a,b) (((a) < (b)) ? (a) : (b))

//#define ITER 66

#define NSEW 0
// Used in Hotspot and SRAD
//    N
// W  T  E
//    S

#define SOBEL 0
// P P P
// P P P
// P P T

#define CANNY 0
// NW NN NE
// WW TT EE
// SW SS SE

#define FD3D 1
// 3k+1 input points (k adjacent points in each direction
//-3xy -2xy -xy -3x -2x -x -3 -2 -1 0 1 2 3 x 2x 3x xy 2xy 3xy

#define R_SIZE (32) // number of rows
#define C_SIZE (32) // number of cols
#define Z_SIZE (32) // number of planes (FD3D only)

#pragma openarc #define R_SIZE (32)
#pragma openarc #define C_SIZE (32)
#pragma openarc #define Z_SIZE (32)

#if FD3D
  #define DATA_SIZE C_SIZE*R_SIZE*Z_SIZE
  #pragma openarc #define DATA_SIZE (C_SIZE*R_SIZE*Z_SIZE)
#else
  #define DATA_SIZE C_SIZE*R_SIZE
  #pragma openarc #define DATA_SIZE (C_SIZE*R_SIZE)
#endif

void print_window(float * sw, char const *s, int sw_size) {
  int ii;
  for (ii = 0; ii < sw_size ; ii++) printf("%s[%d] = %f\n", s, ii, sw[ii]);
}

float *input;
float *output;
float *verify;

void basic();
void fpga();

int main(int argc, char **argv)
{

  int i, j;

  #if NSEW
  printf("NSEW Stencil\n");
  #elif SOBEL
  printf("SOBEL Stencil\n");
  #elif CANNY
  printf("CANNY Stencil\n");
  #elif FD3D 
  printf("FD3D Stencil\n");
  #endif

  

  input  = (float *) malloc(sizeof(float)*DATA_SIZE);
  output = (float *) malloc(sizeof(float)*DATA_SIZE);
  verify = (float *) malloc(sizeof(float)*DATA_SIZE);

  for (i = 0; i < DATA_SIZE; i++) {
    //input[i] = (double)rand() / (double)RAND_MAX ; 
    input[i] = i;
    output[i] = 0;
    verify[i] = 0;
  }
  
  input[0] = 0.1;

#pragma acc data copyin(input[0:DATA_SIZE]) \
  copyout(output[0:DATA_SIZE])
  {
    printf("\n  Running Accelerator version\n");
    fpga();
  }

  printf("\n  Running CPU version\n");
  basic();

  int flag = 1;
  for (i = 0; i < DATA_SIZE; i++) {
    if (output[i] != verify[i]) {
      #if SOBEL
      if (i % R_SIZE > 1) {
        printf("man[%d] = %5.1f, auto[%d] = %5.1f\n", i, output[i], i, verify[i]);
        flag = 0;
        exit(0);
      }
      #else
      printf("man[%d] = %5.1f, auto[%d] = %5.1f\n", i, output[i], i, verify[i]);
      flag = 0;
      exit(0);
      #endif 

    }
  }
  (flag) ? printf("Verification PASS\n") : printf("Verification FAIL\n");
}


#if NSEW
void basic() {
  int i, j;

  for (i = 0; i < R_SIZE * C_SIZE ; i++) {

    int r = i / R_SIZE;
    int c = i % R_SIZE;

    float result = 0.0;
    float N=0.0, S=0.0, E=0.0, W=0.0;

    // Corner 1
    if (r == 0 && c == 0) {
      S = input[i + C_SIZE];
      E = input[i + 1];
    }
    // Corner 2
    else if (r == 0 && c == C_SIZE - 1) {
      S = input[i + C_SIZE];
      W = input[i - 1]; 
    }
    // Corner 3
    else if (r == R_SIZE - 1 && c == C_SIZE - 1) {
      N = input[i - C_SIZE];
      W = input[i - 1]; 
    }
    // Corner 4
    else if (r == R_SIZE - 1 && c == 0) {
      N = input[i - C_SIZE];
      E = input[i + 1];
    }
    // Edge 1
    else if (r == 0) {
      S = input[i + C_SIZE];
      E = input[i + 1];
      W = input[i - 1]; 
    } 
    // Edge 2
    else if (c == C_SIZE - 1) {
      N = input[i - C_SIZE];
      S = input[i + C_SIZE];
      W = input[i - 1]; 
    } 
    // Edge 3
    else if (r == R_SIZE - 1) {
      N = input[i - C_SIZE];
      E = input[i + 1];
      W = input[i - 1]; 
    } 
    // Edge 4
    else if (c == 0) {
      N = input[i - C_SIZE];
      S = input[i + C_SIZE];
      E = input[i + 1];
    } 
    // Inside 
    else {
      N = input[i - C_SIZE];
      S = input[i + C_SIZE];
      E = input[i + 1];
      W = input[i - 1]; 
    } 

    verify[i] = input[i] + N + E + S + W;
  }
}


// For this pattern, diff can be calculated at compile-time
void fpga() {
  int i, j;

#pragma acc parallel num_workers(1) num_gangs(1) \
  present(input[0:R_SIZE*C_SIZE], output[0:R_SIZE*C_SIZE])
  {
    #define BSIZE (32)
    #define TARGET (BSIZE)
    #define SW_BASE_SIZE (2*BSIZE)
    #define SW_SIZE (SW_BASE_SIZE + 1)

    #define INIT_ITER (1)

    printf("init iters: %f\n", 
        ceil( (float) (SW_BASE_SIZE - TARGET) / (float) BSIZE) );

    float sw[SW_SIZE];
    int ii; 
    #pragma unroll
    for (ii = 0; ii < SW_SIZE; ++ii) {
      sw[ii] = 0;
    }

    int x = 0, y = 0, bx = 0;
    do {
      int gx = bx + x;
      int comp_offset_y = y - INIT_ITER;
      int comp_offset = gx + comp_offset_y * R_SIZE;
      int read_offset = bx + (x + SW_BASE_SIZE - TARGET) % BSIZE +
                       (((x + SW_BASE_SIZE - TARGET) / BSIZE) + comp_offset_y)*C_SIZE; 

      int block_offset = BSIZE * (y-1) + x;
      int sw_offset = TARGET - comp_offset;

      #pragma unroll
      for (ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + 1];
      }
      sw[SW_BASE_SIZE] = sw[SW_BASE_SIZE] = read_offset < 0 ? 0 : input[read_offset]; 

      if (comp_offset_y >= 0 && comp_offset_y < R_SIZE) {
        int r = comp_offset / R_SIZE;
        int c = comp_offset % R_SIZE;

        float result = 0.0;
        float N=0, S=0, E=0, W=0;

        // Corner 1
        if (r == 0 && c == 0) {
          float east_1 = (BSIZE - 1) - x < 1 ? 
            input[comp_offset + 1] : 
            sw[comp_offset + 1 + sw_offset];

          S = sw[comp_offset + BSIZE + sw_offset];
          E = east_1;
        }
        // Corner 2
        else if (r == 0 && c == C_SIZE - 1) {
          float west_1 = x < 1 ? 
            input[comp_offset - 1] : 
            sw[comp_offset - 1 + sw_offset];

          S = sw[comp_offset + BSIZE + sw_offset];
          W = west_1;
        }
        // Corner 3
        else if (r == R_SIZE - 1 && c == C_SIZE - 1) {
          float west_1 = x < 1 ? 
            input[comp_offset - 1] : 
            sw[comp_offset - 1 + sw_offset];

          N = sw[comp_offset - BSIZE + sw_offset];
          W = west_1;
        }
        // Corner 4
        else if (r == R_SIZE - 1 && c == 0) {
          float east_1 = (BSIZE - 1) - x < 1 ? 
            input[comp_offset + 1] : 
            sw[comp_offset + 1 + sw_offset];

          N = sw[comp_offset - BSIZE + sw_offset];
          E = east_1;
        }
        // Edge 1
        else if (r == 0) {
          float east_1 = (BSIZE - 1) - x < 1 ? 
            input[comp_offset + 1] : 
            sw[comp_offset + 1 + sw_offset];
          float west_1 = x < 1 ? 
            input[comp_offset - 1] : 
            sw[comp_offset - 1 + sw_offset];

          S = sw[comp_offset + BSIZE + sw_offset];
          E = east_1;
          W = west_1;
        } 
        // Edge 2
        else if (c == C_SIZE - 1) {
          float west_1 = x < 1 ? 
            input[comp_offset - 1] : 
            sw[comp_offset - 1 + sw_offset];

          N = sw[comp_offset - BSIZE + sw_offset];
          S = sw[comp_offset + BSIZE + sw_offset];
          W = west_1;
        } 
        // Edge 3
        else if (r == R_SIZE - 1) {
          float east_1 = (BSIZE - 1) - x < 1 ? 
            input[comp_offset + 1] : 
            sw[comp_offset + 1 + sw_offset];
          float west_1 = x < 1 ? 
            input[comp_offset - 1] : 
            sw[comp_offset - 1 + sw_offset];

          N = sw[comp_offset - BSIZE + sw_offset];
          E = east_1;
          W = west_1;
        } 
        // Edge 4
        else if (c == 0) {
          float east_1 = (BSIZE - 1) - x < 1 ? 
            input[comp_offset + 1] : 
            sw[comp_offset + 1 + sw_offset];

          N = sw[comp_offset - BSIZE + sw_offset];
          S = sw[comp_offset + BSIZE + sw_offset];
          E = east_1;
        } 
        // Inside 
        else {
          float east_1 = (BSIZE - 1) - x < 1 ? 
            input[comp_offset + 1] : 
            sw[comp_offset + 1 + sw_offset];
          float west_1 = x < 1 ? 
            input[comp_offset - 1] : 
            sw[comp_offset - 1 + sw_offset];

          N = sw[comp_offset - BSIZE + sw_offset];
          S = sw[comp_offset + BSIZE + sw_offset];
          E = east_1;
          W = west_1;
        } 

        output[comp_offset] = sw[comp_offset + sw_offset] + N + E + S + W;
      }

      x = x < BSIZE - 1 ? x + 1 : 0;
      y = x == 0 ? (y == R_SIZE + INIT_ITER - 1 ? 0 : y + 1) : y;
      if (x == 0 && y == 0) bx += BSIZE;

    } while (bx < C_SIZE);
  } // exit kernel
}
#endif

#if SOBEL 
void basic() {
  int i, j, k;

  for (i = 0; i < R_SIZE * C_SIZE ; i++) {
    
    float result = 0;

    for (j = 0; j < 3; j++) {
      for (k = 0; k < 3; k++) {

        if (i - (j*C_SIZE) - k >= 0) { 
          result += input[i - (j*C_SIZE) - k]; 
          //if (i == ITER) 
          //  printf("auto: j: %d, k: %d adding input[%d] = %f\n", 
          //      j, k, i- (j*C_SIZE) - k, 
          //      input[i- (j*C_SIZE) - k]);
        }
      }
    }

    verify[i] = result;
  }
}

void fpga() {
  int i, j, k;

#pragma acc parallel num_workers(1) num_gangs(1) \
  present(input[0:R_SIZE*C_SIZE], output[0:R_SIZE*C_SIZE])
  {
    #define BSIZE (32)
    #define TARGET (2*BSIZE + 2)
    #define SW_BASE_SIZE (2*BSIZE + 2)
    #define SW_SIZE (SW_BASE_SIZE + 1)

    #define INIT_ITER (0)

    printf("init iters: %f\n", 
        ceil( (float) (SW_BASE_SIZE - TARGET) / (float) BSIZE) );

    float sw[SW_SIZE];
    int ii;
    #pragma unroll
    for (ii = 0; ii < SW_SIZE; ++ii) {
      sw[ii] = 0;
    }

    int x = 0, y = 0, bx = 0;
    do {
      int gx = bx + x;
      int comp_offset_y = y - INIT_ITER;
      int comp_offset = gx + comp_offset_y * R_SIZE;
      int read_offset = bx + (x + SW_BASE_SIZE - TARGET) % BSIZE +
                       (((x + SW_BASE_SIZE - TARGET) / BSIZE) + comp_offset_y)*C_SIZE;  

      int block_offset = BSIZE * comp_offset_y + x;
      int sw_offset = TARGET - comp_offset;

      #pragma unroll
      for (ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + 1];
      }
      sw[SW_BASE_SIZE] = read_offset < 0 ? 0 : input[read_offset];

      if (comp_offset_y >= 0 && comp_offset_y < R_SIZE) {

        //if (comp_offset == ITER) {
        //  for (int i = 0; i < SW_SIZE; i++) {
        //    printf("inital sw[%d] = %f\n", i, sw[i]);
        //}}

        float result = 0;

        for (j = 0; j < 3; j++) {
          for (k = 0; k < 3; k++) {

            if (comp_offset - (j*C_SIZE) - k >= 0) { 

              if (gx % 32 == 0 || gx % 32 == 1) {
                result += input[comp_offset - (j*C_SIZE) - k]; 
                //if (comp_offset == ITER) 
                  //printf("j: %d, k: %d adding input[%d] = %f\n", 
                  //    j, k, comp_offset - (j*C_SIZE) - k, 
                  //    input[comp_offset- (j*C_SIZE) - k]);
              }
              else {
                result += sw[comp_offset - (j*BSIZE) - k + sw_offset]; 
                //if (comp_offset == ITER) 
                //  printf("j: %d, k: %d adding sw[%d] = %f\n", 
                //      j, k,comp_offset - (j*C_SIZE) - k + sw_offset, 
                //      sw[comp_offset - (j*C_SIZE) - k + sw_offset]);
              }
              
            }
          }
        }

        output[comp_offset] = result;
      }

      x = x < BSIZE - 1 ? x + 1: 0; 
      y = x == 0 ? (y == R_SIZE + INIT_ITER - 1 ? 0 : y + 1) : y;
      if (x == 0 && y == 0) bx += BSIZE;
    } while (bx < C_SIZE);
  }
}
#endif

#if CANNY 
void basic() {
  int i, j;

  for (i = 0; i < R_SIZE * C_SIZE ; i++) {

    int r = i / R_SIZE;
    int c = i % R_SIZE;

    float result = 0.0;
    float NN=0.0, SS=0.0, EE=0.0, WW=0.0;
    float NE=0.0, SE=0.0, NW=0.0, SW=0.0;
    

    // Corner 1
    if (r == 0 && c == 0) {
      SS = input[i + C_SIZE];
      EE = input[i + 1];
      
      SE = input[i + C_SIZE + 1];
    }
    // Corner 2
    else if (r == 0 && c == C_SIZE - 1) {
      SS = input[i + C_SIZE];
      WW = input[i - 1]; 

      SW = input[i + C_SIZE - 1];
    }
    // Corner 3
    else if (r == R_SIZE - 1 && c == C_SIZE - 1) {
      NN = input[i - C_SIZE];
      WW = input[i - 1]; 

      NW = input[i - C_SIZE - 1];
    }
    // Corner 4
    else if (r == R_SIZE - 1 && c == 0) {
      NN = input[i - C_SIZE];
      EE = input[i + 1];

      NE = input[i - C_SIZE + 1];
    }
    // Edge 1
    else if (r == 0) {
      SS = input[i + C_SIZE];
      EE = input[i + 1];
      WW = input[i - 1]; 

      SE = input[i + C_SIZE + 1];
      SW = input[i + C_SIZE - 1];
    } 
    // Edge 2
    else if (c == C_SIZE - 1) {
      NN = input[i - C_SIZE];
      SS = input[i + C_SIZE];
      WW = input[i - 1]; 

      NW = input[i - C_SIZE - 1];
      SW = input[i + C_SIZE - 1];
    } 
    // Edge 3
    else if (r == R_SIZE - 1) {
      NN = input[i - C_SIZE];
      EE = input[i + 1];
      WW = input[i - 1]; 

      NE = input[i - C_SIZE + 1];
      NW = input[i - C_SIZE - 1];
    } 
    // Edge 4
    else if (c == 0) {
      NN = input[i - C_SIZE];
      SS = input[i + C_SIZE];
      EE = input[i + 1];

      NE = input[i - C_SIZE + 1];
      SE = input[i + C_SIZE + 1];
    } 
    // Inside 
    else {
      NN = input[i - C_SIZE];
      SS = input[i + C_SIZE];
      EE = input[i + 1];
      WW = input[i - 1]; 

      NE = input[i - C_SIZE + 1];
      SE = input[i + C_SIZE + 1];
      NW = input[i - C_SIZE - 1];
      SW = input[i + C_SIZE - 1];
    } 

    verify[i] = input[i] + NN + EE + SS + WW + NE + NW + SE + SW;
  }
}

void fpga() {
  int i, j;

#pragma acc parallel num_workers(1) num_gangs(1) \
  present(input[0:R_SIZE*C_SIZE], output[0:R_SIZE*C_SIZE])
  {

    #define BSIZE (32)
    #define TARGET (BSIZE + 1)
    #define SW_BASE_SIZE (2*BSIZE + 2)
    #define SW_SIZE (SW_BASE_SIZE + 1)

    #define INIT_ITER 2

    printf("init iters: %f\n", 
        ceil( (float) (SW_BASE_SIZE - TARGET) / (float) BSIZE) );

    float sw[SW_SIZE];
    int ii;
    #pragma unroll
    for (ii = 0; ii < SW_SIZE; ++ii) {
      sw[ii] = 0;
    }

    //for (i = 0; i < R_SIZE * C_SIZE ; i++) {
    int x = 0, y = 0, bx = 0;
    do {
      int gx = bx + x;
      int comp_offset_y = y - INIT_ITER;
      int comp_offset = gx + comp_offset_y * R_SIZE;
      int read_offset = 
          bx + (x + BSIZE + 1) % BSIZE + 
          ( ( (x + BSIZE + 1) / BSIZE ) + comp_offset_y) * C_SIZE; 

      int block_offset = BSIZE * (y - INIT_ITER) + x;
      int sw_offset = TARGET - comp_offset;

      #pragma unroll
      for (ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + 1];
      }
      // shift in zeros until we get to needed elements
      sw[SW_BASE_SIZE] = read_offset < 0 ? 0 : input[read_offset];

      if (comp_offset_y >= 0 && comp_offset_y < R_SIZE) {
        int r = comp_offset / R_SIZE;
        int c = comp_offset % R_SIZE;
    
        float result = 0.0;
        float NN=0.0, SS=0.0, EE=0.0, WW=0.0;
        float NE=0.0, SE=0.0, NW=0.0, SW=0.0;

        //if (r == 0 && c == 32) print_window(sw, "second column", SW_SIZE);

        // Corner 1
        if (r == 0 && c == 0) {
          SS = sw[comp_offset + BSIZE + sw_offset];
          EE = sw[comp_offset + 1 + sw_offset];

          SE = sw[comp_offset + BSIZE + 1 + sw_offset];

        }
        // Corner 2
        else if (r == 0 && c == C_SIZE - 1) {
          SS = sw[comp_offset + BSIZE + sw_offset];
          WW = sw[comp_offset - 1 + sw_offset]; 

          SW = sw[comp_offset + BSIZE - 1 + sw_offset];
        }
        // Corner 3
        else if (r == R_SIZE - 1 && c == C_SIZE - 1) {
          NN = sw[comp_offset - BSIZE + sw_offset];
          WW = sw[comp_offset - 1 + sw_offset]; 

          NW = sw[comp_offset - BSIZE - 1 + sw_offset];
        }
        // Corner 4
        else if (r == R_SIZE - 1 && c == 0) {
          NN = sw[comp_offset - BSIZE + sw_offset];
          EE = sw[comp_offset + 1 + sw_offset];
          NE = sw[comp_offset - BSIZE + 1 + sw_offset];
        }
        // Edge 1
        else if (r == 0) {
          float ee_1 = (BSIZE - 1) - x < 1 ? 
            input[comp_offset + 1] : 
            sw[comp_offset + 1 + sw_offset];
          float se_1 = (BSIZE - 1) - x < 1 ? 
            input[comp_offset + C_SIZE + 1] : 
            sw[comp_offset + BSIZE + 1 + sw_offset];

          float ww_1 = x < 1 ? 
            input[comp_offset - 1] : 
            sw[comp_offset - 1 + sw_offset];
          float sw_1 = x < 1 ? 
            input[comp_offset + C_SIZE - 1] : 
            sw[comp_offset + BSIZE - 1 + sw_offset];

          SS = sw[comp_offset + BSIZE + sw_offset];
          EE = ee_1; 
          WW = ww_1;
          SE = se_1;
          SW = sw_1;
        } 
        // Edge 2
        else if (c == C_SIZE - 1) {
          NN = sw[comp_offset - BSIZE + sw_offset];
          SS = sw[comp_offset + BSIZE + sw_offset];
          WW = sw[comp_offset - 1 + sw_offset]; 

          NW = sw[comp_offset - BSIZE - 1 + sw_offset];
          SW = sw[comp_offset + BSIZE - 1 + sw_offset];
        } 
        // Edge 3
        else if (r == R_SIZE - 1) {
          float ee_1 = (BSIZE - 1) - x < 1 ? 
            input[comp_offset + 1] : 
            sw[comp_offset + 1 + sw_offset];
          float ne_1 = (BSIZE - 1) - x < 1 ? 
            input[comp_offset - C_SIZE + 1] : 
            sw[comp_offset - BSIZE + 1 + sw_offset];

          float ww_1 = x < 1 ? 
            input[comp_offset - 1] : 
            sw[comp_offset - 1 + sw_offset];
          float nw_1 = x < 1 ? 
            input[comp_offset - C_SIZE - 1] : 
            sw[comp_offset - BSIZE - 1 + sw_offset];

          NN = sw[comp_offset - BSIZE + sw_offset];
          EE = ee_1;
          WW = ww_1;

          NE = ne_1; 
          NW = nw_1; 
        } 
        // Edge 4
        else if (c == 0) {
          NN = sw[comp_offset - BSIZE + sw_offset];
          SS = sw[comp_offset + BSIZE + sw_offset];
          EE = sw[comp_offset + 1 + sw_offset];

          NE = sw[comp_offset - BSIZE + 1 + sw_offset];
          SE = sw[comp_offset + BSIZE + 1 + sw_offset];
        } 
        // Inside 
        else {
          float ee_1, ne_1, se_1;
          float ww_1, nw_1, sw_1;

          if ( (BSIZE - 1) - x < 1) {
            ee_1 = input[comp_offset + 1]; 
            ne_1 = input[comp_offset - C_SIZE + 1]; 
            se_1 = input[comp_offset + C_SIZE + 1]; 
          }
          else {
            ee_1 = sw[comp_offset + 1 + sw_offset];
            ne_1 = sw[comp_offset - BSIZE + 1 + sw_offset];
            se_1 = sw[comp_offset + BSIZE + 1 + sw_offset];
          }

          if ( x < 1 ) {
            ww_1 = input[comp_offset - 1]; 
            nw_1 = input[comp_offset - BSIZE - 1]; 
            sw_1 = input[comp_offset + BSIZE - 1]; 
          }
          else {
            ww_1 = sw[comp_offset - 1 + sw_offset];
            nw_1 = sw[comp_offset - BSIZE - 1 + sw_offset];
            sw_1 = sw[comp_offset + BSIZE - 1 + sw_offset];
          }

          NN = sw[comp_offset - BSIZE + sw_offset];
          SS = sw[comp_offset + BSIZE + sw_offset];

          EE = ee_1;
          NE = ne_1;
          SE = se_1;

          WW = ww_1;
          NW = nw_1;
          SW = sw_1;
        } 

        output[comp_offset] = 
            sw[comp_offset + sw_offset] + NN + EE + SS + WW + NE + NW + SE + SW;
      }

      x = x < BSIZE - 1 ? x + 1: 0;
      y = x == 0 ? (y == R_SIZE + INIT_ITER - 1? 0 : y + 1) : y;
      if (x == 0 && y == 0) bx += BSIZE;
    } while (bx < C_SIZE);
  }
}
#endif

#if FD3D

#define coord(x, y, z) ((z) * C_SIZE * R_SIZE + (y) * Z_SIZE + (x))
#define RADIUS 3
void basic() {

  int i;

  for (i = 0; i < R_SIZE*C_SIZE*Z_SIZE; i++) {
    int index = i;
    int x = index % C_SIZE;
    index /= C_SIZE;
    int y = index % R_SIZE;
    index /= C_SIZE; 
    int z = index;

    if (x >= RADIUS && x < C_SIZE - RADIUS &&
        y >= RADIUS && y < R_SIZE - RADIUS &&
        z >= RADIUS && z < Z_SIZE - RADIUS) {

      float result = input[i];   // T
      for (int r = 1; r <= RADIUS; r++) {
        result += input[coord(x - r, y, z)];  // -X
        result += input[coord(x + r, y, z)];  // +X
        result += input[coord(x, y - r, z)];  // -Y 
        result += input[coord(x, y + r, z)];  // +Y
        result += input[coord(x, y, z - r)];  // -Z
        result += input[coord(x, y, z + r)];  // +Z
      }
      verify[i] = result;
    }
    else {
      verify[i] = input[i];
    }
  }
}

void fpga() {

#pragma acc parallel num_workers(1) num_gangs(1) \
  present(input[0:R_SIZE*C_SIZE], output[0:R_SIZE*C_SIZE])
  {
    #define BSIZE_X  // DIMX
    #define BSIZE_Y  // DIMY

    #define TARGET (RADIUS * BSIZE_X * BSIZE_Y)
    #define SW_BASE_SIZE (2 * RADIUS * BSIZE_X * BSIZE_Y)
    #define SW_SIZE (SW_BASE_SIZE + 1)

    float sw[SW_SIZE];
    for (int ii = 0; ii < SW_SIZE; ii++) sw[ii] = 0;

    
    int x = 0, y = 0, xtile = 0, ytile = 0, ztile = 0;
    for (int i = 0; i < R_SIZE*C_SIZE*Z_SIZE; i++) {
      int index = i;
      int x_coord = index % C_SIZE;
      index /= C_SIZE;
      int y_coord = index % R_SIZE;
      index /= C_SIZE; 
      int z_coord = index;

      int read_offset = ztile*C_SIZE*R_SIZE + (y + ytile) * C_SIZE + (x + xtile);
      int xoutput = x + xtile, youtput = y + ytile, zoutput = ztile - RADIUS;
      int comp_offset = zoutput * C_SIZE * R_SIZE + youtput * C_SIZE + xoutput;
      // this is as far as I've made it on this one

      #pragma unroll
      for (int ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + 1];
      }
      // shift in zeros until we get to needed elements
      sw[SW_BASE_SIZE] = read_offset < 0 ? 0 : input[read_offset];

      if (x_coord >= RADIUS && x_coord < C_SIZE - RADIUS &&
          y_coord >= RADIUS && y_coord < R_SIZE - RADIUS &&
          z_coord >= RADIUS && z_coord < Z_SIZE - RADIUS) {

        float result = input[i];   // T
        for (int r = 1; r <= RADIUS; r++) {
          result += input[i - r];  // -X
          result += input[i + r];  // +X
          result += input[i - r * C_SIZE];  // -Y 
          result += input[i + r * C_SIZE];  // +Y
          result += input[i - r * C_SIZE*R_SIZE];  // -Z
          result += input[i + r * C_SIZE*R_SIZE];  // +Z
        }
        output[i] = result;
      }
      else {
        output[i] = input[i];
      }
    }
  } // exit acc 
}
#endif
