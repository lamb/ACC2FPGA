#define AOCL_ALIGNMENT 64 

#define LIMIT -999
//#define TRACE

#define DEBUG

#ifndef VERIFICATION
#define VERIFICATION 1
#endif

#ifndef HOST_MEM_ALIGNMENT
#define HOST_MEM_ALIGNMENT 1
#endif

#ifndef _MAX_ROWS_
#define _MAX_ROWS_      2049
#ifdef _OPENARC_
#pragma openarc #define _MAX_ROWS_ 2049
#endif
#endif

#ifndef _BSIZE_
#define _BSIZE_ 16
#ifdef _OPENARC_
#pragma openarc #define _BSIZE_ 16
#endif
#endif
