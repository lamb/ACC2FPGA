#include <stdio.h>
#include "nw.h"

#if DIRECTIVE

// Version from openarc/nw_openacce directory
extern int max_rows, max_cols, penalty;

int maximum(int a, int b, int c);

void mainComp(int input_itemsets[_MAX_ROWS_*_MAX_ROWS_], int reference[_MAX_ROWS_*_MAX_ROWS_]) 
{

  #pragma acc parallel loop num_gangs(1) num_workers(1) collapse(2) \
    present(input_itemsets, reference) independent
  #pragma openarc transform window(input_itemsets[0:_MAX_ROWS_*_MAX_ROWS_], \
           input_itemsets[0:_MAX_ROWS_*_MAX_ROWS_])
  for (int j = 0; j < max_cols; ++j) {
    for (int i = 0; i < max_cols; ++i) {
      int index = j * max_cols + i;

      if (i > 0 && j > 0 && i < max_cols-1 && j < max_cols-1) {
        input_itemsets[j*max_cols + i]= maximum(
            input_itemsets[(j*max_cols + i) - 1 -_MAX_ROWS_]+ reference[index],
            input_itemsets[(j*max_cols + i) - 1] - penalty,
            input_itemsets[(j*max_cols + i) - _MAX_ROWS_]  - penalty);
      }

    }
  }

}
#endif
