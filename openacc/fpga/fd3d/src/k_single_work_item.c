#include "stdio.h"
#include "fd3d_config.h"

#if SINGLE_WORK_ITEM 
void Finite(float *input, float *output, float *coeff, int sizex, int sizey, int sizez) {
  #pragma acc parallel num_gangs(1) num_workers(1) \
      present(input[0:sizex*sizey*sizez], \
              output[0:sizex*sizey*sizez], \
              coeff[0:RADIUS + 1])
  {
    #pragma acc loop collapse(3)
    for (int z = 0; z < sizez; z++) {
      for (int y = 0; y < sizey; y++) {
        for (int x = 0; x < sizex; x++) {

          if (x >= RADIUS && x < sizex - RADIUS &&
              y >= RADIUS && y < sizey - RADIUS &&
              z >= RADIUS && z < sizez - RADIUS) {

            float result = coeff[0] * input[coord(x, y, z)];
            #pragma unroll
            for (int r = 1 ; r <= RADIUS; r++) {
              result += coeff[r] * (
                input[coord(x - r, y, z)] +
                input[coord(x + r, y, z)] +
                input[coord(x, y - r, z)] +
                input[coord(x, y + r, z)] +
                input[coord(x, y, z - r)] +
                input[coord(x, y, z + r)] );
            }
            output[coord(x, y, z)] = result;
          } 
          else 
            output[coord(x, y, z)] = input[coord(x, y, z)];
        } // x
      } // y
    } // z
  } // acc
}
#endif
