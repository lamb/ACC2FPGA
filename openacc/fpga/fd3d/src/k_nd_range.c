#include "stdio.h"
#include "fd3d_config.h"

#if ND_RANGE 

//#define SIMD 1
//#pragma openarc #define SIMD 1

void Finite(float *input, float *output, float *coeff, int sizex, int sizey, int sizez) {

  //#pragma openarc opencl num_simd_work_items(SIMD)
  #pragma acc parallel \
      present(input[0:sizex*sizey*sizez], \
              output[0:sizex*sizey*sizez], \
              coeff[0:RADIUS + 1])
  {
    #pragma acc loop gang
    for (int z = 0; z < sizez; z++) {
      #pragma acc loop worker collapse(2)
      for (int y = 0; y < sizey; y++) {
        for (int x = 0; x < sizex; x++) {

          if (x >= RADIUS && x < sizex - RADIUS &&
              y >= RADIUS && y < sizey - RADIUS &&
              z >= RADIUS && z < sizez - RADIUS) {

            float result = coeff[0] * input[coord(x, y, z)];
  
            //#pragma unroll
            for (int r = 1 ; r <= RADIUS; r++) {
              result += coeff[r] * (
                input[coord(x - r, y, z)] +
                input[coord(x + r, y, z)] +
                input[coord(x, y - r, z)] +
                input[coord(x, y + r, z)] +
                input[coord(x, y, z - r)] +
                input[coord(x, y, z + r)] );
            }
            output[coord(x, y, z)] = result;
          } 
          else 
            output[coord(x, y, z)] = input[coord(x, y, z)];
        } // x
      } // y
    } // z
  } // acc
}
#endif
