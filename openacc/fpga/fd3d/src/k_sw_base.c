#include "stdio.h"
#include "fd3d_config.h"

#if SW_BASE 
void Finite(float *input, float *output, float *coeff, int sizex, int sizey, int sizez) {
  #pragma acc parallel num_gangs(1) num_workers(1) \
      present(input[0:sizex*sizey*sizez], \
              output[0:sizex*sizey*sizez], \
              coeff[0:RADIUS + 1])
  {
      
    #define TARGET (RADIUS * DEFAULT_SIZEX * DEFAULT_SIZEY)
    #define SW_BASE_SIZE (2 * RADIUS * DEFAULT_SIZEX * DEFAULT_SIZEY)
    #define SW_SIZE (SW_BASE_SIZE + 1)

    float sw[SW_SIZE] = {0};
    //#pragma unroll
    //for (int i = 0; i < SW_SIZE; ++i) sw[i] = 0;

    int x = 0, y = 0, z = -4; // starting z = -4 gives us right initialization
    for (int iter = TARGET - SW_BASE_SIZE; iter < sizex*sizey*sizez; iter++) {

      #pragma unroll
      for (int i = 0; i < SW_BASE_SIZE; ++i) {
        sw[i] = sw[i + 1];
      }

      int read_offset = iter + SW_BASE_SIZE - TARGET;
      if (read_offset >= 0 && read_offset < sizex*sizey*sizez) 
        sw[SW_BASE_SIZE] = input[read_offset];
      else
        sw[SW_BASE_SIZE] = 0;

      // we've omitted conditionals on writing because we already have this conditional
      if (x >= RADIUS && x < sizex - RADIUS &&
          y >= RADIUS && y < sizey - RADIUS &&
          z >= RADIUS && z < sizez - RADIUS) {

        float result = coeff[0] * sw[TARGET + coord(0, 0, 0)];
        #pragma unroll
        for (int r = 1 ; r <= RADIUS; r++) {
          result += coeff[r] * (
            sw[TARGET + coord(0 - r, 0, 0)] +
            sw[TARGET + coord(0 + r, 0, 0)] +
            sw[TARGET + coord(0, 0 - r, 0)] +
            sw[TARGET + coord(0, 0 + r, 0)] +
            sw[TARGET + coord(0, 0, 0 - r)] +
            sw[TARGET + coord(0, 0, 0 + r)] );
        }
        output[coord(x, y, z)] = result;
      } 
      else 
        output[coord(x, y, z)] = sw[TARGET + coord(0, 0, 0)];

      // collapsed variable incrementation
      x++;
      if (x == sizex) { y++; x = 0; }
      if (y == sizey) { z++; y = 0; }

    } // iter 
  } // acc
}
#endif
