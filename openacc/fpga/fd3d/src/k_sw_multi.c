#include "stdio.h"
#include "fd3d_config.h"

#if SW_MULTI
void Finite(float *input, float *output, float *coeff, int sizex, int sizey, int sizez) {
  #pragma acc parallel num_gangs(1) num_workers(1) \
      present(input[0:sizex*sizey*sizez], \
              output[0:sizex*sizey*sizez], \
              coeff[0:RADIUS + 1])
  {
      
    #define SSIZE (8)
    #define TARGET (RADIUS * DEFAULT_SIZEX * DEFAULT_SIZEY)
    #define SW_BASE_SIZE (2 * RADIUS * DEFAULT_SIZEX * DEFAULT_SIZEY)
    #define SW_SIZE (SW_BASE_SIZE + SSIZE)

    float sw[SW_SIZE];
    #pragma unroll
    for (int i = 0; i < SW_SIZE; ++i) sw[i] = 0;

    int x = 0, y = 0, z = -4; // starting z = -4 gives us right initialization
    for (int iter = TARGET - SW_BASE_SIZE; iter < sizex*sizey*sizez; iter += SSIZE) {

      #pragma unroll
      for (int i = 0; i < SW_BASE_SIZE; ++i) {
        sw[i] = sw[i + SSIZE];
      }

      int read_offset = iter + SW_BASE_SIZE - TARGET;
      #pragma unroll
      for (int i = 0; i < SSIZE; ++i) {
        if (read_offset + i >= 0 && read_offset + i < sizex*sizey*sizez) 
          sw[SW_BASE_SIZE + i] = input[read_offset + i];
        else
          sw[SW_BASE_SIZE + i] = 0;
      }

      float value[SSIZE];

      #pragma unroll
      for (int ss = 0; ss < SSIZE; ++ss) {

        // we've omitted conditionals on writing because we already have this conditional
        if (x+ss >= RADIUS && x+ss < sizex - RADIUS &&
            y >= RADIUS && y < sizey - RADIUS &&
            z >= RADIUS && z < sizez - RADIUS) {

          float result = coeff[0] * sw[TARGET+ss + coord(0, 0, 0)];

          #pragma unroll
          for (int r = 1 ; r <= RADIUS; r++) {
            result += coeff[r] * (
              sw[TARGET+ss + coord(0 - r, 0, 0)] +
              sw[TARGET+ss + coord(0 + r, 0, 0)] +
              sw[TARGET+ss + coord(0, 0 - r, 0)] +
              sw[TARGET+ss + coord(0, 0 + r, 0)] +
              sw[TARGET+ss + coord(0, 0, 0 - r)] +
              sw[TARGET+ss + coord(0, 0, 0 + r)] );
          }
          value[ss] = result;
        } 
        else 
          value[ss] = sw[TARGET+ss + coord(0, 0, 0)];
      } // SSIZE loop

      #pragma unroll
      for (int i = 0; i < SSIZE; ++i) {
        if (coord(x,y,z) + i >= 0) output[coord(x,y,z) + i] = value[i];
      }

      // collapsed variable incrementation
      x += SSIZE;
      if (x == sizex) { y++; x = 0; }
      if (y == sizey) { z++; y = 0; }

    } // iter 
  } // acc
}
#endif
