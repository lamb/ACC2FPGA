#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <sys/time.h>
#include <math.h>

extern const float amb_temp;

#define STR_SIZE	256

#ifndef VERIFICATION
#define VERIFICATION 1
#endif

// Multi threaded versions 
#define ND    0

// Single threaded versions
#define SWI         0
#define SWI_WINDOW  0
#define SWI_WINDOW_HOIST 1

// Hotspot Macros
#define AMB_TEMP (80.0F)
/* maximum power density possible (say 300W for a 10mm x 10mm chip)	*/
#define MAX_PD	(3.0e6)
/* required precision in degrees	*/
#define PRECISION	0.001F
#define SPEC_HEAT_SI 1.75e6
#define K_SI 100
/* capacitance fitting factor	*/
#define FACTOR_CHIP	0.5F
//#define VERBOSE
#define DEBUG

//#ifndef ROW_SIZE
//#define ROW_SIZE 512 
//#endif

//#ifndef COL_SIZE
//#define COL_SIZE 512 
//#endif

//#define TEMP_SIZE   (ROW_SIZE*COL_SIZE)
//#define POWER_SIZE  (ROW_SIZE*COL_SIZE)
//#define RESULT_SIZE (ROW_SIZE*COL_SIZE)

//#ifdef _OPENARC_
//
//#if ROW_SIZE == 64
//#pragma openarc #define ROW_SIZE 64
//#elif ROW_SIZE == 512
//#pragma openarc #define ROW_SIZE 512
//#elif ROW_SIZE == 1024
//#pragma openarc #define ROW_SIZE 1024
//#elif ROW_SIZE == 4096
//#pragma openarc #define ROW_SIZE 4096
//#endif
//
//#if COL_SIZE == 64
//#pragma openarc #define COL_SIZE 64
//#elif COL_SIZE == 512
//#pragma openarc #define COL_SIZE 512
//#elif COL_SIZE == 1024
//#pragma openarc #define COL_SIZE 1024
//#elif COL_SIZE == 4096
//#pragma openarc #define COL_SIZE 4096
//#endif
//
//#pragma openarc #define TEMP_SIZE   (ROW_SIZE*COL_SIZE)
//#pragma openarc #define POWER_SIZE  (ROW_SIZE*COL_SIZE)
//#pragma openarc #define RESULT_SIZE (ROW_SIZE*COL_SIZE)
//
//#endif
