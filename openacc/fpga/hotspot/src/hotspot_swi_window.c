#include "hotspot.h"

#if SWI_WINDOW 
int single_iteration(float *power, float *temp, float *result, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{
  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

  #pragma openarc transform \
    window(temp[0:ROW_SIZE*COL_SIZE], result[0:ROW_SIZE*COL_SIZE])
  #pragma acc parallel loop num_gangs(1) num_workers(1) collapse(2) \
      present(temp, result, power) 
  #pragma unroll 16
  for (int r = 0; r < ROW_SIZE ; r++) {
    for (int c = 0; c < COL_SIZE; c++) {

      float delta;

      /*	Corner 1	*/
      if ( (r == 0) && (c == 0) ) {
        delta = (step_div_Cap) * (power[0] +
            (temp[r*COL_SIZE + c + 1] - 
             temp[r*COL_SIZE + c]) * Rx_1 +
            (temp[(r*COL_SIZE + c) + COL_SIZE] - 
             temp[r*COL_SIZE + c]) * Ry_1 +
            (amb_temp - temp[r*COL_SIZE + c]) * Rz_1);
      }	/*	Corner 2	*/
      else if ((r == 0) && (c == COL_SIZE - 1)) {
        delta = (step_div_Cap) * (power[c] +
            (temp[r*COL_SIZE + c - 1] - 
             temp[r*COL_SIZE + c]) * Rx_1 +
            (temp[r*COL_SIZE + c + COL_SIZE] - 
             temp[r*COL_SIZE + c]) * Ry_1 +
            (amb_temp - temp[r*COL_SIZE + c]) * Rz_1);
      }	/*	Corner 3	*/
      else if ((r == grid_rows - 1) && (c == COL_SIZE - 1)) {
        delta = (step_div_Cap) * (power[r*grid_cols + c] + 
            (temp[r*COL_SIZE + c - 1] - 
             temp[r*COL_SIZE + c]) * Rx_1 + 
            (temp[(r - 1)*COL_SIZE + c] - 
             temp[r*COL_SIZE + c]) * Ry_1 + 
            (amb_temp - temp[r*COL_SIZE + c]) * Rz_1);					
      }	/*	Corner 4	*/
      else if ((r == grid_rows - 1) && (c == 0)) {
        delta = (step_div_Cap) * (power[r*grid_cols] + 
            (temp[r*COL_SIZE + c + 1] - 
             temp[r*COL_SIZE + c]) * Rx_1 + 
            (temp[(r - 1)*COL_SIZE + c] - 
             temp[r*COL_SIZE + c]) * Ry_1 + 
            (amb_temp - temp[r*COL_SIZE + c]) * Rz_1);
      }	/*	Edge 1	*/
      else if (r == 0) {
        delta = (step_div_Cap) * (power[c] + 
            (temp[r*COL_SIZE + c + 1] + 
             temp[r*COL_SIZE + c - 1] - 
             2.0F*temp[r*COL_SIZE + c]) * Rx_1 + 
            (temp[r*COL_SIZE + c + COL_SIZE] - 
             temp[r*COL_SIZE + c]) * Ry_1 + 
            (amb_temp - temp[r*COL_SIZE + c]) * Rz_1);
      }	/*	Edge 2	*/
      else if (c == COL_SIZE - 1) {
        delta = (step_div_Cap) * (power[r*grid_cols + c] + 
            (temp[(r + 1)*COL_SIZE + c] + 
             temp[(r - 1)*COL_SIZE + c] - 
             2.0F*temp[r*COL_SIZE + c]) * Ry_1 + 
            (temp[r*COL_SIZE + c - 1] - 
             temp[r*COL_SIZE + c]) * Rx_1 + 
            (amb_temp - temp[r*COL_SIZE + c]) * Rz_1);
      }	/*	Edge 3	*/
      else if (r == grid_rows - 1) {
        delta = (step_div_Cap) * (power[r*grid_cols + c] + 
            (temp[r*COL_SIZE + c + 1] + 
             temp[r*COL_SIZE + c - 1] - 
             2.0F*temp[r*COL_SIZE + c]) * Rx_1 + 
            (temp[(r - 1)*COL_SIZE + c] - 
             temp[r*COL_SIZE + c]) * Ry_1 + 
            (amb_temp - temp[r*COL_SIZE + c]) * Rz_1);
      }	/*	Edge 4	*/
      else if (c == 0) {
        delta = (step_div_Cap) * (power[r*grid_cols] + 
            (temp[(r + 1)*COL_SIZE + c] + 
             temp[(r - 1)*COL_SIZE + c] - 
             2.0F*temp[r*COL_SIZE + c]) * Ry_1 + 
            (temp[r*COL_SIZE + c + 1] - 
             temp[r*COL_SIZE + c]) * Rx_1 + 
            (amb_temp - temp[r*COL_SIZE + c]) * Rz_1);
      }	/*	Inside the chip	*/
      else {
        delta = (step_div_Cap) * (power[r*grid_cols + c] + 
            (temp[(r + 1)*COL_SIZE + c] + 
             temp[(r - 1)*COL_SIZE + c] - 
             2.0F*temp[r*COL_SIZE + c]) * Ry_1 + 
            (temp[r*COL_SIZE + c + 1] + 
             temp[r*COL_SIZE + c - 1] - 
             2.0F*temp[r*COL_SIZE + c]) * Rx_1 + 
            (amb_temp - temp[r*COL_SIZE + c]) * Rz_1);
      }

      /*	Update Temperatures	*/
      result[r*COL_SIZE + c] = temp[r*COL_SIZE + c] + delta;
    }
  }

}
#endif
