#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <sys/time.h>
#include <math.h>
#include <time.h>

#include "hotspot.h"

/* chip parameters	*/
const float t_chip = 0.0005F;
const float chip_height = 0.016F;
const float chip_width = 0.016F;
/* ambient temperature, assuming no package at all	*/
const float amb_temp = 80.0F;

int num_omp_threads;

int single_iteration(float *power, float *temp, float *result, int col, int row,
    float Cap, float Rx, float Ry, float Rz, float step);

double getCurrentTimestamp() {
  struct timespec a;
  clock_gettime(CLOCK_MONOTONIC, &a);
  return ( (double) a.tv_nsec * 1.0e-9) + (double) a.tv_sec;
}

/* Transient solver driver routine: simply converts the heat 
 * transfer differential equations to difference equations 
 * and solves the difference equations by iterating
 */
void compute_tran_temp(float *result, int num_iterations, float *temp, float *power, int row, int col) 
{
  double start_time, end_time;

  float grid_height = chip_height / row;
  float grid_width = chip_width / col;

  float Cap = FACTOR_CHIP * SPEC_HEAT_SI * t_chip * grid_width * grid_height;
  float Rx = grid_width / (2.0F * K_SI * t_chip * grid_height);
  float Ry = grid_height / (2.0F * K_SI * t_chip * grid_width);
  float Rz = t_chip / (K_SI * grid_height * grid_width);

  float max_slope = MAX_PD / (FACTOR_CHIP * t_chip * SPEC_HEAT_SI);
  float step = PRECISION / max_slope;

#ifdef VERBOSE
  fprintf(stdout, "total iterations: %d s\tstep size: %g s\n", num_iterations, step);
  fprintf(stdout, "Rx: %g\tRy: %g\tRz: %g\tCap: %g\n", Rx, Ry, Rz, Cap);
#endif

  #pragma acc data copy(temp[0:row*col], result[0:row*col]) \
                   copyin(power[0:row*col])
  {
    start_time = getCurrentTimestamp();

    #if SW_BASE_CHANNEL || SW_MULTI_CHANNEL
    for (int i = 0; i < num_iterations / 2; i++) {
    #else
    for (int i = 0; i < num_iterations; i++) {
    #endif
      single_iteration(power, temp, result, col, row, Cap, Rx, Ry, Rz, step);

      // swap pointers for next iteration (requires even number of iterations)
      #if !SW_BASE_CHANNEL && !SW_MULTI_CHANNEL
      float *t = temp;
      temp = result;
      result = t;
      #endif
    }

    end_time = getCurrentTimestamp();
    printf("Accelerator Elapsed Time = %lf sec.\n", end_time - start_time);
  }

}

void compute_tran_temp_CPU(float *result, int num_iterations, float *temp, float *power, int grid_rows, int grid_cols) 
{

#ifdef DEBUG
  double start_time, end_time;
#endif

  float grid_height = chip_height / grid_rows;
  float grid_width = chip_width / grid_cols;

  float Cap = FACTOR_CHIP * SPEC_HEAT_SI * t_chip * grid_width * grid_height;
  float Rx = grid_width / (2.0F * K_SI * t_chip * grid_height);
  float Ry = grid_height / (2.0F * K_SI * t_chip * grid_width);
  float Rz = t_chip / (K_SI * grid_height * grid_width);

  float max_slope = MAX_PD / (FACTOR_CHIP * t_chip * SPEC_HEAT_SI);
  //float step = PRECISION / max_slope / 1000;
  float step = PRECISION / max_slope;

  //////////////////////////////////////////////
  // Added for inlining of single_iteration() //
  //////////////////////////////////////////////
  float delta;

  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

#ifdef VERBOSE
  fprintf(stdout, "total iterations: %d s\tstep size: %g s\n", num_iterations, step);
  fprintf(stdout, "Rx: %g\tRy: %g\tRz: %g\tCap: %g\n", Rx, Ry, Rz, Cap);
#endif

#ifdef DEBUG
  start_time = getCurrentTimestamp();
#endif

  for (int i = 0; i < num_iterations ; i++) 
  {
    for (int rc = 0; rc < grid_rows*grid_cols; rc++) {
      int c = rc%grid_cols;
      int r = rc/grid_cols;
      /*	Corner 1	*/
      if ( (r == 0) && (c == 0) ) {
        delta = (step_div_Cap) * (power[0] +
            (temp[1] - temp[0]) * Rx_1 +
            (temp[grid_cols] - temp[0]) * Ry_1 +
            (amb_temp - temp[0]) * Rz_1);
      }	/*	Corner 2	*/
      else if ((r == 0) && (c == grid_cols-1)) {
        delta = (step_div_Cap) * (power[c] +
            (temp[c-1] - temp[c]) * Rx_1 +
            (temp[c+grid_cols] - temp[c]) * Ry_1 +
            (amb_temp - temp[c]) * Rz_1);
      }	/*	Corner 3	*/
      else if ((r == grid_rows-1) && (c == grid_cols-1)) {
        delta = (step_div_Cap) * (power[r*grid_cols+c] + 
            (temp[r*grid_cols+c-1] - temp[r*grid_cols+c]) * Rx_1 + 
            (temp[(r-1)*grid_cols+c] - temp[r*grid_cols+c]) * Ry_1 + 
            (amb_temp - temp[r*grid_cols+c]) * Rz_1);					
      }	/*	Corner 4	*/
      else if ((r == grid_rows-1) && (c == 0)) {
        delta = (step_div_Cap) * (power[r*grid_cols] + 
            (temp[r*grid_cols+1] - temp[r*grid_cols]) * Rx_1 + 
            (temp[(r-1)*grid_cols] - temp[r*grid_cols]) * Ry_1 + 
            (amb_temp - temp[r*grid_cols]) * Rz_1);
      }	/*	Edge 1	*/
      else if (r == 0) {
        delta = (step_div_Cap) * (power[c] + 
            (temp[c+1] + temp[c-1] - 2.0F*temp[c]) * Rx_1 + 
            (temp[grid_cols+c] - temp[c]) * Ry_1 + 
            (amb_temp - temp[c]) * Rz_1);
      }	/*	Edge 2	*/
      else if (c == grid_cols-1) {
        delta = (step_div_Cap) * (power[r*grid_cols+c] + 
            (temp[(r+1)*grid_cols+c] + temp[(r-1)*grid_cols+c] - 2.0F*temp[r*grid_cols+c]) * Ry_1 + 
            (temp[r*grid_cols+c-1] - temp[r*grid_cols+c]) * Rx_1 + 
            (amb_temp - temp[r*grid_cols+c]) * Rz_1);
      }	/*	Edge 3	*/
      else if (r == grid_rows-1) {
        delta = (step_div_Cap) * (power[r*grid_cols+c] + 
            (temp[r*grid_cols+c+1] + temp[r*grid_cols+c-1] - 2.0F*temp[r*grid_cols+c]) * Rx_1 + 
            (temp[(r-1)*grid_cols+c] - temp[r*grid_cols+c]) * Ry_1 + 
            (amb_temp - temp[r*grid_cols+c]) * Rz_1);
      }	/*	Edge 4	*/
      else if (c == 0) {
        delta = (step_div_Cap) * (power[r*grid_cols] + 
            (temp[(r+1)*grid_cols] + temp[(r-1)*grid_cols] - 2.0F*temp[r*grid_cols]) * Ry_1 + 
            (temp[r*grid_cols+1] - temp[r*grid_cols]) * Rx_1 + 
            (amb_temp - temp[r*grid_cols]) * Rz_1);
      }	/*	Inside the chip	*/
      else {
        delta = (step_div_Cap) * (power[r*grid_cols+c] + 
            (temp[(r+1)*grid_cols+c] + temp[(r-1)*grid_cols+c] - 2.0F*temp[r*grid_cols+c]) * Ry_1 + 
            (temp[r*grid_cols+c+1] + temp[r*grid_cols+c-1] - 2.0F*temp[r*grid_cols+c]) * Rx_1 + 
            (amb_temp - temp[r*grid_cols+c]) * Rz_1);
      }


      /*	Update Temperatures	*/
      result[r*grid_cols+c] =temp[r*grid_cols+c]+ delta;
    }

    // pointer swap
    float *swap = temp;
    temp = result;
    result = swap;
  }	
#ifdef DEBUG
  end_time = getCurrentTimestamp();
  printf("Serial Elapsed Time = %lf sec.\n", end_time - start_time);
#endif
}

void fatal(char const* s)
{
  fprintf(stderr, "error: %s\n", s);
  exit(1);
}

void writeoutput(float *vect, int grid_rows, int grid_cols, char *file) {

  int i,j, index=0;
  FILE *fp;
  char str[STR_SIZE];

  if( (fp = fopen(file, "w" )) == 0 ) 
    printf( "The file was not opened\n" );

  for (i=0; i < grid_rows; i++) 
    for (j=0; j < grid_cols; j++)
    {   
      sprintf(str, "%d\t%0.6f\n", index, vect[i*grid_cols+j]);
      fputs(str,fp);
      index++;
    }   

  fclose(fp); 
}

void read_input(float *vect, int grid_rows, int grid_cols, char *file)
{
  int i, index;
  FILE *fp;
  char str[STR_SIZE];
  float val;

  fp = fopen (file, "r");
  if (!fp)
    fatal ("file could not be opened for reading");

  for (i=0; i < grid_rows * grid_cols; i++) {
    fgets(str, STR_SIZE, fp);
    if (feof(fp))
      fatal("not enough lines in file");
    //if ((sscanf(str, "%lf", &val) != 1) )
    if ((sscanf(str, "%f", &val) != 1) )
      fatal("invalid file format");
    vect[i] = val;
  }

  fclose(fp);	
}

void usage(int argc, char **argv)
{
  fprintf(stderr, "Usage: %s <grid_rows> <grid_cols> <sim_time> <no. of threads><temp_file> <power_file>\n", argv[0]);
  fprintf(stderr, "\t<grid_rows>  - number of rows in the grid (positive integer)\n");
  fprintf(stderr, "\t<grid_cols>  - number of columns in the grid (positive integer)\n");
  fprintf(stderr, "\t<sim_time>   - number of iterations\n");
  fprintf(stderr, "\t<no. of threads>   - number of threads\n");
  fprintf(stderr, "\t<temp_file>  - name of the file containing the initial temperature values of each cell\n");
  fprintf(stderr, "\t<power_file> - name of the file containing the dissipated power values of each cell\n");
  fprintf(stderr, "\t<output_file> - name of the output file\n");
  exit(1);
}

int main(int argc, char **argv)
{
  int grid_rows, grid_cols, sim_time, i;
  float *temp, *power, *result;
  char *tfile, *pfile, *ofile;

  double start_time, end_time;

  /* check validity of inputs	*/
  if (argc != 8)
    usage(argc, argv);
  if ((grid_rows = atoi(argv[1])) <= 0 ||
      (grid_cols = atoi(argv[2])) <= 0 ||
      (sim_time = atoi(argv[3])) <= 0 || 
      (num_omp_threads = atoi(argv[4])) <= 0
     )
    usage(argc, argv);

  if (sim_time % 2 != 0) {
    printf("Error: sim_time for kernel must be even\n");
    exit(0); 
  }

  start_time = getCurrentTimestamp();

  /* allocate memory for the temperature and power arrays	*/
  temp = (float *) calloc (grid_rows * grid_cols, sizeof(float));
  power = (float *) calloc (grid_rows * grid_cols, sizeof(float));
  result = (float *) calloc (grid_rows * grid_cols, sizeof(float));
  if(!temp || !power)
    fatal("unable to allocate memory");

  /* read initial temperatures and input power	*/
  tfile = argv[5];
  pfile = argv[6];
  ofile = argv[7];
  read_input(temp, grid_rows, grid_cols, tfile);
  read_input(power, grid_rows, grid_cols, pfile);

  printf("Start computing the transient temperature\n");
  compute_tran_temp(result, sim_time, temp, power, grid_rows, grid_cols);
  printf("Ending simulation\n");

  writeoutput(temp, grid_rows, grid_cols, ofile);

  if(VERIFICATION) {
    float *tempCPU, *powerCPU, *resultCPU;
    float deltaL2Norm = 0;
    float nonAccL2Norm = 0;
    float L2Norm;

    tempCPU = (float *) calloc (grid_rows * grid_cols, sizeof(float));
    powerCPU = (float *) calloc (grid_rows * grid_cols, sizeof(float));
    resultCPU = (float *) calloc (grid_rows * grid_cols, sizeof(float));

    read_input(tempCPU, grid_rows, grid_cols, tfile);
    read_input(powerCPU, grid_rows, grid_cols, pfile);

    printf("Start computing the transient temperature\n");
    compute_tran_temp_CPU(resultCPU, sim_time, tempCPU, powerCPU, grid_rows, grid_cols);
    printf("Ending simulation\n");

    writeoutput(tempCPU, grid_rows, grid_cols, "cpu.out");

    for (i = 0; i < grid_rows * grid_cols; ++i) {
      float d = tempCPU[i] - temp[i];

      deltaL2Norm += d * d;
      nonAccL2Norm += tempCPU[i] * tempCPU[i];
    }

    L2Norm = sqrt(deltaL2Norm / nonAccL2Norm);

    if (L2Norm < 1e-6)
      printf("Verification: SUCCESSFUL\n");
    else
      printf("Verification: FAILED (L2Norm = %g)\n", L2Norm);

    free(tempCPU);
    free(powerCPU);
    free(resultCPU);
  }

  /* cleanup	*/
  free(temp);
  free(power);

  end_time = getCurrentTimestamp();
  printf("Total Execution Time = %lf sec.\n", end_time - start_time);

  return 0;
}
