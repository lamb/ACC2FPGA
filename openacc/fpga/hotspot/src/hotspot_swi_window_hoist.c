#include "hotspot.h"

#if SWI_WINDOW_HOIST 
int single_iteration(float *power, float *temp, float *result, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{
  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

  //#pragma openarc transform \
    window(temp[0:ROW_SIZE*COL_SIZE], result[0:ROW_SIZE*COL_SIZE])
  #pragma acc parallel loop num_gangs(1) num_workers(1) collapse(2) \
      present(temp, result, power) copyin(Rx_1, Ry_1, Rz_1, grid_rows, amb_temp, step_div_Cap)
  //#pragma unroll 8
  for (int r = 0; r < ROW_SIZE ; r++) {
    for (int c = 0; c < COL_SIZE; c++) {

      float delta;
      float N = temp[r*COL_SIZE + c];
      float E = temp[r*COL_SIZE + c];
      float S = temp[r*COL_SIZE + c];
      float W = temp[r*COL_SIZE + c];

      /*	Corner 1	*/
      if ( (r == 0) && (c == 0) ) {
        S = temp[r*COL_SIZE + c + COL_SIZE];
        E = temp[r*COL_SIZE + c + 1];
      }	/*	Corner 2	*/
      else if ((r == 0) && (c == COL_SIZE - 1)) {
        S = temp[r*COL_SIZE + c + COL_SIZE];
        W = temp[r*COL_SIZE + c - 1];
      }	/*	Corner 3	*/
      else if ((r == grid_rows - 1) && (c == COL_SIZE - 1)) {
        N = temp[r*COL_SIZE + c - COL_SIZE];
        W = temp[r*COL_SIZE + c - 1];

      }	/*	Corner 4	*/
      else if ((r == grid_rows - 1) && (c == 0)) {
        N = temp[r*COL_SIZE + c - COL_SIZE];
        E = temp[r*COL_SIZE + c + 1];
      }	/*	Edge 1	*/
      else if (r == 0) {
        E = temp[r*COL_SIZE + c + 1];
        W = temp[r*COL_SIZE + c - 1];
        S = temp[r*COL_SIZE + c + COL_SIZE];
      }	/*	Edge 2	*/
      else if (c == COL_SIZE - 1) {
        N = temp[r*COL_SIZE + c - COL_SIZE];
        S = temp[r*COL_SIZE + c + COL_SIZE];
        W = temp[r*COL_SIZE + c - 1];
      }	/*	Edge 3	*/
      else if (r == grid_rows - 1) {
        N = temp[r*COL_SIZE + c - COL_SIZE];
        E = temp[r*COL_SIZE + c + 1];
        W = temp[r*COL_SIZE + c - 1];
      }	/*	Edge 4	*/
      else if (c == 0) {
        N = temp[r*COL_SIZE + c - COL_SIZE];
        S = temp[r*COL_SIZE + c + COL_SIZE];
        E = temp[r*COL_SIZE + c + 1];
      }	/*	Inside the chip	*/
      else {
        N = temp[r*COL_SIZE + c - COL_SIZE];
        E = temp[r*COL_SIZE + c + 1];
        W = temp[r*COL_SIZE + c - 1];
        S = temp[r*COL_SIZE + c + COL_SIZE];
      }

      float v = power[r*COL_SIZE + c] + 
        (E + W - 2.0f * temp[r*COL_SIZE + c]) * Rx_1 + 
        (N + S - 2.0f * temp[r*COL_SIZE + c]) * Ry_1 + 
        (amb_temp - temp[r*COL_SIZE + c]) * Rz_1;

      delta = step_div_Cap * v;

      /*	Update Temperatures	*/
      result[r*COL_SIZE + c] = temp[r*COL_SIZE + c] + delta;
    }
  }

}
#endif
