#include "hotspot.h"

#if SWI 
int single_iteration(float *power, float *temp, float *result, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{
  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

  #pragma acc parallel num_workers(1) num_gangs(1)  \
      present(temp, result, power) 
  {
    float delta;

    #pragma acc loop collapse(2)
    for (int r = 0; r < grid_rows; r++) {
      for (int c = 0; c < grid_cols; c++) {
        /*	Corner 1	*/
        if ( (r == 0) && (c == 0) ) {
          delta = (step_div_Cap) * (power[0] +
              (temp[1] - 
               temp[0]) * Rx_1 +
              (temp[grid_cols] - 
               temp[0]) * Ry_1 +
              (amb_temp - temp[0]) * Rz_1);
        }	/*	Corner 2	*/
        else if ((r == 0) && (c == grid_cols - 1)) {
          delta = (step_div_Cap) * (power[c] +
              (temp[c - 1] - 
               temp[c]) * Rx_1 +
              (temp[c + grid_cols] - 
               temp[c]) * Ry_1 +
              (amb_temp - temp[c]) * Rz_1);
        }	/*	Corner 3	*/
        else if ((r == grid_rows - 1) && (c == grid_cols - 1)) {
          delta = (step_div_Cap) * (power[r*grid_cols + c] + 
              (temp[r*grid_cols + c - 1] - 
               temp[r*grid_cols + c]) * Rx_1 + 
              (temp[(r - 1)*grid_cols + c] - 
               temp[r*grid_cols + c]) * Ry_1 + 
              (amb_temp - temp[r*grid_cols + c]) * Rz_1);					
        }	/*	Corner 4	*/
        else if ((r == grid_rows - 1) && (c == 0)) {
          delta = (step_div_Cap) * (power[r*grid_cols] + 
              (temp[r*grid_cols + 1] - 
               temp[r*grid_cols]) * Rx_1 + 
              (temp[(r - 1)*grid_cols] - 
               temp[r*grid_cols]) * Ry_1 + 
              (amb_temp - temp[r*grid_cols]) * Rz_1);
        }	/*	Edge 1	*/
        else if (r == 0) {
          delta = (step_div_Cap) * (power[c] + 
              (temp[c + 1] + 
               temp[c - 1] - 
               2.0F*temp[c]) * Rx_1 + 
              (temp[grid_cols + c] - 
               temp[c]) * Ry_1 + 
              (amb_temp - temp[c]) * Rz_1);
        }	/*	Edge 2	*/
        else if (c == grid_cols - 1) {
          delta = (step_div_Cap) * (power[r*grid_cols + c] + 
              (temp[(r + 1)*grid_cols + c] + 
               temp[(r - 1)*grid_cols + c] - 
               2.0F*temp[r*grid_cols + c]) * Ry_1 + 
              (temp[r*grid_cols + c - 1] - 
               temp[r*grid_cols + c]) * Rx_1 + 
              (amb_temp - temp[r*grid_cols + c]) * Rz_1);
        }	/*	Edge 3	*/
        else if (r == grid_rows - 1) {
          delta = (step_div_Cap) * (power[r*grid_cols + c] + 
              (temp[r*grid_cols + c + 1] + 
               temp[r*grid_cols + c - 1] - 
               2.0F*temp[r*grid_cols + c]) * Rx_1 + 
              (temp[(r - 1)*grid_cols + c] - 
               temp[r*grid_cols + c]) * Ry_1 + 
              (amb_temp - temp[r*grid_cols + c]) * Rz_1);
        }	/*	Edge 4	*/
        else if (c == 0) {
          delta = (step_div_Cap) * (power[r*grid_cols] + 
              (temp[(r + 1)*grid_cols] + 
               temp[(r - 1)*grid_cols] - 
               2.0F*temp[r*grid_cols]) * Ry_1 + 
              (temp[r*grid_cols + 1] - 
               temp[r*grid_cols]) * Rx_1 + 
              (amb_temp - temp[r*grid_cols]) * Rz_1);
        }	/*	Inside the chip	*/
        else {
          delta = (step_div_Cap) * (power[r*grid_cols + c] + 
              (temp[(r + 1)*grid_cols + c] + 
               temp[(r - 1)*grid_cols + c] - 
               2.0F*temp[r*grid_cols + c]) * Ry_1 + 
              (temp[r*grid_cols + c + 1] + 
               temp[r*grid_cols + c - 1] - 
               2.0F*temp[r*grid_cols + c]) * Rx_1 + 
              (amb_temp - temp[r*grid_cols + c]) * Rz_1);
        }

        /*	Update Temperatures	*/
        result[r*grid_cols + c] = temp[r*grid_cols + c] + delta;
      }
    }

  } // exit acc parallel	

}
#endif
