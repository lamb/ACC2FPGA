#include <stdio.h>
#include "hotspot.h"

double getCurrentTimestamp();

#if RODIN_V1
int single_iteration(float *power, float *temp_src, float *temp_dst, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{
  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

#pragma acc parallel num_workers(1) num_gangs(1) 
  {
    for (int r = 0; r < grid_rows; ++r) {
      for (int c = 0; c < grid_cols; ++c) {
        int index = c + r * grid_cols;
        int offset_n = (r == grid_rows - 1) ? 0 : grid_cols;
        int offset_s = (r == 0) ? 0 : - grid_cols;
        int offset_e = (c == grid_cols - 1) ? 0 : 1;
        int offset_w = (c == 0) ? 0 : -1;
        float v = power[index] +
          (temp_src[index + offset_n] + temp_src[index + offset_s]
           - 2.0f * temp_src[index]) * Ry_1 +
          (temp_src[index + offset_e] + temp_src[index + offset_w]
           - 2.0f * temp_src[index]) * Rx_1 +
          (AMB_TEMP - temp_src[index]) * Rz_1;
        float delta = step_div_Cap * v;

        temp_dst[index] = temp_src[index] + delta;
      }
    }
  } // exit SINGLE WORK ITEM

}
#endif

#if RODIN_V7 

// Shift size (must be a factor of BSIZE)
#define SSIZE (1)

// Block size
#define BSIZE (32)

// Number of columns in sliding window
#define SW_COLS (BSIZE)

// Min number of cells needed for sliding window
#define SW_BASE_SIZE ((SW_COLS) * 2)

// total size of sliding window
#define SW_SIZE (SW_BASE_SIZE + SSIZE)

int single_iteration(float *power, float *temp_src, float *temp_dst, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{

  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;


#pragma acc parallel num_workers(1) num_gangs(1) 
  {
    for (int cb = 0; cb < grid_cols; cb += BSIZE) {

      float sw[SW_SIZE];

      // initialize
#pragma unroll
      for (int i = 0; i < SW_SIZE; ++i) {
        sw[i] = 0.0f;
      }

#pragma unroll
      for (int c = 0; c < BSIZE; ++c) {
        sw[SSIZE + c] = temp_src[cb + c];
        sw[SSIZE + c + SW_COLS] = temp_src[cb + c];
      }

      int x = 0, y = 0;
      do {
        int gx = cb + x;
        int comp_offset = gx + y * grid_cols;
        int read_offset = comp_offset +
          (y < grid_rows - 1 ? grid_cols : 0);

        // shift
#pragma unroll
        for (int i = 0; i < SW_BASE_SIZE; ++i) {
          sw[i] = sw[i + SSIZE];
        }

        // read new values
#pragma unroll
        for (int i = 0; i < SSIZE; ++i) {
          sw[SW_BASE_SIZE + i] = temp_src[read_offset + i];
        }

        float value[SSIZE];
        int sw_offset = SW_COLS;
        int sw_offset_n = sw_offset + SW_COLS;
        int sw_offset_s = sw_offset - SW_COLS;
        int sw_offset_e = sw_offset + 1;
        int sw_offset_w = sw_offset - 1;

#pragma unroll
        for (int i = 0; i < SSIZE; ++i) {
          float w;
          if (i == 0 && gx == 0) {
            w = sw[sw_offset];
          } else if (i ==0 && x == 0) {
            w = temp_src[comp_offset - 1];
          } else {
            w = sw[sw_offset_w + i];
          }
          float e;
          if (i == SSIZE - 1 && gx + SSIZE == grid_cols) {
            e = sw[sw_offset + i];
          } else if (i == SSIZE - 1 && x + SSIZE == BSIZE) {
            e = temp_src[comp_offset + SSIZE];
          } else {
            e = sw[sw_offset_e + i];
          }

          float v = power[comp_offset + i] +
            (e + w - 2.0f * sw[sw_offset + i]) * Rx_1 +
            (sw[sw_offset_n + i] + sw[sw_offset_s + i] -
             2.0f * sw[sw_offset + i]) * Ry_1 +
            (AMB_TEMP - sw[sw_offset + i]) * Rz_1;
          float delta = step_div_Cap * v;
          value[i] = sw[sw_offset + i] + delta;
        }

#pragma unroll
        for (int i = 0; i < SSIZE; ++i) {
          temp_dst[comp_offset + i] = value[i];
        }

        x = x < BSIZE - SSIZE ? x + SSIZE : 0;
        y = x == 0 ? y + 1 : y;
      } while (y < grid_rows);
    } // exit cb loop
  } // exit SINGLE WORK ITEM

}
#endif

#if RODIN_V9 

#define SSIZE (4)
#define BSIZE (1024)
#define SW_COLS (BSIZE)
#define SW_BASE_SIZE ((SW_COLS) * 2)
#define SW_SIZE (SW_BASE_SIZE + SSIZE)

int single_iteration(float *power, float *temp_src, float *temp_dst, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{
  // RODIN_V9
  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

  double start_time, end_time;
  start_time = getCurrentTimestamp();

  #pragma acc parallel num_workers(1) num_gangs(1) present(power, temp_src, temp_dst)
  {
    float sw[SW_SIZE];
    // initialize
    #pragma unroll
    for (int i = 0; i < SW_SIZE; ++i) {
      sw[i] = 0.0f;
    }

    // read index
    int x = 0, y = -1, bx = 0;
    do {
      int gx = bx + x;
      int comp_offset_y = y - 1;
      int comp_offset = gx + comp_offset_y * grid_cols;
      int read_offset_y = y < 0 ? 0 : y == grid_rows ? grid_rows - 1 : y;
      int read_offset = gx + read_offset_y * grid_cols;

      // shift
      #pragma unroll
      for (int i = 0; i < SW_BASE_SIZE; ++i) {
        sw[i] = sw[i + SSIZE];
      }

      // read new values
      #pragma unroll
      for (int i = 0; i < SSIZE; ++i) {
        sw[SW_BASE_SIZE + i] = temp_src[read_offset + i];
      }

      float value[SSIZE];
      int sw_offset = SW_COLS;
      int sw_offset_n = sw_offset + SW_COLS;
      int sw_offset_s = sw_offset - SW_COLS;
      int sw_offset_e = sw_offset + 1;
      int sw_offset_w = sw_offset - 1;

      if (comp_offset_y >= 0 && comp_offset_y < grid_rows) {

        #pragma unroll
        for (int i = 0; i < SSIZE; ++i) {
          float w;
          if (i == 0 && gx == 0) {
            w = sw[sw_offset];
          } else if (i ==0 && x == 0) {
            w = temp_src[comp_offset - 1];
          } else {
            w = sw[sw_offset_w + i];
          }
          float e;
          if (i == SSIZE - 1 && gx + SSIZE == grid_cols) {
            e = sw[sw_offset + i];
          } else if (i == SSIZE - 1 && x + SSIZE == BSIZE) {
            e = temp_src[comp_offset + SSIZE];
          } else {
            e = sw[sw_offset_e + i];
          }
          float v = power[comp_offset + i] +
            (e + w - 2.0f * sw[sw_offset + i]) * Rx_1 +
            (sw[sw_offset_n + i] + sw[sw_offset_s + i] -
             2.0f * sw[sw_offset + i]) * Ry_1 +
            (AMB_TEMP - sw[sw_offset + i]) * Rz_1;
          float delta = step_div_Cap * v;
          value[i] = sw[sw_offset + i] + delta;
        }

        #pragma unroll
        for (int i = 0; i < SSIZE; ++i) {
          temp_dst[comp_offset + i] = value[i];
        }
      }

      x = x < BSIZE - SSIZE ? x + SSIZE : 0;
      y = x == 0 ? (y == grid_rows ? -1 : y + 1) : y;
      if (x == 0 && y == -1) bx += BSIZE;
    } while (bx < grid_cols);

  } // exit single work item
}
#endif
