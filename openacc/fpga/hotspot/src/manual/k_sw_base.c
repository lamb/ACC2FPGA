#include "hotspot.h"

#if SW_BASE 
int single_iteration(float *power, float *temp, float *result, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{
  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

  #pragma acc parallel num_workers(1) num_gangs(1) 
  {
    // need to use hard-coded ROW_SIZE in non-blocking versions 
    #define TARGET (ROW_SIZE)
    #define SW_BASE_SIZE (2*ROW_SIZE)
    #define SW_SIZE (SW_BASE_SIZE + 1)

    // Step 1
    float sw[SW_SIZE];
    #pragma unroll
    for (int i = 0; i <  SW_SIZE; ++i) { 
      sw[i] = 0;
    }

    float delta;

    int r = -1, c = 0;
    for (int rc = TARGET - SW_BASE_SIZE; rc < grid_rows*grid_cols; rc++) {

      // Step 2
      #pragma unroll
      for (int i = 0; i < SW_BASE_SIZE; ++i) {
        sw[i] = sw[i + 1];
      }

      // Step 3
      int read_offset = rc + SW_BASE_SIZE - TARGET; 
      if (read_offset >=0 && read_offset < grid_rows*grid_cols) 
        sw[SW_BASE_SIZE] = temp[read_offset];
      else
        sw[SW_BASE_SIZE] = 0;

      /*	Corner 1	*/
      if ( (r == 0) && (c == 0) ) {
        delta = (step_div_Cap) * (power[0] +
            (sw[TARGET + 1] - 
             sw[TARGET]) * Rx_1 +
            (sw[TARGET + COL_SIZE] - 
             sw[TARGET]) * Ry_1 +
            (amb_temp - sw[TARGET]) * Rz_1);
      }	/*	Corner 2	*/
      else if ((r == 0) && (c == grid_cols-1)) {
        delta = (step_div_Cap) * (power[c] +
            (sw[TARGET + 0 - 1] - 
             sw[TARGET + 0]) * Rx_1 +
            (sw[TARGET + 0 + COL_SIZE] - 
             sw[TARGET + 0]) * Ry_1 +
            (amb_temp - sw[TARGET + 0]) * Rz_1);
      }	/*	Corner 3	*/
      else if ((r == grid_rows - 1) && (c == grid_cols - 1)) {
        delta = (step_div_Cap) * (power[r*grid_cols+c] + 
            (sw[TARGET + 0*COL_SIZE + 0 - 1] - 
             sw[TARGET + 0*COL_SIZE + 0]) * Rx_1 + 
            (sw[TARGET + (0 - 1)*COL_SIZE + 0] - 
             sw[TARGET + 0*COL_SIZE + 0]) * Ry_1 + 
            (amb_temp - sw[TARGET + 0*COL_SIZE + 0]) * Rz_1);
      }	/*	Corner 4	*/
      else if ((r == grid_rows - 1) && (c == 0)) {
        delta = (step_div_Cap) * (power[r*grid_cols] + 
            (sw[TARGET + 0*COL_SIZE + 1] - 
             sw[TARGET + 0*COL_SIZE]) * Rx_1 + 
            (sw[TARGET + (0 - 1)*COL_SIZE] - 
             sw[TARGET + 0*COL_SIZE]) * Ry_1 + 
            (amb_temp - sw[TARGET + 0*COL_SIZE]) * Rz_1);
      }	/*	Edge 1	*/
      else if (r == 0) {
        delta = (step_div_Cap) * (power[c] + 
            (sw[TARGET + 0 + 1] + 
             sw[TARGET + 0 - 1] - 
             2.0F*sw[TARGET + 0]) * Rx_1 + 
            (sw[TARGET + COL_SIZE + 0] - 
             sw[TARGET + 0]) * Ry_1 + 
            (amb_temp - sw[TARGET + 0]) * Rz_1);
      }	/*	Edge 2	*/
      else if (c == grid_cols - 1) {
        delta = (step_div_Cap) * (power[r*grid_cols+c] + 
            (sw[TARGET + (0 + 1)*COL_SIZE + 0] + 
             sw[TARGET + (0 - 1)*COL_SIZE + 0] - 
             2.0F*sw[TARGET + 0*COL_SIZE + 0]) * Ry_1 + 
            (sw[TARGET + 0*COL_SIZE + 0 - 1] - 
             sw[TARGET + 0*COL_SIZE + 0]) * Rx_1 + 
            (amb_temp - sw[TARGET + 0*COL_SIZE + 0]) * Rz_1);
      }	/*	Edge 3	*/
      else if (r == grid_rows - 1) {
        delta = (step_div_Cap) * (power[r*grid_cols + c] + 
            (sw[TARGET + 0*COL_SIZE + 0 + 1] + 
             sw[TARGET + 0*COL_SIZE + 0 - 1] - 
             2.0F*sw[TARGET + 0*COL_SIZE + 0]) * Rx_1 + 
            (sw[TARGET + (0 - 1)*COL_SIZE + 0] - 
             sw[TARGET + 0*COL_SIZE + 0]) * Ry_1 + 
            (amb_temp - sw[TARGET + 0*COL_SIZE + 0]) * Rz_1);
      }	/*	Edge 4	*/
      else if (c == 0) {
        delta = (step_div_Cap) * (power[r*grid_cols] + 
            (sw[TARGET + (0 + 1)*COL_SIZE] + 
             sw[TARGET + (0 - 1)*COL_SIZE] - 
             2.0F*sw[TARGET + 0*COL_SIZE]) * Ry_1 + 
            (sw[TARGET + 0*COL_SIZE + 1] - 
             sw[TARGET + 0*COL_SIZE]) * Rx_1 + 
            (amb_temp - sw[TARGET + 0*COL_SIZE]) * Rz_1);
      }	/*	Inside the chip	*/
      else {
        delta = (step_div_Cap) * (power[r*grid_cols+c] + 
            (sw[TARGET + (0 + 1)*COL_SIZE + 0] + 
             sw[TARGET + (0 - 1)*COL_SIZE + 0] - 
             2.0F*sw[TARGET + 0*COL_SIZE + 0]) * Ry_1 + 
            (sw[TARGET + 0*COL_SIZE + 0 + 1] + 
             sw[TARGET + 0*COL_SIZE + 0 - 1] - 
             2.0F*sw[TARGET + 0*COL_SIZE + 0]) * Rx_1 + 
            (amb_temp - sw[TARGET + 0*COL_SIZE + 0]) * Rz_1);
      }

      /*	Update Temperatures	*/
      if (rc >= 0)
        result[r*grid_cols + c] = sw[TARGET + 0*COL_SIZE + 0] + delta;

      c++;
      if (c == grid_cols) { r++; c = 0; }
    } // exit Main Loop
  } // exit acc

}
#endif
