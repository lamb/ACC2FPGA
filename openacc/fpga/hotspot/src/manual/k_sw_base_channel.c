#include "hotspot.h"

#if SW_BASE_CHANNEL

// Step 1
#define TARGET (ROW_SIZE)
#define SW_BASE_SIZE (2*ROW_SIZE)
#define SW_SIZE (SW_BASE_SIZE + 1)

int single_iteration(float *power, float *temp, float *result, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{
  float delta;
  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

  // Also want to pipe power, but can't be done with current API
  #pragma acc data pipe(result[0:grid_rows*grid_cols])
  {

  // Enclose Everything
  #pragma acc parallel num_gangs(1) num_workers(1) \
      present(power, temp, result) pipeout(result)
  {
    // intialize window
    float sw[SW_SIZE];
    #pragma unroll
    for (int i = 0; i < SW_SIZE; ++i) 
      sw[i] = 0;

    int c = 0, r = -1;
    for (int rc = TARGET - SW_BASE_SIZE; rc < grid_rows*grid_cols; rc++) {

      // slide the window 
      #pragma unroll
      for (int i = 0; i < SW_BASE_SIZE; ++i) {
        sw[i] = sw[i + 1];
      }

      // shift in new inputs 
      int read_offset = rc + SW_BASE_SIZE - TARGET;
      if (read_offset >= 0 && read_offset < grid_rows*grid_cols) 
        sw[SW_BASE_SIZE] = temp[read_offset];
      else 
        sw[SW_BASE_SIZE] = 0;


        // check if in initalization iterations
        if (rc >= 0) {

          float N = sw[TARGET], 
                S = sw[TARGET],
                E = sw[TARGET],
                W = sw[TARGET];

          // Main Computation
          /*	Corner 1	*/
          if ( (r == 0) && (c == 0) ) {
            S = sw[TARGET + COL_SIZE];
            E = sw[TARGET + 1];
          }	/*	Corner 2	*/
          else if ((r == 0) && (c == grid_cols - 1)) {
            S = sw[TARGET + COL_SIZE];
            W = sw[TARGET - 1];
          }	/*	Corner 3	*/
          else if ((r == grid_rows - 1) && (c == grid_cols - 1)) {
            N = sw[TARGET - COL_SIZE];
            W = sw[TARGET - 1];
          }	/*	Corner 4	*/
          else if ((r == grid_rows - 1) && (c == 0)) {
            N = sw[TARGET - COL_SIZE];
            E = sw[TARGET + 1];
          }	/*	Edge 1	*/
          else if (r == 0) {
            E = sw[TARGET + 1];
            W = sw[TARGET - 1]; 
            S = sw[TARGET + COL_SIZE];
          }	/*	Edge 2	*/
          else if (c == grid_cols - 1) {
            N = sw[TARGET - COL_SIZE];
            S = sw[TARGET + COL_SIZE];
            W = sw[TARGET - 1];
          }	/*	Edge 3	*/
          else if (r == grid_rows - 1) {
            N = sw[TARGET - COL_SIZE];
            E = sw[TARGET + 1];
            W = sw[TARGET - 1];
          }	/*	Edge 4	*/
          else if (c == 0) {
            N = sw[TARGET - COL_SIZE];
            S = sw[TARGET + COL_SIZE];
            E = sw[TARGET + 1];
          }	/*	Inside the chip	*/
          else {
            N = sw[TARGET - COL_SIZE];
            E = sw[TARGET + 1];
            W = sw[TARGET - 1]; 
            S = sw[TARGET + COL_SIZE];
          }

          //float v = power[r*grid_cols + c] + 
          float v = power[rc] + 
            (E + W - 2.0f * sw[TARGET]) * Rx_1 + 
            (N + S - 2.0f * sw[TARGET]) * Ry_1 + 
            (amb_temp - sw[TARGET]) * Rz_1; 

          float delta = step_div_Cap * v;

          /*	Update Temperatures	*/
          result[r*grid_cols + c] = sw[TARGET] + delta;
        }

      c++;
      if (c == grid_cols) { r++; c = 0; }   
    } // exit Main loop 


  } //exit acc


  // Enclose Everything
  #pragma acc parallel num_gangs(1) num_workers(1) \
      present(power, temp, result) pipein(result)
  {
    // intialize window
    float sw[SW_SIZE];
    #pragma unroll
    for (int i = 0; i < SW_SIZE; ++i) 
      sw[i] = 0;

    int c = 0, r = -1;
    for (int rc = TARGET - SW_BASE_SIZE; rc < grid_rows*grid_cols; rc++) {

      // slide the window 
      #pragma unroll
      for (int i = 0; i < SW_BASE_SIZE; ++i) {
        sw[i] = sw[i + 1];
      }

      // shift in new inputs 
      int read_offset = rc + SW_BASE_SIZE - TARGET;
      if (read_offset >= 0 && read_offset < grid_rows*grid_cols) 
        sw[SW_BASE_SIZE] = result[read_offset];
      else 
        sw[SW_BASE_SIZE] = 0;

      // check if in initalization iterations
      if (rc >= 0) {

        float N = sw[TARGET], 
              S = sw[TARGET],
              E = sw[TARGET],
              W = sw[TARGET];

        // Main Computation
        /*	Corner 1	*/
        if ( (r == 0) && (c == 0) ) {
          S = sw[TARGET + COL_SIZE];
          E = sw[TARGET + 1];
        }	/*	Corner 2	*/
        else if ((r == 0) && (c == grid_cols - 1)) {
          S = sw[TARGET + COL_SIZE];
          W = sw[TARGET - 1];
        }	/*	Corner 3	*/
        else if ((r == grid_rows - 1) && (c == grid_cols - 1)) {
          N = sw[TARGET - COL_SIZE];
          W = sw[TARGET - 1];
        }	/*	Corner 4	*/
        else if ((r == grid_rows - 1) && (c == 0)) {
          N = sw[TARGET - COL_SIZE];
          E = sw[TARGET + 1];
        }	/*	Edge 1	*/
        else if (r == 0) {
          E = sw[TARGET + 1];
          W = sw[TARGET - 1]; 
          S = sw[TARGET + COL_SIZE];
        }	/*	Edge 2	*/
        else if (c == grid_cols - 1) {
          N = sw[TARGET - COL_SIZE];
          S = sw[TARGET + COL_SIZE];
          W = sw[TARGET - 1];
        }	/*	Edge 3	*/
        else if (r == grid_rows - 1) {
          N = sw[TARGET - COL_SIZE];
          E = sw[TARGET + 1];
          W = sw[TARGET - 1];
        }	/*	Edge 4	*/
        else if (c == 0) {
          N = sw[TARGET - COL_SIZE];
          S = sw[TARGET + COL_SIZE];
          E = sw[TARGET + 1];
        }	/*	Inside the chip	*/
        else {
          N = sw[TARGET - COL_SIZE];
          E = sw[TARGET + 1];
          W = sw[TARGET - 1]; 
          S = sw[TARGET + COL_SIZE];
        }

        //float v = power[r*grid_cols + c] + 
        float v = power[rc] + 
          (E + W - 2.0f * sw[TARGET]) * Rx_1 + 
          (N + S - 2.0f * sw[TARGET]) * Ry_1 + 
          (amb_temp - sw[TARGET]) * Rz_1; 

        float delta = step_div_Cap * v;

        /*	Update Temperatures	*/
        temp[r*grid_cols + c] = sw[TARGET] + delta;
      }

      c++;
      if (c == grid_cols) { r++; c = 0; }   
    } // exit Main loop 

  } //exit acc


  } // exit pipe

}
#endif
