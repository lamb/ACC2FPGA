#include "hotspot.h"

#if SW_MULTI_CHANNEL 


int single_iteration(float *power, float *temp, float *result, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{
  float delta;
  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

  // Also want to pipe power, but can't be done with current API
  #pragma acc data pipe(result[0:grid_rows*grid_cols])
  {

  // Enclose Everything
  #pragma acc parallel num_gangs(1) num_workers(1) \
      present(power, temp, result) pipeout(result)
  {
    // Step 1
    #define SSIZE (2)
    #define TARGET (ROW_SIZE)
    #define SW_BASE_SIZE (2*ROW_SIZE)
    #define SW_SIZE (SW_BASE_SIZE + SSIZE)

    // intialize window
    float sw[SW_SIZE];
    #pragma unroll
    for (int i = 0; i < SW_SIZE; ++i) 
      sw[i] = 0;

    int c = 0, r = -1;
    for (int rc = TARGET - SW_BASE_SIZE; rc < grid_rows*grid_cols; rc += SSIZE) {

      // slide the window 
      #pragma unroll
      for (int i = 0; i < SW_BASE_SIZE; ++i) {
        sw[i] = sw[i + SSIZE];
      }

      // shift in new inputs 
      int read_offset = rc + SW_BASE_SIZE - TARGET;
      #pragma unroll
      for (int i = 0; i < SSIZE; ++i) {
          sw[SW_BASE_SIZE + i] = temp[read_offset + i];
      }
      
      float value[SSIZE];

      // check if in initalization iterations
      if (rc >= 0) {

        #pragma unroll
        for (int ss = 0; ss < SSIZE; ++ss) {

          float N = sw[TARGET+ss], 
                S = sw[TARGET+ss],
                E = sw[TARGET+ss],
                W = sw[TARGET+ss];

          // Main Computation
          /*	Corner 1	*/
          if ( (r == 0) && (c+ss == 0) ) {
            S = sw[TARGET+ss + COL_SIZE];
            E = sw[TARGET+ss + 1];
          }	/*	Corner 2	*/
          else if ((r == 0) && (c+ss == grid_cols - 1)) {
            S = sw[TARGET+ss + COL_SIZE];
            W = sw[TARGET+ss - 1];
          }	/*	Corner 3	*/
          else if ((r == grid_rows - 1) && (c+ss == grid_cols - 1)) {
            N = sw[TARGET+ss - COL_SIZE];
            W = sw[TARGET+ss - 1];
          }	/*	Corner 4	*/
          else if ((r == grid_rows - 1) && (c+ss == 0)) {
            N = sw[TARGET+ss - COL_SIZE];
            E = sw[TARGET+ss + 1];
          }	/*	Edge 1	*/
          else if (r == 0) {
            E = sw[TARGET+ss + 1];
            W = sw[TARGET+ss - 1]; 
            S = sw[TARGET+ss + COL_SIZE];
          }	/*	Edge 2	*/
          else if (c+ss == grid_cols - 1) {
            N = sw[TARGET+ss - COL_SIZE];
            S = sw[TARGET+ss + COL_SIZE];
            W = sw[TARGET+ss - 1];
          }	/*	Edge 3	*/
          else if (r == grid_rows - 1) {
            N = sw[TARGET+ss - COL_SIZE];
            E = sw[TARGET+ss + 1];
            W = sw[TARGET+ss - 1];
          }	/*	Edge 4	*/
          else if (c+ss == 0) {
            N = sw[TARGET+ss - COL_SIZE];
            S = sw[TARGET+ss + COL_SIZE];
            E = sw[TARGET+ss + 1];
          }	/*	Inside the chip	*/
          else {
            N = sw[TARGET+ss - COL_SIZE];
            E = sw[TARGET+ss + 1];
            W = sw[TARGET+ss - 1]; 
            S = sw[TARGET+ss + COL_SIZE];
          }

          //float v = power[r*grid_cols + c] + 
          float v = power[rc] + 
            (E + W - 2.0f * sw[TARGET+ss]) * Rx_1 + 
            (N + S - 2.0f * sw[TARGET+ss]) * Ry_1 + 
            (amb_temp - sw[TARGET+ss]) * Rz_1; 

          float delta = step_div_Cap * v;

          /*	Update Temperatures	*/
          value[ss] = sw[TARGET+ss] + delta;
        } // SSIZE loop

        // assign values to results array
        #pragma unroll
        for (int i = 0; i < SSIZE; ++i) {
          result[r*grid_cols + c + i] = value[i];
        }

      } // iter >= 0 loop


      c += SSIZE;
      if (c == grid_cols) { r++; c = 0; }   
    } // exit Main loop 


  } //exit acc


  // Enclose Everything
  #pragma acc parallel num_gangs(1) num_workers(1) \
      present(power, temp, result) pipein(result)
  {
    // Step 1
    #define SSIZE (2)
    #define TARGET (ROW_SIZE)
    #define SW_BASE_SIZE (2*ROW_SIZE)
    #define SW_SIZE (SW_BASE_SIZE + SSIZE)

    // intialize window
    float sw[SW_SIZE];
    #pragma unroll
    for (int i = 0; i < SW_SIZE; ++i) 
      sw[i] = 0;

    int c = 0, r = -1;
    for (int rc = TARGET - SW_BASE_SIZE; rc < grid_rows*grid_cols; rc += SSIZE) {

      // slide the window 
      #pragma unroll
      for (int i = 0; i < SW_BASE_SIZE; ++i) {
        sw[i] = sw[i + SSIZE];
      }

      // shift in new inputs 
      int read_offset = rc + SW_BASE_SIZE - TARGET;
      for (int i = 0; i < SSIZE; ++i) {
        #pragma unroll
        sw[SW_BASE_SIZE + i] = result[read_offset + i];
      }
      
      float value[SSIZE];

      // check if in initalization iterations
      if (rc >= 0) {

        #pragma unroll
        for (int ss = 0; ss < SSIZE; ++ss) {

          float N = sw[TARGET+ss], 
                S = sw[TARGET+ss],
                E = sw[TARGET+ss],
                W = sw[TARGET+ss];

          // Main Computation
          /*	Corner 1	*/
          if ( (r == 0) && (c+ss == 0) ) {
            S = sw[TARGET+ss + COL_SIZE];
            E = sw[TARGET+ss + 1];
          }	/*	Corner 2	*/
          else if ((r == 0) && (c+ss == grid_cols - 1)) {
            S = sw[TARGET+ss + COL_SIZE];
            W = sw[TARGET+ss - 1];
          }	/*	Corner 3	*/
          else if ((r == grid_rows - 1) && (c+ss == grid_cols - 1)) {
            N = sw[TARGET+ss - COL_SIZE];
            W = sw[TARGET+ss - 1];
          }	/*	Corner 4	*/
          else if ((r == grid_rows - 1) && (c+ss == 0)) {
            N = sw[TARGET+ss - COL_SIZE];
            E = sw[TARGET+ss + 1];
          }	/*	Edge 1	*/
          else if (r == 0) {
            E = sw[TARGET+ss + 1];
            W = sw[TARGET+ss - 1]; 
            S = sw[TARGET+ss + COL_SIZE];
          }	/*	Edge 2	*/
          else if (c+ss == grid_cols - 1) {
            N = sw[TARGET+ss - COL_SIZE];
            S = sw[TARGET+ss + COL_SIZE];
            W = sw[TARGET+ss - 1];
          }	/*	Edge 3	*/
          else if (r == grid_rows - 1) {
            N = sw[TARGET+ss - COL_SIZE];
            E = sw[TARGET+ss + 1];
            W = sw[TARGET+ss - 1];
          }	/*	Edge 4	*/
          else if (c+ss == 0) {
            N = sw[TARGET+ss - COL_SIZE];
            S = sw[TARGET+ss + COL_SIZE];
            E = sw[TARGET+ss + 1];
          }	/*	Inside the chip	*/
          else {
            N = sw[TARGET+ss - COL_SIZE];
            E = sw[TARGET+ss + 1];
            W = sw[TARGET+ss - 1]; 
            S = sw[TARGET+ss + COL_SIZE];
          }

          //float v = power[r*grid_cols + c] + 
          float v = power[rc] + 
            (E + W - 2.0f * sw[TARGET+ss]) * Rx_1 + 
            (N + S - 2.0f * sw[TARGET+ss]) * Ry_1 + 
            (amb_temp - sw[TARGET+ss]) * Rz_1; 

          float delta = step_div_Cap * v;

          /*	Update Temperatures	*/
          value[ss] = sw[TARGET+ss] + delta;
        } // SSIZE loop

        // assign values to results array
        #pragma unroll
        for (int i = 0; i < SSIZE; ++i) {
          temp[r*grid_cols + c + i] = value[i];
        }

      } // iter >= 0 loop


      c += SSIZE;
      if (c == grid_cols) { r++; c = 0; }   
    } // exit Main loop 

  } //exit acc


  } // exit pipe

}
#endif
