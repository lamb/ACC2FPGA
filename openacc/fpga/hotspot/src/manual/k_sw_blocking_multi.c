#include "hotspot.h"

// need to fix / %
// need to fix collapse indicies
// may need to fix read offsets
// need to fix ii/ss
//

#if SW_BLOCKING_MULTI

// Step 1
#define SSIZE (4)
#define BSIZE (1024)
#define TARGET (BSIZE)
#define SW_BASE_SIZE (2*BSIZE)
#define SW_SIZE (SW_BASE_SIZE + SSIZE)

#define INIT_ITER (1)

int single_iteration(float *power, float *temp, float *result, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{
  float delta;
  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

  // Enclose Everything
  #pragma acc parallel num_gangs(1) num_workers(1) \
      present(power, temp, result)
  {
    // intialize window
    float sw[SW_SIZE];
    #pragma unroll
    for (int ii = 0; ii < SW_SIZE; ++ii) 
      sw[ii] = 0;

    // redifine loop header
    int x = 0, y = 0, bx = 0;
    do {

      // offset variables
      int gx = bx + x;
      int comp_offset_y = y - INIT_ITER;
      int comp_offset = gx + comp_offset_y * grid_rows;
      int read_offset = bx + (x + SW_BASE_SIZE - TARGET) % BSIZE +
        (((x + SW_BASE_SIZE - TARGET) / BSIZE) + comp_offset_y)*grid_cols;

      // slide the window 
      #pragma unroll
      for (int ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + SSIZE];
      }

      // shift in new temps
      #pragma unroll
      for (int ii = 0; ii < SSIZE; ++ii) {
        sw[SW_BASE_SIZE + ii] = read_offset + ii < 0 ? 0 : temp[read_offset + ii];
      }

      // store multiple results per iteration 
      float value[SSIZE];

      // conditional to check for intialization phase
      if (comp_offset_y >= 0 && comp_offset_y < grid_rows) {

        #pragma unroll
        for (int ii = 0; ii < SSIZE; ++ii) {
        
          // from collapse clause
          int r = comp_offset_y;
          int c = gx + ii;

          // Main Computation
          /*	Corner 1	*/
          if ( (r == 0) && (c == 0) ) {
            delta = (step_div_Cap) * (power[0] +
                (sw[TARGET + 1] - 
                 sw[TARGET + 0]) * Rx_1 +
                (sw[TARGET + BSIZE] - 
                 sw[TARGET + 0]) * Ry_1 +
                (amb_temp - sw[TARGET + 0]) * Rz_1);
          }	/*	Corner 2	*/
          else if ((r == 0) && (c == grid_cols - 1)) {
            delta = (step_div_Cap) * (power[c] +
                (sw[TARGET + (0+ii) - 1] - 
                 sw[TARGET + (0+ii)]) * Rx_1 +
                (sw[TARGET + (0+ii) + BSIZE] - 
                 sw[TARGET + (0+ii)]) * Ry_1 +
                (amb_temp - sw[TARGET + (0+ii)]) * Rz_1);
          }	/*	Corner 3	*/
          else if ((r == grid_rows - 1) && (c == grid_cols - 1)) {
            delta = (step_div_Cap) * (power[r*grid_cols + c] + 
                (sw[TARGET + 0*BSIZE + (0+ii) - 1] - 
                 sw[TARGET + 0*BSIZE + (0+ii)]) * Rx_1 + 
                (sw[TARGET + (0 - 1)*BSIZE + (0+ii)] - 
                 sw[TARGET + 0*BSIZE + (0+ii)]) * Ry_1 + 
                (amb_temp - sw[TARGET + 0*BSIZE + (0+ii)]) * Rz_1);
          }	/*	Corner 4	*/
          else if ((r == grid_rows - 1) && (c == 0)) {
            delta = (step_div_Cap) * (power[r*grid_cols] + 
                (sw[TARGET + 0*BSIZE + 1] - 
                 sw[TARGET + 0*BSIZE]) * Rx_1 + 
                (sw[TARGET + (0 - 1)*BSIZE] - 
                 sw[TARGET + 0*BSIZE]) * Ry_1 + 
                (amb_temp - sw[TARGET + 0*BSIZE]) * Rz_1);
          }	/*	Edge 1	*/
          else if (r == 0) {
            float east = (x + SSIZE == BSIZE && ii >= SSIZE - 1) ? 
              temp[c + 1] :
              sw[TARGET + (0+ii) + 1];
            float west = (x == 0 && ii < 1 ) ? 
              temp[c - 1] :
              sw[TARGET + (0+ii) - 1];

            delta = (step_div_Cap) * (power[c] + 
                (east + 
                 west - 
                 2.0F*sw[TARGET + (0+ii)]) * Rx_1 + 
                (sw[TARGET + BSIZE + (0+ii)] - 
                 sw[TARGET + (0+ii)]) * Ry_1 + 
                (amb_temp - sw[TARGET + (0+ii)]) * Rz_1);
          }	/*	Edge 2	*/
          else if (c == grid_cols - 1) {
            delta = (step_div_Cap) * (power[r*grid_cols + c] + 
                (sw[TARGET + (0 + 1)*BSIZE + (0+ii)] + 
                 sw[TARGET + (0 - 1)*BSIZE + (0+ii)] - 
                 2.0F*sw[TARGET + 0*BSIZE + (0+ii)]) * Ry_1 + 
                (sw[TARGET + 0*BSIZE + (0+ii) - 1] - 
                 sw[TARGET + 0*BSIZE + (0+ii)]) * Rx_1 + 
                (amb_temp - sw[TARGET + 0*BSIZE + (0+ii)]) * Rz_1);
          }	/*	Edge 3	*/
          else if (r == grid_rows - 1) {
            float east = (x + SSIZE == BSIZE && ii >= SSIZE - 1) ? 
              temp[r*grid_cols + c + 1] :
              sw[TARGET + 0*BSIZE + (0+ii) + 1];
            float west = (x == 0 && ii < 1 ) ? 
              temp[r*grid_cols + c - 1] :
              sw[TARGET + 0*BSIZE + (0+ii) - 1];

            delta = (step_div_Cap) * (power[r*grid_cols + c] + 
                (east + 
                 west - 
                 2.0F*sw[TARGET + 0*BSIZE + (0+ii)]) * Rx_1 + 
                (sw[TARGET + (0 - 1)*BSIZE + (0+ii)] - 
                 sw[TARGET + 0*BSIZE + (0+ii)]) * Ry_1 + 
                (amb_temp - sw[TARGET + 0*BSIZE + (0+ii)]) * Rz_1);
          }	/*	Edge 4	*/
          else if (c == 0) {
            delta = (step_div_Cap) * (power[r*grid_cols] + 
                (sw[TARGET + (0 + 1)*BSIZE] + 
                 sw[TARGET + (0 - 1)*BSIZE] - 
                 2.0F*sw[TARGET + 0*BSIZE]) * Ry_1 + 
                (sw[TARGET + 0*BSIZE + 1] - 
                 sw[TARGET + 0*BSIZE]) * Rx_1 + 
                (amb_temp - sw[TARGET + 0*BSIZE]) * Rz_1);
          }	/*	Inside the chip	*/
          else {

            float east = (x + SSIZE == BSIZE && ii >= SSIZE - 1) ? 
              temp[r*grid_cols + c + 1] :
              sw[TARGET + 0*BSIZE + (0+ii) + 1];
            float west = (x == 0 && ii < 1 ) ? 
              temp[r*grid_cols + c - 1] :
              sw[TARGET + 0*BSIZE + (0+ii) - 1];

            delta = (step_div_Cap) * (power[r*grid_cols + c] + 
                (sw[TARGET + (0 + 1)*BSIZE + (0+ii)] + 
                 sw[TARGET + (0 - 1)*BSIZE + (0+ii)] - 
                 2.0F*sw[TARGET + 0*BSIZE + (0+ii)]) * Ry_1 + 
                (east + 
                 west -  
                 2.0F*sw[TARGET + 0*BSIZE + (0+ii)]) * Rx_1 + 
                (amb_temp - sw[TARGET + 0*BSIZE + (0+ii)]) * Rz_1);
          }

          /*	Update Temperatures	*/
          value[ii] = sw[TARGET + 0*BSIZE + (0+ii)] + delta;
        }

        int r = comp_offset_y;
        int c = gx;
        // assign values to result array
        #pragma unroll
        for (int ii = 0; ii < SSIZE; ++ii) {
          result[r*grid_cols + c + ii] = value[ii];
        }
      }

      // iteration variables
      x = x < BSIZE - SSIZE ? x + SSIZE: 0;
      y = x == 0 ? (y == grid_rows + INIT_ITER - 1 ? 0 : y + 1) : y;
      if (x == 0 && y == 0) bx += BSIZE;
    } while (bx < grid_cols);

  }

}
#endif
