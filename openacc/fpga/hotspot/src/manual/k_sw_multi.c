#include "hotspot.h"

#if SW_MULTI
int single_iteration(float *power, float *temp, float *result, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{
  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

  #pragma acc parallel num_workers(1) num_gangs(1) 
  {
    // need to use hard-coded ROW_SIZE in non-blocking versions 
    #define SSIZE (16)
    #define TARGET (ROW_SIZE)
    #define SW_BASE_SIZE (2*ROW_SIZE)
    #define SW_SIZE (SW_BASE_SIZE + SSIZE)

    // define the sliding window 
    float sw[SW_SIZE];
    #pragma unroll
    for (int i = 0; i <  SW_SIZE; ++i) { 
      sw[i] = 0;
    }

    float delta;

    int c = 0, r = -1;
    for (int rc = TARGET - SW_BASE_SIZE; rc < grid_rows*grid_cols; rc += SSIZE) {

      // slide the window
      #pragma unroll
      for (int i = 0; i < SW_BASE_SIZE; ++i) {
        sw[i] = sw[i + SSIZE];
      }

      // shift in new inputs
      int read_offset = rc + SW_BASE_SIZE - TARGET; 
      #pragma unroll
      for (int i = 0; i < SSIZE; ++i) {
        if (read_offset + i >= 0 && read_offset + i < grid_rows*grid_cols)
          sw[SW_BASE_SIZE + i] = temp[read_offset + i];
        else
          sw[SW_BASE_SIZE + i] = 0;
      }

      float value[SSIZE];

      #pragma unroll
      for (int ss = 0; ss < SSIZE; ++ss) {

        if (rc + ss >= 0) {

          /*	Corner 1	*/
          if ( (r == 0) && (c+ss == 0) ) {
            delta = (step_div_Cap) * (power[0] +
                (sw[TARGET+ss + 1] - 
                 sw[TARGET+ss]) * Rx_1 +
                (sw[TARGET+ss + COL_SIZE] - 
                 sw[TARGET+ss]) * Ry_1 +
                (amb_temp - sw[TARGET+ss]) * Rz_1);
          }	/*	Corner 2	*/
          else if ((r == 0) && (c+ss == grid_cols-1)) {
            delta = (step_div_Cap) * (power[c+ss] +
                (sw[TARGET+ss + 0 - 1] - 
                 sw[TARGET+ss + 0]) * Rx_1 +
                (sw[TARGET+ss + 0 + COL_SIZE] - 
                 sw[TARGET+ss + 0]) * Ry_1 +
                (amb_temp - sw[TARGET+ss + 0]) * Rz_1);
          }	/*	Corner 3	*/
          else if ((r == grid_rows - 1) && (c+ss == grid_cols - 1)) {
            delta = (step_div_Cap) * (power[r*grid_cols + c+ss] + 
                (sw[TARGET+ss + 0*COL_SIZE + 0 - 1] - 
                 sw[TARGET+ss + 0*COL_SIZE + 0]) * Rx_1 + 
                (sw[TARGET+ss + (0 - 1)*COL_SIZE + 0] - 
                 sw[TARGET+ss + 0*COL_SIZE + 0]) * Ry_1 + 
                (amb_temp - sw[TARGET+ss + 0*COL_SIZE + 0]) * Rz_1);
          }	/*	Corner 4	*/
          else if ((r == grid_rows - 1) && (c+ss == 0)) {
            delta = (step_div_Cap) * (power[r*grid_cols] + 
                (sw[TARGET+ss + 0*COL_SIZE + 1] - 
                 sw[TARGET+ss + 0*COL_SIZE]) * Rx_1 + 
                (sw[TARGET+ss + (0 - 1)*COL_SIZE] - 
                 sw[TARGET+ss + 0*COL_SIZE]) * Ry_1 + 
                (amb_temp - sw[TARGET+ss + 0*COL_SIZE]) * Rz_1);
          }	/*	Edge 1	*/
          else if (r == 0) {
            delta = (step_div_Cap) * (power[c+ss] + 
                (sw[TARGET+ss + 0 + 1] + 
                 sw[TARGET+ss + 0 - 1] - 
                 2.0F*sw[TARGET+ss + 0]) * Rx_1 + 
                (sw[TARGET+ss + COL_SIZE + 0] - 
                 sw[TARGET+ss + 0]) * Ry_1 + 
                (amb_temp - sw[TARGET+ss + 0]) * Rz_1);
          }	/*	Edge 2	*/
          else if (c+ss == grid_cols - 1) {
            delta = (step_div_Cap) * (power[r*grid_cols + c+ss] + 
                (sw[TARGET+ss + (0 + 1)*COL_SIZE + 0] + 
                 sw[TARGET+ss + (0 - 1)*COL_SIZE + 0] - 
                 2.0F*sw[TARGET+ss + 0*COL_SIZE + 0]) * Ry_1 + 
                (sw[TARGET+ss + 0*COL_SIZE + 0 - 1] - 
                 sw[TARGET+ss + 0*COL_SIZE + 0]) * Rx_1 + 
                (amb_temp - sw[TARGET+ss + 0*COL_SIZE + 0]) * Rz_1);
          }	/*	Edge 3	*/
          else if (r == grid_rows - 1) {
            delta = (step_div_Cap) * (power[r*grid_cols + c+ss] + 
                (sw[TARGET+ss + 0*COL_SIZE + 0 + 1] + 
                 sw[TARGET+ss + 0*COL_SIZE + 0 - 1] - 
                 2.0F*sw[TARGET+ss + 0*COL_SIZE + 0]) * Rx_1 + 
                (sw[TARGET+ss + (0 - 1)*COL_SIZE + 0] - 
                 sw[TARGET+ss + 0*COL_SIZE + 0]) * Ry_1 + 
                (amb_temp - sw[TARGET+ss + 0*COL_SIZE + 0]) * Rz_1);
          }	/*	Edge 4	*/
          else if (c+ss == 0) {
            delta = (step_div_Cap) * (power[r*grid_cols] + 
                (sw[TARGET+ss + (0 + 1)*COL_SIZE] + 
                 sw[TARGET+ss + (0 - 1)*COL_SIZE] - 
                 2.0F*sw[TARGET+ss + 0*COL_SIZE]) * Ry_1 + 
                (sw[TARGET+ss + 0*COL_SIZE + 1] - 
                 sw[TARGET+ss + 0*COL_SIZE]) * Rx_1 + 
                (amb_temp - sw[TARGET+ss + 0*COL_SIZE]) * Rz_1);
          }	/*	Inside the chip	*/
          else {
            delta = (step_div_Cap) * (power[r*grid_cols + c+ss] + 
                (sw[TARGET+ss + (0 + 1)*COL_SIZE + 0] + 
                 sw[TARGET+ss + (0 - 1)*COL_SIZE + 0] - 
                 2.0F*sw[TARGET+ss + 0*COL_SIZE + 0]) * Ry_1 + 
                (sw[TARGET+ss + 0*COL_SIZE + 0 + 1] + 
                 sw[TARGET+ss + 0*COL_SIZE + 0 - 1] - 
                 2.0F*sw[TARGET+ss + 0*COL_SIZE + 0]) * Rx_1 + 
                (amb_temp - sw[TARGET+ss + 0*COL_SIZE + 0]) * Rz_1);
          }

          /*	Update Temperatures	*/
          value[ss] = sw[TARGET+ss + 0*COL_SIZE + 0] + delta;

        } // rc >= 0 condition 
      } // SSIZE loop

      // assign values to result array
      #pragma unroll
      for (int i = 0; i < SSIZE; ++i) {
        if (rc + i >= 0)
          result[r*grid_cols + c + i] = value[i];
      } 

      c += SSIZE;
      if (c == grid_cols) { r++; c = 0; }
    } // exit Main Loop
  } // exit acc

}
#endif
