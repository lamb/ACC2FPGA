#include "hotspot.h"

// need to fix / %
// need to fix collapse indicies
// may need to fix read offsets
// need to fix ii/ss

#if SW_BLOCKING_MULTI_HOIST

// Step 1
#define SSIZE (2)
#define BSIZE (1024)
#define TARGET (BSIZE)
#define SW_BASE_SIZE (2*BSIZE)
#define SW_SIZE (SW_BASE_SIZE + SSIZE)

#define INIT_ITER (1)

int single_iteration(float *power, float *temp, float *result, int grid_cols, 
    int grid_rows, float Cap, float Rx, float Ry, float Rz, float step) 
{
  float delta;
  float step_div_Cap = step/Cap;
  float Rx_1 = 1 / Rx;
  float Ry_1 = 1 / Ry;
  float Rz_1 = 1 / Rz;

  // Enclose Everything
  #pragma acc parallel num_gangs(1) num_workers(1) \
      present(power, temp, result)
  {
    // intialize window
    float sw[SW_SIZE];
    #pragma unroll
    for (int ii = 0; ii < SW_SIZE; ++ii) 
      sw[ii] = 0;

    // redifine loop header
    int x = 0, y = 0, bx = 0;
    do {

      // offset variables
      int gx = bx + x;
      int comp_offset_y = y - INIT_ITER;
      int comp_offset = gx + comp_offset_y * grid_rows;
      int read_offset = bx + (x + SW_BASE_SIZE - TARGET) % BSIZE +
        (((x + SW_BASE_SIZE - TARGET) / BSIZE) + comp_offset_y)*grid_cols;

      // slide the window 
      #pragma unroll
      for (int ii = 0; ii < SW_BASE_SIZE; ++ii) {
        sw[ii] = sw[ii + SSIZE];
      }

      // shift in new inputs 
      #pragma unroll
      for (int ii = 0; ii < SSIZE; ++ii) {
        sw[SW_BASE_SIZE + ii] = read_offset + ii < 0 ? 0 : temp[read_offset + ii];
      }

      // store multiple results per iteration 
      float value[SSIZE];

      // conditional to check for intialization phase
      if (comp_offset_y >= 0 && comp_offset_y < grid_rows) {

        #pragma unroll
        for (int ii = 0; ii < SSIZE; ++ii) {
        
          // from collapse clause
          int r = comp_offset_y; 
          int c = gx + ii;

          float N = sw[TARGET + ii], 
                S = sw[TARGET + ii],
                E = sw[TARGET + ii],
                W = sw[TARGET + ii];

          // Main Computation
          /*	Corner 1	*/
          if ( (r == 0) && (c == 0) ) {
            S = sw[TARGET + BSIZE + ii];
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ?
                temp[comp_offset + SSIZE] :
                sw[TARGET+ii + 1];
          }	/*	Corner 2	*/
          else if ((r == 0) && (c == grid_cols - 1)) {
            S = sw[TARGET + BSIZE + ii];
            W = (ii == 0 && x == 0) ?
                temp[comp_offset - 1] :
                sw[TARGET+ii - 1];
          }	/*	Corner 3	*/
          else if ((r == grid_rows - 1) && (c == grid_cols - 1)) {
            N = sw[TARGET+ii - BSIZE];
            W = (ii == 0 && x == 0) ?
                temp[comp_offset - 1] :
                sw[TARGET+ii - 1];
          }	/*	Corner 4	*/
          else if ((r == grid_rows - 1) && (c == 0)) {
            N = sw[TARGET+ii - BSIZE];
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ?
                temp[comp_offset + SSIZE] :
                sw[TARGET+ii + 1];

          }	/*	Edge 1	*/
          else if (r == 0) {
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ?
                temp[comp_offset + SSIZE] :
                sw[TARGET+ii + 1];
            W = (ii == 0 && x == 0) ?
                temp[comp_offset - 1] :
                sw[TARGET+ii - 1]; 
            S = sw[TARGET + BSIZE + ii];
          }	/*	Edge 2	*/
          else if (c == grid_cols - 1) {
            N = sw[TARGET+ii - BSIZE];
            S = sw[TARGET + BSIZE + ii];
            W = (ii == 0 && x == 0) ?
                temp[comp_offset - 1] :
                sw[TARGET+ii - 1];
          }	/*	Edge 3	*/
          else if (r == grid_rows - 1) {
            N = sw[TARGET+ii - BSIZE];
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ?
                temp[comp_offset + SSIZE] :
                sw[TARGET+ii + 1];
            W = (ii == 0 && x == 0) ?
                temp[comp_offset - 1] :
                sw[TARGET+ii - 1];
          }	/*	Edge 4	*/
          else if (c == 0) {
            N = sw[TARGET+ii - BSIZE];
            S = sw[TARGET+ii + BSIZE];
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ?
                temp[comp_offset + SSIZE] :
                sw[TARGET+ii + 1];
          }	/*	Inside the chip	*/
          else {
            //N = sw[(r-1)*BSIZE + (x+ii)];
            N = sw[TARGET+ii - BSIZE];
            E = (ii == SSIZE - 1 && x + SSIZE == BSIZE) ?
                temp[comp_offset + SSIZE] :
                sw[TARGET+ii + 1];
            W = (ii == 0 && x == 0) ?
                temp[comp_offset - 1] :
                sw[TARGET+ii - 1]; 
            S = sw[TARGET+ii + BSIZE];
          }

          float v = power[comp_offset + ii] + 
            (E + W - 2.0f * sw[TARGET + ii]) * Rx_1 + 
            (N + S - 2.0f * sw[TARGET + ii]) * Ry_1 + 
            (amb_temp - sw[TARGET + ii]) * Rz_1; 

          float delta = step_div_Cap * v;

          /*	Update Temperatures	*/
          value[ii] = sw[TARGET + ii] + delta;
        }

        int r = comp_offset_y; 
        int c = gx;
        // assign values to result array
        #pragma unroll
        for (int ii = 0; ii < SSIZE; ++ii) {
          result[r*grid_cols + c + ii] = value[ii];
        }
      }

      // iteration variables
      x = x < BSIZE - SSIZE ? x + SSIZE: 0;
      y = x == 0 ? (y == grid_rows + INIT_ITER - 1 ? 0 : y + 1) : y;
      if (x == 0 && y == 0) bx += BSIZE;
    } while (bx < grid_cols);

  }

}
#endif
