#include "versions.h"

#if WINDOW

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#if OMP == 1
#include <omp.h>
#endif

double my_timer ()
{
  struct timeval time;

  gettimeofday (&time, 0);

  return time.tv_sec + time.tv_usec / 1000000.0;
}

float a_flat[SIZE_2*SIZE_2];
float b_flat[SIZE_2*SIZE_2];

int main (int argc, char *argv[])
{
  //int c;
  float sum = 0.0f;

  double strt_time, done_time;
#if VERIFICATION >= 1
  float** a_CPU = (float**)malloc(sizeof(float*) * SIZE_2);
  float** b_CPU = (float**)malloc(sizeof(float*) * SIZE_2);

  float* a_data = (float*)malloc(sizeof(float) * SIZE_2 * SIZE_2);
  float* b_data = (float*)malloc(sizeof(float) * SIZE_2 * SIZE_2);

  for (int i = 0; i < SIZE_2; i++)
  {
    a_CPU[i] = &a_data[i * SIZE_2];
    b_CPU[i] = &b_data[i * SIZE_2];
  }

#endif 

  //while ((c = getopt (argc, argv, "")) != -1);

  for (int i = 0; i < SIZE_2; i++)
  {
    for (int j = 0; j < SIZE_2; j++)
    {
      b_flat[i*SIZE_2 + j] = 0;
#if VERIFICATION >= 1
      b_CPU[i][j] = 0;
#endif 
    }
  }

  for (int j = 0; j <= SIZE_1; j++)
  {
    b_flat[j*SIZE_2] = 1.0;
    b_flat[j*SIZE_2 + SIZE_1] = 1.0;

#if VERIFICATION >= 1
    b_CPU[j][0] = 1.0;
    b_CPU[j][SIZE_1] = 1.0;
#endif 

  }
  for (int i = 1; i <= SIZE; i++)
  {
    b_flat[i] = 1.0;
    b_flat[SIZE_1*SIZE_2 + i] = 1.0;

#if VERIFICATION >= 1
    b_CPU[0][i] = 1.0;
    b_CPU[SIZE_1][i] = 1.0;
#endif 
  }

  printf ("Performing %d iterations on a %d by %d array\n", ITER, SIZE, SIZE);

  /* -- Timing starts before the main loop -- */
  printf("-------------------------------------------------------------\n");

  strt_time = my_timer ();

  #pragma acc data copy(b_flat[0:SIZE_2*SIZE_2]), create(a_flat[0:SIZE_2*SIZE_2])
  for (int k = 0; k < ITER; k++) {
    #pragma openarc transform window(b_flat[0:SIZE_2*SIZE_2], a_flat[0:SIZE_2*SIZE_2])
    #pragma unroll 16
    #pragma acc parallel loop num_gangs(1) num_workers(1) collapse(2) 
    //#pragma ivdep
    for (int i = 0; i < SIZE_2; i++) {
      for (int j = 0; j < SIZE_2; j++) {
        if (i > 0 && j > 0 && i < SIZE_2 - 1 && j < SIZE_2 - 1)
          a_flat[i*SIZE_2 + j] = 
            (b_flat[(i-1)*SIZE_2+j] + 
             b_flat[(i+1)*SIZE_2+j] + 
             b_flat[(i)*SIZE_2+j-1] + 
             b_flat[(i)*SIZE_2+j+1]) / 4.0f;
      }
    }

    #pragma acc parallel loop gang worker //num_workers(SIZE_2-2) num_gangs(SIZE_2-2) //collapse(2)
    #pragma openarc transform permute(j,i)
    //#pragma openarc opencl num_simd_work_items(16)
    #pragma openarc opencl num_compute_units(4)
    //#pragma acc parallel loop num_gangs(1) num_workers(1) collapse(2) 
    #pragma ivdep
    //for (int i = 0; i < SIZE_2; i++) {
    //  for (int j = 0; j < SIZE_2; j++) {
    for (int i = 1; i < SIZE_2 - 1; i++) {
      for (int j = 1; j < SIZE_2 - 1; j++) {
        //if (i > 0 && j > 0 && i < SIZE_2 - 1 && j < SIZE_2 - 1)
          b_flat[i*SIZE_2 + j] = a_flat[i*SIZE_2 + j];
      }
    }
  }

  done_time = my_timer ();
  printf ("Accelerator Elapsed time = %lf sec\n", done_time - strt_time);

#if VERIFICATION >= 1

  strt_time = my_timer ();

  for (int k = 0; k < ITER; k++)
  {
#if LOOP_TILING == 1
#pragma acc loop tile(16,16)
#endif
#pragma omp parallel for shared(a_CPU,b_CPU) private(i,j)
    for (int i = 1; i <= SIZE; i++)
    {
      for (int j = 1; j <= SIZE; j++)
      {
        a_CPU[i][j] = (b_CPU[i - 1][j] + b_CPU[i + 1][j] + b_CPU[i][j - 1] + b_CPU[i][j + 1]) / 4.0f;
      }
    }

#if LOOP_TILING == 1
#pragma acc loop tile(16,16)
#endif
#pragma omp parallel for shared(a_CPU,b_CPU) private(i,j)
    for (int i = 1; i <= SIZE; i++)
    {
      for (int j = 1; j <= SIZE; j++)
      {
        b_CPU[i][j] = a_CPU[i][j];
      }
    }
  }

  done_time = my_timer ();
  printf ("Reference CPU time = %lf sec\n", done_time - strt_time);
#if VERIFICATION == 1
  {
    double cpu_sum = 0.0;
    double gpu_sum = 0.0;
    double rel_err = 0.0;

    for (int i = 1; i <= SIZE; i++)
    {
      cpu_sum += b_CPU[i][i]*b_CPU[i][i];

      int index = i*SIZE_2 + i;
      gpu_sum += b_flat[index]*b_flat[index];
    }

    cpu_sum = sqrt(cpu_sum);
    gpu_sum = sqrt(gpu_sum);
    if( cpu_sum > gpu_sum) {
      rel_err = (cpu_sum-gpu_sum)/cpu_sum;
    } else {
      rel_err = (gpu_sum-cpu_sum)/cpu_sum;
    }

    if(rel_err < 1e-9)
    {
      printf("Verification Successful err = %e\n", rel_err);
    }
    else
    {
      printf("Verification Fail err = %e\n", rel_err);
    }
  }
#else
  {
    double cpu_sum = 0.0;
    double gpu_sum = 0.0;
    double rel_err = 0.0;
    int error_found = 0;

    for (int i = 1; i <= SIZE; i++)
    {
      for (int j = 1; j <= SIZE; j++)
      {
        cpu_sum = b_CPU[i][j];

        int index = i*SIZE_2 + j;
        gpu_sum = b_flat[index];
        if( cpu_sum == gpu_sum ) {
          continue;
        }
        if( cpu_sum > gpu_sum) {
          if( cpu_sum == 0.0 ) {
            rel_err = cpu_sum-gpu_sum;
          } else {
            rel_err = (cpu_sum-gpu_sum)/cpu_sum;
          }
        } else {
          if( cpu_sum == 0.0 ) {
            rel_err = gpu_sum-cpu_sum;
          } else {
            rel_err = (gpu_sum-cpu_sum)/cpu_sum;
          }
        }
        if(rel_err < 0.0) {
          rel_err = -1*rel_err;
        }

        if(rel_err >= 1e-9)
        {
          error_found = 1;
          break;
        }
      }
      if( error_found == 1 ) {
        break;
      }
    }
    if( error_found == 0 )
    {
      printf("Verification Successful\n");
    }
    else
    {
      printf("Verification Fail err = %e\n", rel_err);
    }
  }
#endif
#endif


#ifdef CHECK_RESULT
  for (int i = 1; i <= SIZE; i++)
  {
    int index = i*SIZE_2 + i;
    sum += b_flat[index];
  }
  printf("Diagonal sum = %.10E\n", sum);
#endif

  return 0;
}

#endif
