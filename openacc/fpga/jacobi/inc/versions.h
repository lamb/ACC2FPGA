// Multi-threaded versions
#define ND_ND   1

// Single-threaded versions
#define SWI_SWI 0
#define WINDOW  0

#ifndef VERIFICATION
#define VERIFICATION 1
#endif

#define ITER 10

#ifndef SIZE
//#define SIZE 2048 //128 * 16
//#define SIZE 4096 //256 * 16
#define SIZE 8192 //256 * 32
//#define SIZE 12288 //256 * 48
#ifdef _OPENARC_
#pragma openarc #define SIZE 8192
#endif
#endif

#define SIZE_1  (SIZE+1)
#define SIZE_2  (SIZE+2)

#ifdef _OPENARC_
#pragma openarc #define SIZE_2 (2+SIZE)
#endif

#define CHECK_RESULT
