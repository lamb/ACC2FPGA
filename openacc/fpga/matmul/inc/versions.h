
// multi-threaded
#define ND        0
#define ND_BLOCK  0

// single-threaded
#define SWI       1
#define SWI_BLOCK 0

#if OMP == 1
#include <omp.h>
#endif

#ifdef _OPENACCM
#include <openacc.h>
#endif

#ifndef _N_
#define _N_ 512
#endif

#ifdef _OPENARC_
#if _N_ == 512
#pragma openarc #define _N_ 512
#endif
#if _N_ == 2048
#pragma openarc #define _N_ 2048
#endif
#endif

#define N _N_
#define M _N_
#define P _N_

#ifdef _OPENARC_
#pragma openarc #define N _N_
#pragma openarc #define M _N_
#pragma openarc #define P _N_
#endif

#define MUL(x,y) ((x)*(y)) 

#ifndef VERIFICATION
#define VERIFICATION 1
#endif

#ifndef TRANSPOSE_Bs
#define TRANSPOSE_Bs 0
#endif

#ifndef HOST_MEM_ALIGNMENT
#define HOST_MEM_ALIGNMENT 1
#endif

#if HOST_MEM_ALIGNMENT == 1
#define AOCL_ALIGNMENT 64
#endif

#ifndef DEBUG_PRINT
#define DEBUG_PRINT 0
#endif

#ifndef BLOCK_SIZE
#define BLOCK_SIZE 16
#endif

#ifndef _UNROLL_FAC_
#define _UNROLL_FAC_ 16
#pragma openarc #define _UNROLL_FAC_ 16
#endif

#ifdef _OPENARC_
#if BLOCK_SIZE == 4
#pragma openarc #define BLOCK_SIZE 4
#elif BLOCK_SIZE == 8
#pragma openarc #define BLOCK_SIZE 8
#elif BLOCK_SIZE == 16
#pragma openarc #define BLOCK_SIZE 16
#elif BLOCK_SIZE == 32
#pragma openarc #define BLOCK_SIZE 32
#elif BLOCK_SIZE == 64
#pragma openarc #define BLOCK_SIZE 64
#endif
#endif
