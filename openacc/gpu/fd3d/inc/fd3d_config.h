// Specifies the RADIUS of the stencil
// For an order-k stencil, RADIUS = k / 2
#define RADIUS 4
#pragma openarc #define RADIUS 4

// Volume dimensions (default 504)
#define DEFAULT_SIZEX 64 
#define DEFAULT_SIZEY 64
#define DEFAULT_SIZEZ 64 

#pragma openarc #define DEFAULT_SIZEX 64
#pragma openarc #define DEFAULT_SIZEY 64
#pragma openarc #define DEFAULT_SIZEZ 64

#define AOCL_ALIGNMENT 64 
#define MIN(a,b) (((a) < (b)) ? (a) : (b)) 

#define coord(x, y, z) ((z) * DEFAULT_SIZEX * DEFAULT_SIZEY + (y) * DEFAULT_SIZEX + (x))
//#define coord(x, y, z) ((z) * sizex * sizey + (y) * sizex + (x))
