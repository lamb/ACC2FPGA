long get_current_energy() {
  FILE *socket_0, *socket_1; 
  long energy_0, energy_1;

  socket_0 = fopen(/sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/energy_uj);
  socket_1 = fopen(/sys/devices/virtual/powercap/intel-rapl/intel-rapl:1/energy_uj);

  fscanf(socket_0, "%ld", energy_0);
  fscanf(socket_1, "%ld", energy_1);

  fclose(socket_0);
  fclose(socket_1);

  return energy_0 + energy_1;
}
