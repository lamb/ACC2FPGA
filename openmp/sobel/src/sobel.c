// Copyright (C) 2013-2016 Altera Corporation, San Jose, California, USA. All rights reserved.
// Permission is hereby granted, free of charge, to any person obtaining a copy of this
// software and associated documentation files (the "Software"), to deal in the Software
// without restriction, including without limitation the rights to use, copy, modify, merge,
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to
// whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
// 
// This agreement shall be governed in all respects by the laws of the State of California and
// by the laws of the United States of America.

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "parse_ppm.h"
#include "defines.h"

long getCurrentEnergy() {
  long energy_0, energy_1;

  FILE *socket_0 =
    fopen("/sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/energy_uj", "r");
  FILE *socket_1 =
    fopen("/sys/devices/virtual/powercap/intel-rapl/intel-rapl:1/energy_uj", "r");

  int rv1 = fscanf(socket_0, "%ld", &energy_0);
  int rv2 = fscanf(socket_1, "%ld", &energy_1);

  fclose(socket_0);
  fclose(socket_1);

  return energy_0 + energy_1;
}

unsigned int thresh = 128;

unsigned int *input = NULL;
unsigned int *omp_output = NULL;
unsigned int *cpu_output = NULL;

char const *imageFilename;
bool testMode = false;
unsigned testThresholds[] = {32, 96, 128, 192, 225};
unsigned testFrameIndex = 0;

void sobel_OMP(unsigned int * frame_in, unsigned int * frame_out, int iterations, int threshold);
void sobel_CPU(unsigned int * frame_in, unsigned int * frame_out, int iterations, int threshold);

void teardown(int exit_status);
void dumpFrame(unsigned frameIndex, unsigned *frameData);
void filter();
double getCurrentTimestamp();

int main(int argc, char **argv)
{
  imageFilename = "./butterflies.ppm";

  testMode = true;

  posix_memalign((void  **)(&input), AOCL_ALIGNMENT, sizeof(unsigned int) * ROWS * COLS);
  posix_memalign((void  **)(&omp_output), AOCL_ALIGNMENT, sizeof(unsigned int) * ROWS * COLS);
  posix_memalign((void  **)(&cpu_output), AOCL_ALIGNMENT, sizeof(unsigned int) * ROWS * COLS);

  // Read the image
  if (!parse_ppm(imageFilename, COLS, ROWS, (unsigned char *)input)) {
    fprintf(stderr, "Error: could not load %s\n", argv[1]);
    teardown(1);
  }

  filter();

  teardown(0);
}

/// Helper Functions
void filter()
{
  size_t sobelSize = 1;

  int pixels = COLS * ROWS;

  {
    long energy = getCurrentEnergy();
    double time = getCurrentTimestamp();

    sobel_OMP(input, omp_output, pixels, thresh);

    time = (getCurrentTimestamp() - time);
    energy = getCurrentEnergy() - energy;

    printf("\tMulti-Threaded %d time:%.4f ms\n", thresh, time * 1000);
    printf("\tEnergy Consumed: %.4lf J\n", (double) energy / 1E6);
  }

  // Verification
  double diff_norm = 0;
  double cpu_norm = 0;

  for (int i = 0; i < ROWS*COLS; ++i) {
    int diff = omp_output[i] - cpu_output[i];
    diff_norm += diff * diff;
    cpu_norm += cpu_output[i] * cpu_output[i];
  }

  diff_norm = sqrt(diff_norm);
  cpu_norm = sqrt(cpu_norm);

  double rel_err = diff_norm / cpu_norm;
    
  if(rel_err < 1e-6)
    printf("\nVerification Successfull\n\t CPU L2Norm = %e, Diff L2Norm = %e Err=%e\n", cpu_norm, diff_norm, rel_err);
  else
    printf("\nVerification Failed\n\t CPU L2Norm = %e, Diff L2Norm = %e Err=%e\n", cpu_norm, diff_norm, rel_err);

  //dumpFrame(testFrameIndex, output);

}

// Dump frame data in PPM format.
void dumpFrame(unsigned frameIndex, unsigned *frameData) {
  unsigned y; 
  unsigned x; 
 
  char fname[256];
  sprintf(fname, "frame%d.ppm", frameIndex);

  printf("Dumping %s\n", fname);

  FILE *f = fopen(fname, "wb");
  fprintf(f, "P6\n%d %d\n%d\n", COLS, ROWS, 255);
  for(y = 0; y < ROWS; ++y) {
    for(x = 0; x < COLS; ++x) {
      // This assumes byte-order is little-endian.
      unsigned int pixel = frameData[y * COLS + x];
      fwrite(&pixel, 1, 3, f);
    }
  }
  fclose(f);
}

void teardown(int exit_status)
{
  if (input) free(input);
  if (omp_output) free(omp_output);
  if (cpu_output) free(cpu_output);

  exit(exit_status);
}

// High-resolution timer.
double getCurrentTimestamp() {
  struct timespec a;
  clock_gettime(CLOCK_MONOTONIC, &a);
  return ( (double) a.tv_nsec * 1.0e-9) + (double) a.tv_sec;
}

