#include "stdio.h"
#include "fd3d_config.h"

#if OMP_BASIC 
extern int nthreads;

void Finite(float *input, float *output, float *coeff, int sizex, int sizey, int sizez) {

  omp_set_num_threads(nthreads);

  #pragma omp parallel for collapse(3) shared(input, output, coeff)
  for (int z = 0; z < sizez; z++) {
    for (int y = 0; y < sizey; y++) {
      for (int x = 0; x < sizex; x++) {

        if (x >= RADIUS && x < sizex - RADIUS &&
            y >= RADIUS && y < sizey - RADIUS &&
            z >= RADIUS && z < sizez - RADIUS) {

          float result = coeff[0] * input[coord(x, y, z)];
          // #pragma unroll
          for (int r = 1 ; r <= RADIUS; r++) {
            result += coeff[r] * (
                input[coord(x - r, y, z)] +
                input[coord(x + r, y, z)] +
                input[coord(x, y - r, z)] +
                input[coord(x, y + r, z)] +
                input[coord(x, y, z - r)] +
                input[coord(x, y, z + r)] );
          }
          output[coord(x, y, z)] = result;
        } 
        else 
          output[coord(x, y, z)] = input[coord(x, y, z)];
      } // x
    } // y
  } // z

}
#endif
