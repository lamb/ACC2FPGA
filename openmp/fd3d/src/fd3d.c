#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include "fd3d_config.h"

long getCurrentEnergy() {
  long energy_0, energy_1;

  FILE *socket_0 = 
    fopen("/sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/energy_uj", "r");
  FILE *socket_1 = 
    fopen("/sys/devices/virtual/powercap/intel-rapl/intel-rapl:1/energy_uj", "r");

  int rv1 = fscanf(socket_0, "%ld", &energy_0);
  int rv2 = fscanf(socket_1, "%ld", &energy_1);

  fclose(socket_0);
  fclose(socket_1);

  return energy_0 + energy_1;
}

int fd3d_gold(float *dataA, float *dataB, float *coeff, 
        int sizex, int sizey, int sizez, int time_steps);
int fd3d_verify(float *dataA, float *dataB,
        int sizex, int sizey, int sizez);

double getCurrentTimestamp();

void Finite(float *input, float *output, float *coeff, int dimx, int dimy, int dimz);

int nthreads = NUM_THREADS;

// Entry point.
int main(int argc, char **argv) {

  // Host memory buffers
  float *h_dataA, *h_dataB;
  float *h_verifyA, *h_verifyB;
  float *h_coeff;

  if (argc != 2) {
    printf("usage: fd3d timesteps\n");
    exit(0);
  }

  // command line options t, x, y, z
  int sizex = DEFAULT_SIZEX, sizey = DEFAULT_SIZEY, sizez = DEFAULT_SIZEZ;
  int time_steps = atoi(argv[1]);

  // Options validation.
  if (time_steps <= 0 || sizex <= 2 * RADIUS || sizey <= 2 * RADIUS || sizez <= 2 * RADIUS) {
    printf("ERROR: Invalid arguments\n\nUsage: %s [n [x y z]]\n\tn: number of time_steps to run (default 5)\n"
        "\tx, y, z: volume dimensions on each axis (default %d x %d x %d, min %d x %d x %d)\n", 
        argv[0], DEFAULT_SIZEX, DEFAULT_SIZEY, DEFAULT_SIZEZ, 2 * RADIUS + 1, 2 * RADIUS + 1, 2 * RADIUS + 1);
    return 0;
  }
  printf("Volume size: %d x %d x %d\n", sizex, sizey, sizez);

  // Allocate host memory
  posix_memalign((void **)(&h_dataA), AOCL_ALIGNMENT, sizeof(float)*sizex*sizey*sizez);
  posix_memalign((void **)(&h_dataB), AOCL_ALIGNMENT, sizeof(float)*sizex*sizey*sizez);
  posix_memalign((void **)(&h_verifyA), AOCL_ALIGNMENT, sizeof(float)*sizex*sizey*sizez);
  posix_memalign((void **)(&h_verifyB), AOCL_ALIGNMENT, sizeof(float)*sizex*sizey*sizez);
  posix_memalign((void **)(&h_coeff), AOCL_ALIGNMENT, sizeof(float) * (RADIUS + 1));
  if (!(h_dataA && h_dataB && h_verifyA && h_verifyB && h_coeff)) {
    printf("ERROR: Couldn't create host buffers\n");
    return 0;
  }

  printf("Launching an order-%d stencil computation for %d time steps\n", RADIUS * 2, time_steps);

  // Initialize input
  for (int i = 0; i < RADIUS + 1; i++) {
    h_coeff[i] = (float)((double)rand() / (double)RAND_MAX);
  }
  for (int i = 0; i < sizex * sizey * sizez; i++) {
    h_verifyA[i] = h_dataA[i] = (float)((double)rand() / (double)RAND_MAX);
  }

  long energy = getCurrentEnergy();

  double time;
  time = getCurrentTimestamp();

  for (int i = 0; i < time_steps; i++) {
    // even :: odd
    float *input = i % 2 == 0 ? h_dataA : h_dataB;
    float *output = i % 2  == 0 ? h_dataB : h_dataA;

    Finite(input, output, h_coeff, sizex, sizey, sizez);
  }

  time = getCurrentTimestamp() - time;
  energy = getCurrentEnergy() - energy;

  double gpoints_per_sec;
  printf("\n\tMulti-Threaded processing time = %.4f ms\n", (float)(time * 1E3));

  gpoints_per_sec = ((double) sizex * sizey * sizez / (time / time_steps)) * 1E-9;
  printf("\tThroughput = %.4f Gpoints / sec\n", gpoints_per_sec);

  printf("\tEnergy Consumed: %.4lf J\n", (double) energy / 1E6);
  
  time = getCurrentTimestamp();
  fd3d_gold(h_verifyA, h_verifyB, h_coeff, sizex, sizey, sizez, time_steps);
  time = getCurrentTimestamp() - time;

  printf("\n\tSerial processing time = %.4fms\n", (float)(time * 1E3));

  gpoints_per_sec = ((double) sizex * sizey * sizez / (time / time_steps)) * 1E-9;
  printf("\tThroughput = %.4f Gpoints / sec\n", gpoints_per_sec);

  int verify = time_steps % 2 ? 
      fd3d_verify(h_dataB, h_verifyB, sizex, sizey, sizez) :
      fd3d_verify(h_dataA, h_verifyA, sizex, sizey, sizez);
  printf("\n\tVerifying data --> %s\n", verify ? "PASSED" : "FAILED");

  // Free the resources allocated
  free(h_dataA);
  free(h_dataB);
  free(h_verifyA);
  free(h_verifyB);
  free(h_coeff);

  return 0;
}

int fd3d_gold(float *dataA, float *dataB, float *coeff, 
      int sizex, int sizey, int sizez, int time_steps) {
  float *from, *to;
  for (int i = 0; i < time_steps; i++) {
    // even : odd 
    from = i % 2 == 0 ? dataA : dataB;
    to = i % 2 == 0 ? dataB : dataA;
    for (int z = 0; z < sizez; z++) {
      for (int y = 0; y < sizey; y++) {
        for (int x = 0; x < sizex; x++) {
          if (x >= RADIUS && x < sizex - RADIUS &&
              y >= RADIUS && y < sizey - RADIUS &&
              z >= RADIUS && z < sizez - RADIUS) {
            float result = coeff[0] * from[coord(x, y, z)];
            for (int r = 1 ; r <= RADIUS; r++) {
              result += coeff[r] * from[coord(x - r, y, z)];
              result += coeff[r] * from[coord(x + r, y, z)];
              result += coeff[r] * from[coord(x, y - r, z)];
              result += coeff[r] * from[coord(x, y + r, z)];
              result += coeff[r] * from[coord(x, y, z - r)];
              result += coeff[r] * from[coord(x, y, z + r)];
            }
            to[coord(x, y, z)] = result;
          } else {
            to[coord(x, y, z)] = from[coord(x, y, z)];
          }
        }
      }
    }
  }
}

int fd3d_verify(float *acc_dataB, float *cpu_dataB, int sizex, int sizey, int sizez)
{ 
  for (int z = 0; z < sizez; z++) {
    for (int y = 0; y < sizey; y++) {
      for (int x = 0; x < sizex; x++) {
        float delta = acc_dataB[coord(x, y, z)] - cpu_dataB[coord(x, y, z)];
        float error = cpu_dataB[coord(x, y, z)] != 0 ? 
            fabs(delta) / cpu_dataB[coord(x, y, z)] : 
            fabs(delta);
        if (error >  1e-4) {
          printf("%d %d %d acc_data: %f cpu_data: %f\n", x, y, z, 
            acc_dataB[coord(x, y, z)], cpu_dataB[coord(x, y, z)]);
          return 0;
        }
      }
    }
  }
  return 1;
}

// High-resolution timer.
double getCurrentTimestamp() {
  struct timespec a;
  clock_gettime(CLOCK_MONOTONIC, &a);
  return ( (double) a.tv_nsec * 1.0e-9) + (double) a.tv_sec;
}
